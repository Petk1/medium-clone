
# Medium Clone

[![RealWorld Frontend](https://img.shields.io/badge/realworld-frontend-%23783578.svg)](http://realworld.io)


Medium Clone is an Angular project that uses the [RealWorld API](https://www.realworld.how/docs/specs/backend-specs/endpoints) to create own version of Medium.com. It's a social blogging site that enables users to create an account, read and post articles and comments.

**Live Demo:** [Medium Clone Demo](https://medium-clone-blush-iota.vercel.app/global-feed)

## Getting started

Make sure you have the Angular CLI installed.  

To manage dependencies you can use yarn or npm.  
Depending on what you choose, run `yarn install` or `npm install`.

Once all dependencies are resolved run ```ng serve``` for a dev server.  
Then open your browser and navigate tn `http://localhost:4200/`.

## Functionality overview

* Authenticate users via JWT (login and signup pages + logout option)
* CRU* users (sign up and settings page)
* CRUD Articles
* CR*D Comments on articles
* GET and display paginated lists of articles
* Favorite articles
* Follow users

<div>
  <p align="center">
    <img src="src/assets/images/angular_logo.png" alt="angular-logo" width="110px" height="121px"/>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <img src="src/assets/images/ngrx_logo.png" alt="ngrx-logo" width="120px" height="120px"/>
  </p>
</div>
