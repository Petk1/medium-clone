import { ActivatedRoute, NavigationStart, Params, Router, convertToParamMap } from "@angular/router";
import { By } from "@angular/platform-browser";
import { TestBed, fakeAsync, flush } from "@angular/core/testing";
import { of } from "rxjs";
import { provideMockStore } from "@ngrx/store/testing";
import { selectCurrentUser } from "src/app/auth/store/reducers";
import { selectArticlesData } from "src/app/shared/components/articles/store/reducers";
import { HomeComponent } from "./home.component";
import { PopularTagsComponent } from "src/app/shared/components/popular-tags/popular-tags.component";
import { IGetArticlesResponse } from "src/app/shared/components/articles/types/getArticlesResponse.interface";

describe('HomeComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  describe('Setting the initial toggler option', () => {
    describe('User is logged in', () => {
      it('should toggle to the Global Feed option', () => {
        const { componentInstance } = setup(true, 'global-feed');
        expect(componentInstance.initSelectedOptionSig().apiUrl).toBe(componentInstance.togglerOptionsSig()[1].apiUrl);
      })

      it('should toggle to the Your Feed option', () => {
        const { componentInstance } = setup(true, 'your-feed', '/your-feed');
        expect(componentInstance.initSelectedOptionSig().apiUrl).toBe(componentInstance.togglerOptionsSig()[0].apiUrl);
      })
    })

    describe('User is NOT logged in', () => {
      it('should toggle to the Global Feed option', () => {
        const { componentInstance } = setup(false, 'global-feed');
        expect(componentInstance.initSelectedOptionSig().apiUrl).toBe(componentInstance.togglerOptionsSig()[1].apiUrl);
      })

      it('should navigate to the login page if the feedType url param is "your-feed"', fakeAsync(() => {
        const { componentInstance, mockRouter } = setup(false, 'your-feed', '/your-feed');
        const apiUr = componentInstance.togglerOptionsSig()[0].apiUrl;
        flush();
        expect(apiUr).toBeUndefined();
        expect(mockRouter.navigateByUrl).toHaveBeenCalled();
      }))
    })

    it('should toggle to the Global Feed option if the url is "/"', () => {
      const { componentInstance } = setup(true, 'your-feed', '/');
      expect(componentInstance.initSelectedOptionSig().apiUrl).toBe(componentInstance.togglerOptionsSig()[1].apiUrl);
    })
  })

  it('should increase the updateViewCounterSig', () => {
    const { componentInstance } = setup(false, 'global-feed');
    expect(componentInstance['updateViewCounterSig']()).toBe(1);
  })

  it('should display popular tags', () => {
    const { debugEl } = setup(false, 'global-feed');
    const popularTagsDebugEl = debugEl.query(By.directive(PopularTagsComponent));
    expect(popularTagsDebugEl).toBeTruthy();
  })
})

function setup(userIsLoggedIn = true, feedTypeInUrl: 'your-feed' | 'global-feed' = 'global-feed', url = '/') {
  const mockRouter = {
    url: '/',
    events: of(new NavigationStart(1, url)),
    navigate: jasmine.createSpy('navigate'),
    navigateByUrl: jasmine.createSpy('navigateByUrl'),
    createUrlTree: () => {},
    serializeUrl: () => {},
  };

  const mockActivatedRoute = {
    snapshot: {
      paramMap: {
        get: jasmine.createSpy('feedType').and.returnValue(feedTypeInUrl),
      },
      queryParamMap: {
        get: jasmine.createSpy('get').and.returnValue('1')
      },
    },
    queryParams: {} as Params,
    queryParamMap: of(convertToParamMap({ page: 1 })),
  }

  TestBed.configureTestingModule({
    providers: [
      {
        provide: Router,
        useValue: mockRouter
      },
      {
        provide: ActivatedRoute,
        useValue: mockActivatedRoute
      },
      provideMockStore({
        selectors: [
          { selector: selectCurrentUser, value: userIsLoggedIn ? {} : null },
          { selector: selectArticlesData, value: MOCK_ARTICLES_DATA }
        ]
      })
    ]
  })

  const fixture = TestBed.createComponent(HomeComponent);
  const componentInstance = fixture.componentInstance;
  const debugEl = fixture.debugElement;

  fixture.detectChanges();

  return {
    fixture,
    componentInstance,
    mockRouter,
    debugEl,
  }
}

const MOCK_ARTICLES_DATA: IGetArticlesResponse = {
  articles: [
    {
      slug: "asdas-240342",
      title: "asdas",
      description: "dasd",
      body: "asdasd",
      tagList: [
        "asdasd"
      ],
      createdAt: "2023-11-05T16:47:51.553Z",
      updatedAt: "2023-11-05T16:47:51.553Z",
      favorited: false,
      favoritesCount: 0,
      author: {
        username: "ABC_123_ABC",
        bio: "abcdefghijklmnollko",
        image: "https://api.realworld.io/images/smiley-cyrus.jpeg",
        following: false
      }
    },
    {
      slug: "sadasd-240342",
      title: "sadasd",
      description: "asdas",
      body: "dasda",
      tagList: [
        "sdasd",
        'ABC',
        'test tag'
      ],
      createdAt: "2023-11-05T16:14:21.006Z",
      updatedAt: "2023-11-05T16:14:21.006Z",
      favorited: false,
      favoritesCount: 0,
      author: {
        username: "ABC_123_ABC",
        bio: "abcdefghijklmnollko",
        image: "https://api.realworld.io/images/smiley-cyrus.jpeg",
        following: false
      }
    }
  ],
  articlesCount: 2
}
