import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, computed, effect, inject, signal } from "@angular/core";
import { ActivatedRoute, NavigationStart, Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { FeedTogglerComponent } from "src/app/shared/components/feed-toggler/feed-toggler.component";
import { PopularTagsComponent } from "src/app/shared/components/popular-tags/popular-tags.component";
import { IFeedTogglerOption } from "src/app/shared/components/feed-toggler/types/feedTogglerOption.interface";
import { selectCurrentUser } from "src/app/auth/store/reducers";
import { Subject, takeUntil, tap } from "rxjs";

@Component({
  selector: 'mc-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    FeedTogglerComponent,
    PopularTagsComponent,
  ],
})
export class HomeComponent implements OnInit, OnDestroy {
  private readonly router = inject(Router);
  private readonly activatedRoute = inject(ActivatedRoute);
  private feedTypeFromUrl = this.activatedRoute.snapshot.paramMap.get('feedType');
  private queryParams = this.activatedRoute.snapshot.queryParams;
  private readonly currentUser = inject(Store).selectSignal(selectCurrentUser);
  private readonly updateViewCounterSig = signal(0);
  private readonly destroy$ = new Subject<void>();

  togglerOptionsSig = computed<IFeedTogglerOption[]>(() => (
    [
      {
        name: 'Your Feed',
        apiUrl: this.currentUser() ? '/articles/feed' : undefined,
        callback: () => {
          this.currentUser()
            ? this.getOptionNavigateByUrl('/your-feed')
            : this.getOptionNavigateByUrl('/login')
        },
      },
      {
        name: 'Global Feed',
        apiUrl: '/articles',
        callback: () => this.getOptionNavigateByUrl('/global-feed'),
      },
    ]
  ));

  initSelectedOptionSig = computed<IFeedTogglerOption>(() => {
    this.updateViewCounterSig();
    const togglerOptions = this.togglerOptionsSig();
    return this.getInitSelectedOption(togglerOptions);
  })

  getInitSelectedOption(togglerOptions: IFeedTogglerOption[]): IFeedTogglerOption {
    if(this.feedTypeFromUrl === 'your-feed') {
      return {
        ...togglerOptions[0],
        callback: () => this.getOptionNavigate('/your-feed'),
      }
    }
    if(this.feedTypeFromUrl === 'global-feed') {
      return {
        ...togglerOptions[1],
        callback: () => this.getOptionNavigate('/global-feed'),
      }
    }

    return {
      ...togglerOptions[1],
      callback: () => this.getOptionNavigate('/global-feed'),
    }
  }

  constructor(){
    effect(() => {
      const currentUser = this.currentUser();
      if(this.feedTypeFromUrl === 'your-feed' && currentUser === null) {
        setTimeout(() => this.router.navigateByUrl('/login'));
      }
    })
  }

  ngOnInit(): void {
    // To handle the case when the user uses the menu to navigate to the 'Home' page being on the 'Home" page.
    // Without this snippet of code the data reload and showing articles were not triggered.
    // The updateViewCounterSig signal triggers a recompute of the initSelectedOptionSig signal and it
    // triggers the onToggle method in the feed-toggle.component.ts.
    // In result it kind of works like the user used the toggler to change the type of articles displayed.
    this.router.events
    .pipe(
      tap(routerEvent => {
        if(routerEvent instanceof NavigationStart && routerEvent.url === '/') {
          this.feedTypeFromUrl = 'global-feed';
          this.queryParams = {};
          let updateViewCounterValue = this.updateViewCounterSig();
          this.updateViewCounterSig.set(++updateViewCounterValue);
        }
      }),
      takeUntil(this.destroy$)
    )
    .subscribe()
  }

  private getOptionNavigate(path: string) {
    return this.router.navigate([path], { queryParams: this.queryParams })
  }

  private getOptionNavigateByUrl(path: string) {
    return this.router.navigateByUrl(path);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
