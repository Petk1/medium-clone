import { Route } from "@angular/router";
import { HomeComponent } from "./components/home.component";

export const routes: Route[] =[
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: '/global-feed',
        pathMatch: 'full',
      },
      {
        path: ':feedType',
        component: HomeComponent,
        pathMatch: 'full',
      }
    ]
  },
]
