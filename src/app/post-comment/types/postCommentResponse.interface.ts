import { IComment } from '../../shared/types/comment.interface';

export interface IPostCommentResponse {
  comment: IComment;
}
