import { IComment } from "src/app/shared/types/comment.interface";

export interface IPostCommentState {
  comment: IComment | undefined | null;
}
