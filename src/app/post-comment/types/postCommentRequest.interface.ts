export interface IPostCommentRequest {
  comment: {
    body: string;
  }
}
