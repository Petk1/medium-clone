import { inject } from "@angular/core";
import { createEffect, Actions, ofType } from "@ngrx/effects";
import { switchMap, map, catchError, of, tap } from "rxjs";
import { PostCommentService } from "../services/post-comment.service";
import { postCommentActions } from "./actions";
import { commentsActions } from "src/app/comments/store/actions";

export const postCommentEffect = createEffect((
  actions$ = inject(Actions),
  commentSrv = inject(PostCommentService),
) => (
  actions$.pipe(
    ofType(postCommentActions.postComment),
    switchMap(({ slug, comment }) => (
      commentSrv.postComment(slug, comment)
      .pipe(
        map(postedComment => postCommentActions.postCommentSuccess({ comment: postedComment })),
        catchError(() => of(postCommentActions.postCommentFailure()))
      )
    ))
  )
), {
  functional: true
})

export const addNewComment = createEffect((
  actions$ = inject(Actions),
) => (
  actions$.pipe(
    ofType(postCommentActions.postCommentSuccess),
    map(({ comment: { comment } }) => commentsActions.addComment({ comment }))
  )
), {
  functional: true
})
