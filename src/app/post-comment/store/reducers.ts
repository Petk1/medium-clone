import { createFeature, createReducer, on } from "@ngrx/store";
import { IPostCommentState } from "../types/postCommentState.interface";
import { postCommentActions } from "./actions";
import { routerNavigatedAction } from "@ngrx/router-store";

const initialState: IPostCommentState = {
  comment: undefined,
}

const postCommentFeature = createFeature({
  name: 'postComment',
  reducer: createReducer(
    initialState,
    on(postCommentActions.postCommentSuccess, (state, { comment: { comment } }) => ({
      ...state,
      comment,
    })),
    on(routerNavigatedAction, () => initialState)
  )
})

export const {
  name: postCommentFeatureKey,
  reducer: postCommentReducer,
  selectComment,
} = postCommentFeature;
