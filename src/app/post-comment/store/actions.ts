import { createActionGroup, emptyProps, props } from "@ngrx/store";
import { IPostCommentRequest } from "../types/postCommentRequest.interface";
import { IPostCommentResponse } from "../types/postCommentResponse.interface";

export const postCommentActions = createActionGroup({
  source: 'Post Comment',
  events: {
    'Post Comment': props<{ slug: string, comment: IPostCommentRequest }>(),
    'Post Comment success': props<{ comment: IPostCommentResponse }>(),
    'Post Comment failure': emptyProps(),
  }
})
