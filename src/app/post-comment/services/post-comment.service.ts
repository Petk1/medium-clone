import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable } from 'rxjs';
import { IPostCommentResponse } from '../types/postCommentResponse.interface';
import { IPostCommentRequest } from '../types/postCommentRequest.interface';

@Injectable()
export class PostCommentService {
  private http = inject(HttpClient);

  postComment(slug: string, comment: IPostCommentRequest): Observable<IPostCommentResponse> {
    const url = `articles/${slug}/comments`;
    return this.http.post<IPostCommentResponse>(url, comment);
  }
}
