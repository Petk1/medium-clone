import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, effect, inject } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { Store } from "@ngrx/store";
import { Router } from "@angular/router";
import { postCommentActions } from "../store/actions";
import { IPostCommentRequest } from "../types/postCommentRequest.interface";
import { selectCurrentUser } from "src/app/auth/store/reducers";
import { selectComment } from "../store/reducers";

@Component({
  selector: 'mc-post-comment',
  templateUrl: './post-comment.component.html',
  styleUrls: ['./post-comment.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    FormsModule,
  ]
})
export class PostCommentComponent {
  @Input({ required: true }) slug!: string;

  comment: string = '';

  private store = inject(Store);
  private currentUser = this.store.selectSignal(selectCurrentUser);
  private router = inject(Router);
  private cdr = inject(ChangeDetectorRef);

  postedCommentSignal = this.store.selectSignal(selectComment);

  constructor() {
    effect(() => this.postedCommentSignal() && this.clearComment());
  }

  clearComment() {
    this.comment = '';
    this.cdr.markForCheck();
  }

  postComment() {
    if(!this.currentUser()) {
      this.router.navigate(['/login']);
      return;
    }

    if(!this.comment) return;

    const postCommentRequest: IPostCommentRequest = {
      comment: {
        body: this.comment
      },
    }

    this.store.dispatch(postCommentActions.postComment({
      slug: this.slug,
      comment: postCommentRequest,
    }));
  }
}
