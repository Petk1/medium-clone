import { Router } from "@angular/router"
import { TestBed} from "@angular/core/testing"
import { MockStore, provideMockStore } from "@ngrx/store/testing"
import { postCommentActions } from "../store/actions"
import { selectComment } from "../store/reducers"
import { selectCurrentUser } from "src/app/auth/store/reducers"
import { PostCommentComponent } from "./post-comment.component"
import { IPostCommentRequest } from "../types/postCommentRequest.interface"

describe('PostCommentComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  it('should post a comment if user is logged in', () => {
    const { componentInstance, store } = setup();
    const mockStoreDispatchSpy = spyOn(store, 'dispatch').and.callThrough();

    componentInstance.comment = 'Test comment';
    componentInstance.postComment();
    const postCommentRequest: IPostCommentRequest = {
      comment: {
        body: componentInstance.comment
      },
    }

    expect(mockStoreDispatchSpy).toHaveBeenCalledOnceWith(postCommentActions.postComment({
      slug: componentInstance.slug,
      comment: postCommentRequest,
    }))
  })

  it('should navigate to login page if a user is not logged in', () => {
    const { componentInstance, store } = setup(false);
    const mockStoreDispatchSpy = spyOn(store, 'dispatch').and.callThrough();
    componentInstance.comment = 'Test comment';
    componentInstance.postComment();
    expect(mockStoreDispatchSpy).not.toHaveBeenCalled();
  })

  it('should clear an entered comment data', () => {
    const { componentInstance } = setup();
    componentInstance.comment = 'Test comment';
    componentInstance.clearComment();
    expect(componentInstance.comment).toBe('');
  })
})

function setup(isUserLoggedIn = true) {
  const mockRouter = {
    navigate: jasmine.createSpy('navigate'),
  };

  TestBed.configureTestingModule({
    providers: [
      {
        provide: Router,
        useValue: mockRouter
      },
      provideMockStore({
        selectors: [
          { selector: selectCurrentUser, value: isUserLoggedIn ? {} : null },
          { selector: selectComment, value: null}
        ]
      })
    ]
  })

  const fixture = TestBed.createComponent(PostCommentComponent);
  const componentInstance = fixture.componentInstance;
  const debugEl = fixture.debugElement;
  const store = TestBed.inject(MockStore);
  fixture.componentRef.setInput('slug', 'test-slug');

  fixture.detectChanges();

  return {
    fixture,
    componentInstance,
    debugEl,
    store,
  }
}
