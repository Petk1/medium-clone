import { IArticle } from "../../shared/types/article.interface";

export interface IEditArticleRequest {
  article: IArticle;
}
