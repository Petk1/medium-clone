import { Route } from "@angular/router";
import { provideEffects } from "@ngrx/effects";
import { provideState } from "@ngrx/store";
import * as articleEffects from './store/effects';
import { ArticleComponent } from "./components/article/article.component";
import { ArticleService } from "../shared/services/article.service";
import { articleFeatureKey, articleReducer } from "./store/reducers";

export const routes: Route[] = [
  {
    path: '',
    component: ArticleComponent,
    providers: [
      provideState(articleFeatureKey, articleReducer),
      provideEffects(articleEffects),
      ArticleService,
    ]
  }
]
