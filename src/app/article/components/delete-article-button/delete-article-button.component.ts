import { ChangeDetectionStrategy, Component, Input, inject } from "@angular/core";
import { Store } from "@ngrx/store";
import { articleActions } from "../../store/actions";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { IInitConfirmDialogData } from "src/app/shared/types/initConfirmDialogData.interface";
import { ConfirmDialogComponent } from "src/app/shared/components/confirm-dialog/confirm-dialog.component";

@Component({
  selector: 'mc-delete-article-button',
  template: `
    <button class="primary-btn primary-btn--active" (click)="onDelete()">
      Delete Article
    </button>
  `,
  styles: [`
    button {
      height: 30px;
    }
  `],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteArticleButtonComponent {
  @Input({ required: true }) slug!: string;

  private readonly store = inject(Store);
  private readonly matDialog = inject(MatDialog);

  onDelete(): void {
    const dialogConfig = new MatDialogConfig<IInitConfirmDialogData>();
    dialogConfig.width = '440px';
    dialogConfig.height = '275px';
    dialogConfig.closeOnNavigation = true;
    dialogConfig.position = {
      top: '125px',
    }
    dialogConfig.data = {
      title: 'Confirm deletion',
      content: 'Are you sure you want to delete this article?'
    }

    this.matDialog.open(ConfirmDialogComponent, dialogConfig)
      .afterClosed()
      .subscribe((decision: boolean) => {
        if(decision) {
          this.store.dispatch(articleActions.deleteArticle({ slug: this.slug }));
        }
      })
  }
}
