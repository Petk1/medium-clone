import { MatDialog } from "@angular/material/dialog"
import { TestBed } from "@angular/core/testing"
import { Subject } from "rxjs"
import { MockStore, provideMockStore } from "@ngrx/store/testing"
import { articleActions } from "../../store/actions"
import { DeleteArticleButtonComponent } from "./delete-article-button.component"

describe('DeleteArticleButtonComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  describe('Article deletion handling', () => {
    let _componentInstance: DeleteArticleButtonComponent;
    let _mockMatDialog: { open: jasmine.Spy<jasmine.Func> };
    let _store: MockStore<any>;
    let mockStoreDispatchSpy: jasmine.Spy;

    beforeEach(() => {
      const { componentInstance, mockMatDialog, store } = setup();
      _componentInstance = componentInstance;
      _mockMatDialog = mockMatDialog;
      _store = store;
      mockStoreDispatchSpy = spyOn(store, 'dispatch').and.callThrough();
    })

    it('should open a confirm dialog and delete an article if confirmed', () => {
      const afterClosed$ = new Subject<boolean>()
      const mockMatDialogRef = {
        afterClosed: () => afterClosed$,
        close: jasmine.createSpy('close').and.callFake(() => {
          afterClosed$.next(true);
        })
      }
      _mockMatDialog.open.and.returnValue(mockMatDialogRef);

      _componentInstance.onDelete();
      mockMatDialogRef.close();

      expect(_mockMatDialog.open).toHaveBeenCalled();
      expect(mockStoreDispatchSpy).toHaveBeenCalledOnceWith(articleActions.deleteArticle({ slug: _componentInstance.slug }));
    })

    it('should open a confirm dialog and do not delete an article if not confirmed', () => {
      const afterClosed$ = new Subject<boolean>()
      const mockMatDialogRef = {
        afterClosed: () => afterClosed$,
        close: jasmine.createSpy('close').and.callFake(() => {
          afterClosed$.next(false);
        })
      }
      _mockMatDialog.open.and.returnValue(mockMatDialogRef);

      _componentInstance.onDelete();
      mockMatDialogRef.close();

      expect(_mockMatDialog.open).toHaveBeenCalled();
      expect(mockStoreDispatchSpy).not.toHaveBeenCalled();
    })
  })
})

function setup() {
  const mockMatDialog = {
    open: jasmine.createSpy('open')
  }

  TestBed.configureTestingModule({
    providers: [
      {
        provide: MatDialog,
        useValue: mockMatDialog,
      },
      provideMockStore({ initialState: {} })
    ]
  })

  const fixture = TestBed.createComponent(DeleteArticleButtonComponent);
  fixture.componentRef.setInput('slug', 'testSlug');
  const componentInstance = fixture.componentInstance;
  const store = TestBed.inject(MockStore);

  fixture.detectChanges();

  return {
    fixture,
    componentInstance,
    mockMatDialog,
    store,
  }
}
