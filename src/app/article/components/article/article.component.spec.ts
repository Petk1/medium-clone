import { DebugElement } from "@angular/core"
import { By } from "@angular/platform-browser"
import { ActivatedRoute, NavigationEnd, Router, convertToParamMap } from "@angular/router"
import { TestBed } from "@angular/core/testing"
import { of } from "rxjs"
import { MockStore, provideMockStore } from "@ngrx/store/testing"
import { selectArticleData, selectError, selectIsLoading } from "../../store/reducers"
import { articleActions } from "../../store/actions"
import { selectCurrentUser } from "src/app/auth/store/reducers"
import { selectCommentsData } from "src/app/comments/store/reducers"
import { ArticleComponent } from "./article.component"
import { TagsComponent } from "src/app/shared/components/tags/tags.component"
import { CommentsComponent } from "src/app/comments/components/comments.component"
import { LoadingComponent } from "src/app/shared/components/loading/loading.component"
import { PostCommentComponent } from "src/app/post-comment/components/post-comment.component"
import { EditArticleButtonComponent } from "../edit-article-button/edit-article-button.component"
import { DeleteArticleButtonComponent } from "../delete-article-button/delete-article-button.component"
import { FollowButtonComponent } from "src/app/shared/components/follow-button/follow-button.component"
import { ArticleAuthorComponent } from "src/app/shared/components/article-author/article-author.component"
import { FavoriteArticleButtonComponent } from "src/app/shared/components/favorite-article-button/favorite-article-button.component"

describe('ArticleComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  it('should dispatch the getArticle action', () => {
    const { store, componentInstance, mockSlug } = setup();
    const mockStoreDispatchSpy = spyOn(store, 'dispatch').and.callThrough();
    componentInstance.ngOnInit()
    expect(mockStoreDispatchSpy).toHaveBeenCalledWith(articleActions.getArticle({ slug: mockSlug }));
  })

  describe('Property isLoading is true', () => {
    it('should display a loading component', () => {
      const { debugEl } = setup(true);
      const loadingDebugEl = debugEl.query(By.directive(LoadingComponent));
      expect(loadingDebugEl).toBeTruthy();
    })
  })

  describe('Property isLoading is false', () => {
    it('should display an article data', () => {
      const { debugEl } = setup(false);
      const articleAuthorDebugEl = debugEl.query(By.directive(ArticleAuthorComponent));
      const tagsDebugEl = debugEl.query(By.directive(TagsComponent));
      const commentsDebugEl = debugEl.query(By.directive(CommentsComponent));
      const articleBodyEl = debugEl.query(By.css('[data-testingId="article-body"]')).nativeElement as HTMLElement;

      expect(articleAuthorDebugEl).toBeTruthy();
      expect(tagsDebugEl).toBeTruthy();
      expect(commentsDebugEl).toBeTruthy();
      expect(articleBodyEl.textContent).toContain(INITIAL_STATE.articleData.body);
    })

    describe('Current logged in user is also the author of an article', () => {
      let _debugEl: DebugElement;

      beforeEach(() => {
        const { debugEl } = setup(false, {
          username: INITIAL_STATE.articleData.author.username,
        });

        _debugEl = debugEl;
      })

      it('should display the appropriate buttons', () => {
        const followButtonDebugEl = _debugEl.query(By.directive(FollowButtonComponent));
        const favoriteArticleButtonDebugEl = _debugEl.query(By.directive(FavoriteArticleButtonComponent));
        const editArticleButtonDebugEl = _debugEl.query(By.directive(EditArticleButtonComponent));
        const deleteArticleButtonDebugEl = _debugEl.query(By.directive(DeleteArticleButtonComponent));

        expect(followButtonDebugEl).toBeFalsy();
        expect(favoriteArticleButtonDebugEl).toBeFalsy();
        expect(editArticleButtonDebugEl).toBeTruthy();
        expect(deleteArticleButtonDebugEl).toBeTruthy();
      })

      it('should not display a post comment option', () => {
        const postCommentDebugEl = _debugEl.query(By.directive(PostCommentComponent));
        expect(postCommentDebugEl).toBeFalsy();
      })
    })

    describe('Current logged in user is not the author of an article', () => {
      let _debugEl: DebugElement;

      beforeEach(() => {
        const { debugEl } = setup(false, {
          username: 'anotherOne',
        });

        _debugEl = debugEl;
      })

      it('should display the appropriate buttons', () => {
        const followButtonDebugEl = _debugEl.query(By.directive(FollowButtonComponent));
        const favoriteArticleButtonDebugEl = _debugEl.query(By.directive(FavoriteArticleButtonComponent));
        const editArticleButtonDebugEl = _debugEl.query(By.directive(EditArticleButtonComponent));
        const deleteArticleButtonDebugEl = _debugEl.query(By.directive(DeleteArticleButtonComponent));

        expect(followButtonDebugEl).toBeTruthy();
        expect(favoriteArticleButtonDebugEl).toBeTruthy();
        expect(editArticleButtonDebugEl).toBeFalsy();
        expect(deleteArticleButtonDebugEl).toBeFalsy();
      })

      it('should display a post comment option', () => {
        const postCommentDebugEl = _debugEl.query(By.directive(PostCommentComponent));
        expect(postCommentDebugEl).toBeTruthy();
      })
    })
  })
})

function setup(isLoading = false, storedUserData = { username: 'Mock username' }){
  const mockSlug = 'some-test-slug';

  const mockActivatedRoute = {
    snapshot: {
      paramMap: {
        get: jasmine.createSpy('get').and.returnValue(mockSlug)
      }
    },
    queryParamMap: of(convertToParamMap({ page: 1 })),
  }

  const mockRouter = {
    url: 'url-test',
    events: of(new NavigationEnd(1, 'url', 'urlAfterRedirects')),
    navigate: jasmine.createSpy('navigate'),
    createUrlTree: () => {},
    serializeUrl: () => {},
  };

  TestBed.configureTestingModule({
    providers: [
      {
        provide: ActivatedRoute,
        useValue: mockActivatedRoute
      },
      {
        provide: Router,
        userValue: mockRouter
      },
      provideMockStore({
        initialState: {},
        selectors: [
          { selector: selectIsLoading, value: isLoading },
          { selector: selectError, value: null },
          { selector: selectArticleData, value: INITIAL_STATE.articleData },
          { selector: selectCurrentUser, value: storedUserData },
          { selector: selectCommentsData, value: {} },
        ]
      })
    ]
  })

  const fixtrue = TestBed.createComponent(ArticleComponent);
  const componentInstance = fixtrue.componentInstance;
  const debugEl = fixtrue.debugElement;
  const store = TestBed.inject(MockStore);

  fixtrue.detectChanges();

  return {
    componentInstance,
    debugEl,
    store,
    mockSlug,
  }
}

const INITIAL_STATE = {
  articleData: {
    slug: "asdas-240342",
    title: "asdas",
    description: "dasd",
    body: "asdasd",
    tagList: [
      "asdasd"
    ],
    createdAt: "2023-11-05T16:47:51.553Z",
    updatedAt: "2023-11-05T16:47:51.553Z",
    favorited: false,
    favoritesCount: 0,
    author: {
      username: "ABC_123_ABC",
      bio: "abcdefghijklmnollko",
      image: "https://api.realworld.io/images/smiley-cyrus.jpeg",
      following: false
    }
  }
}
