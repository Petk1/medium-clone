import { ChangeDetectionStrategy, Component, OnInit, inject } from "@angular/core";
import { NgIf } from "@angular/common";
import { ActivatedRoute } from "@angular/router";
import { Store } from "@ngrx/store";
import { articleActions } from "../../store/actions";
import { selectArticleData, selectError, selectIsLoading } from '../../store/reducers';
import { ArticleAuthorComponent } from '../../../shared/components/article-author/article-author.component';
import { FollowButtonComponent } from "src/app/shared/components/follow-button/follow-button.component";
import { FavoriteArticleButtonComponent } from "src/app/shared/components/favorite-article-button/favorite-article-button.component";
import { TagsComponent } from "src/app/shared/components/tags/tags.component";
import { LoadingComponent } from "src/app/shared/components/loading/loading.component";
import { CommentsComponent } from 'src/app/comments/components/comments.component';
import { selectCurrentUser } from 'src/app/auth/store/reducers';
import { EditArticleButtonComponent } from 'src/app/article/components/edit-article-button/edit-article-button.component';
import { PostCommentComponent } from '../../../post-comment/components/post-comment.component';
import { DeleteArticleButtonComponent } from "../delete-article-button/delete-article-button.component";

@Component({
  selector: 'mc-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgIf,
    ArticleAuthorComponent,
    FollowButtonComponent,
    FavoriteArticleButtonComponent,
    TagsComponent,
    LoadingComponent,
    PostCommentComponent,
    CommentsComponent,
    EditArticleButtonComponent,
    DeleteArticleButtonComponent,
  ]
})
export class ArticleComponent implements OnInit {
  private readonly store = inject(Store);
  readonly slug = inject(ActivatedRoute).snapshot.paramMap.get('slug') ?? '';

  isLoading = this.store.selectSignal(selectIsLoading);
  error = this.store.selectSignal(selectError);
  article = this.store.selectSignal(selectArticleData);
  currentUser = this.store.selectSignal(selectCurrentUser);

  ngOnInit(): void {
    this.store.dispatch(articleActions.getArticle({ slug: this.slug }));
  }
}
