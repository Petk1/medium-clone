import { Injectable, inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, map } from "rxjs";
import { IArticle } from "src/app/shared/types/article.interface";
import { IEditArticleRequest } from "../../types/editArticleRequest.interface";
import { IArticleResponse } from "src/app/shared/types/articleResponse.interface";


@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  private http = inject(HttpClient);

  editArtilce(slug: string, articleRequestData: IEditArticleRequest): Observable<IArticle> {
    const url = `articles/${slug}`;
    return this.http.put<IArticleResponse>(url, articleRequestData)
    .pipe((map(({ article }) => article)));
  }

  deleteArticle(slug: string): Observable<void> {
    const url = `articles/${slug}`;
    return this.http.delete<void>(url);
  }
}
