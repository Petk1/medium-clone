import { ChangeDetectionStrategy, Component, inject } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'mc-edit-article-button',
  template: `
    <button class="primary-btn primary-btn--active" (click)="onEdit()">
      Edit Article
    </button>
  `,
  styles: [`
    button {
      height: 30px;
    }
  `],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditArticleButtonComponent {
  router = inject(Router);
  route = inject(ActivatedRoute);

  onEdit(): void {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }
}
