import { ActivatedRoute, Router } from "@angular/router";
import { TestBed } from "@angular/core/testing";
import { EditArticleButtonComponent } from "./edit-article-button.component";

describe('EditArticleButtonComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  it('should navigate to edit', () => {
    const { componentInstance, mockRouter } = setup();
    componentInstance.onEdit();
    expect(mockRouter.navigate).toHaveBeenCalledOnceWith(['edit'], { relativeTo: componentInstance.route });
  })
})

function setup() {
  const mockRouter = {
    navigate: jasmine.createSpy('navigate'),
  };

  const mockActivatedRoute = {};

  TestBed.configureTestingModule({
    providers: [
      {
        provide: Router,
        useValue: mockRouter
      },
      {
        provide: ActivatedRoute,
        useValue: mockActivatedRoute
      }
    ]
  })

  const fixture = TestBed.createComponent(EditArticleButtonComponent);
  const componentInstance = fixture.componentInstance;

  fixture.detectChanges();

  return {
    componentInstance,
    mockRouter,
  }
}
