import { createFeature, createReducer, on } from '@ngrx/store';
import { IArticleState } from './../types/articleState.interface';
import { articleActions } from './actions';

const initialState: IArticleState = {
  isLoading: false,
  error: null,
  articleData: null,
}

const articleFeature = createFeature({
  name: 'article',
  reducer: createReducer(
    initialState,
    on(articleActions.getArticle, state => ({
      ...state,
      isLoading: true
    })),
    on(articleActions.getArticleSuccess, (state, { article: articleData }) => ({
      ...state,
      isLoading: false,
      articleData,
    })),
    on(articleActions.getArticleFailure, state => ({
      ...state,
      isLoading: false,
    })),
    on(articleActions.editArticleSuccess, (state, { article: articleData }) => ({
      ...state,
      articleData,
    })),
    on(articleActions.deleteArticle, () => ({
      ...initialState
    }))
  )
})

export const {
  name: articleFeatureKey,
  reducer: articleReducer,
  selectIsLoading,
  selectError,
  selectArticleData,
} = articleFeature;
