import { inject } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { switchMap, map, catchError, of, tap } from 'rxjs';
import { articleActions } from './actions';
import { ArticleService as SharedArticleService } from 'src/app/shared/services/article.service';
import { ArticleService } from '../components/service/article.service';

export const getArticleEffect = createEffect((
  actions$ = inject(Actions),
  articleSrv = inject(SharedArticleService),
) => (
  actions$.pipe(
    ofType(articleActions.getArticle),
    switchMap(({ slug }) => (
      articleSrv.getArticle(slug)
      .pipe(
        map(article => articleActions.getArticleSuccess({ article })),
        catchError(({ error: { errors } }) => of(articleActions.getArticleFailure({ error: errors })))
      )
    ))
  )
), {
  functional: true
})

export const getArticleFailureEffect = createEffect((
  actions$ = inject(Actions),
  router = inject(Router),
) => (
  actions$.pipe(
    ofType(articleActions.getArticleFailure),
    tap(() => router.navigateByUrl(`/`)),
    map(({ error }) => {
      throw error;
    })
  )
), {
  functional: true,
  dispatch: false
})

export const editArticleEffect = createEffect((
  actions$ = inject(Actions),
  artilceSrv = inject(ArticleService),
) => (
  actions$.pipe(
    ofType(articleActions.editArticle),
    switchMap(({ slug, articleRequestData }) => (
      artilceSrv.editArtilce(slug, articleRequestData)
      .pipe(
        map(article => articleActions.editArticleSuccess({ article })),
        catchError(error => of(articleActions.getArticleFailure({ error })))
      )
    ))
  )
), {
  functional: true
})

export const editArticleSuccessEffect = createEffect((
  actions$ = inject(Actions),
  router = inject(Router),
) => (
  actions$.pipe(
    ofType(articleActions.editArticleSuccess),
    tap(({ article: { slug } }) => router.navigateByUrl(`/article/${slug}`)),
  )
), {
  functional: true,
  dispatch: false
})

export const deleteArticleEffect = createEffect((
  actions$ = inject(Actions),
  artilceSrv = inject(ArticleService),
) => (
  actions$.pipe(
    ofType(articleActions.deleteArticle),
    switchMap(({ slug }) => (
      artilceSrv.deleteArticle(slug)
      .pipe(
        map(() => articleActions.deleteArticleSuccess()),
        catchError(() => of(articleActions.deleteArticleFailure()))
      )
    ))
  )
), {
  functional: true,
})

export const deleteArticleSuccessEffect = createEffect((
  actions$ = inject(Actions),
  router = inject(Router),
) => (
  actions$.pipe(
    ofType(articleActions.deleteArticleSuccess),
    tap(() => router.navigateByUrl(`/`)),
  )
), {
  functional: true,
  dispatch: false
})
