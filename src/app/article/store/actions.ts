import { createActionGroup, emptyProps, props } from "@ngrx/store";
import { IArticle } from "src/app/shared/types/article.interface";
import { IEditArticleRequest } from "../types/editArticleRequest.interface";

export const articleActions = createActionGroup({
  source: 'article',
  events: {
    'Get article': props<{ slug: string }>(),
    'Get article success': props<{ article: IArticle }>(),
    'Get article failure': props<{ error: any }>(),

    'Delete article': props<{ slug: string }>(),
    'Delete article success': emptyProps(),
    'Delete article failure': emptyProps(),

    'Edit article': props<{ slug: string, articleRequestData: IEditArticleRequest }>(),
    'Edit article success': props<{ article: IArticle }>(),
    'Edit article failure': emptyProps(),
  }
})
