import { DebugElement } from "@angular/core"
import { DatePipe } from "@angular/common"
import { By } from "@angular/platform-browser"
import { ActivatedRoute, NavigationEnd, Router, convertToParamMap } from "@angular/router"
import { TestBed } from "@angular/core/testing"
import { MatDialog } from "@angular/material/dialog"
import { BehaviorSubject, Subject, of } from "rxjs"
import { MockStore, provideMockStore } from "@ngrx/store/testing"
import { commentsActions } from "../store/actions"
import { selectCurrentUser } from "src/app/auth/store/reducers"
import { selectCommentsData, selectError, selectIsLoading } from "../store/reducers"
import { CommentsComponent, LIMIT_COMMENTS } from "./comments.component"
import { LoadingComponent } from "src/app/shared/components/loading/loading.component"
import { ICommentsState } from "../types/commentsState.interface"

describe('CommentsComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  describe('Property isLoading is true', () => {
    it('should display a loading component', () => {
      const { debugEl } = setup(true);
      const loadingDebugEl = debugEl.query(By.directive(LoadingComponent));
      expect(loadingDebugEl).toBeTruthy();
    })
  })

  describe('Property isLoading is false', () => {
    const checkCommentDebugEls = (
      debugEl: DebugElement,
      componentInstance: CommentsComponent,
      datePipe: DatePipe,
      expectedCommponentsNumberOnPage: number,
    ) => {
      const commentDebugEls = debugEl.queryAll(By.css('[data-testingId="comment-container"]'));
      expect(commentDebugEls.length).toBe(expectedCommponentsNumberOnPage);

      commentDebugEls.forEach((commentDebugEl, index) => {
        const comment = componentInstance.commentsDisplayed()[index];

        expect(comment).toBeTruthy();
        if(!comment) return;

        const commentBodyEl = commentDebugEl.query(By.css('[data-testingId="comment-body"]')).nativeElement as HTMLElement;
        const commentAuthorInfoEl = commentDebugEl.query(By.css('[data-testingId="comment-author-info"]')).nativeElement as HTMLElement;

        expect(commentBodyEl.textContent).toContain(comment.body);
        expect(commentAuthorInfoEl.textContent)
          .toContain(`${comment.author.username} ${datePipe.transform(comment.createdAt, 'short')}`);
      })
    }

    it('should dispatch a getComments action on init', () => {
      const { store, componentInstance } = setup();
      const mockStoreDispatchSpy = spyOn(store, 'dispatch').and.callThrough();
      componentInstance.ngOnInit();
      expect(mockStoreDispatchSpy).toHaveBeenCalledOnceWith(commentsActions.getComments({ slug: componentInstance.slug }));
    })

    it('should display a page indicated by a URL param', () => {
      const { debugEl, datePipe, componentInstance } = setup(false, 2, 2);
      checkCommentDebugEls(debugEl, componentInstance, datePipe, 2);
    })

    it('should display appropriate comments on a specific page (pagination)', () => {
      const { fixture, debugEl, datePipe, queryParamMap$, componentInstance } = setup(false, 2);
      const linkDebugEls = debugEl.queryAll(By.css('[data-testingId="page-link"]'));

      const checkAllPage = (pageDebugEls: DebugElement[]) => {
        pageDebugEls.forEach((_, pageIndex) => {
          queryParamMap$.next(convertToParamMap({ page: pageIndex + 1 }));
          fixture.detectChanges();
          checkCommentDebugEls(debugEl, componentInstance, datePipe, 2)
        })
      }

      checkAllPage(linkDebugEls);
      checkAllPage(linkDebugEls.reverse());
    })

    it('should open a confirmation dialog on delete', () => {
      const { mockMatDialog, componentInstance, store } = setup(false, 3);
      const mockStoreDispatchSpy = spyOn(store, 'dispatch').and.callThrough();
      const afterClosed$ = new Subject<boolean>()
      const mockMatDialogRef = {
        afterClosed: () => afterClosed$,
        close: jasmine.createSpy('close').and.callFake(() => {
          afterClosed$.next(true);
        })
      }
      mockMatDialog.open.and.returnValue(mockMatDialogRef);

      const checkCommentDeletion = (commentId: number) => {
        componentInstance.onDelete(commentId);
        mockMatDialogRef.close();
        expect(mockStoreDispatchSpy).toHaveBeenCalledWith(commentsActions.deleteComment({ slug: componentInstance.slug, commentId }));
      }

      checkCommentDeletion(1);
      checkCommentDeletion(2);
      checkCommentDeletion(3);
    })
  })
})

function setup(isLoading = false, limitComments = 20, currentPage = 1) {
  const queryParamMap$ = new BehaviorSubject(convertToParamMap({ page: currentPage }))

  const mockActivatedRoute = {
    queryParamMap: queryParamMap$,
  }

  const mockRouter = {
    url: 'url-test',
    events: of(new NavigationEnd(1, 'url', 'urlAfterRedirects')),
    navigate: jasmine.createSpy('navigate'),
  };

  const mockMatDialog = {
    open: jasmine.createSpy('open')
  }

  TestBed.configureTestingModule({
    providers: [
      {
        provide: LIMIT_COMMENTS,
        useValue: limitComments,
      },
      {
        provide: ActivatedRoute,
        useValue: mockActivatedRoute,
      },
      {
        provide: MatDialog,
        useValue: mockMatDialog,
      },
      {
        provide: Router,
        useValue: mockRouter
      },
      provideMockStore({
        initialState: {},
        selectors: [
          { selector: selectIsLoading, value: isLoading },
          { selector: selectError, value: null },
          { selector: selectCommentsData, value: INITIAL_STATE.commentsData },
          { selector: selectCurrentUser, value: {} },
        ]
      })
    ]
  })

  const fixture = TestBed.createComponent(CommentsComponent);
  fixture.componentRef.setInput('slug', 'test-passed-slug');
  const componentInstance = fixture.componentInstance;
  const debugEl = fixture.debugElement;
  const datePipe = new DatePipe('en');
  const store = TestBed.inject(MockStore);

  fixture.detectChanges();

  return {
    fixture,
    debugEl,
    componentInstance,
    datePipe,
    queryParamMap$,
    store,
    mockMatDialog,
  }
}

const INITIAL_STATE: ICommentsState = {
  isLoading: false,
  error: null,
  commentsData: [
    {
      "id": 99695,
      "createdAt": "2023-11-06T14:59:45.245Z",
      "updatedAt": "2023-11-06T14:59:45.245Z",
      "body": "3",
      "author": {
        "username": "ABC_123_ABC",
        "bio": "abcdefghijklmnollko",
        "image": "https://api.realworld.io/images/smiley-cyrus.jpeg",
        "following": false
      }
    },
    {
      "id": 99696,
      "createdAt": "2023-11-06T15:00:07.271Z",
      "updatedAt": "2023-11-06T15:00:07.271Z",
      "body": "3",
      "author": {
        "username": "ABC_123_ABC",
        "bio": "abcdefghijklmnollko",
        "image": "https://api.realworld.io/images/smiley-cyrus.jpeg",
        "following": false
      }
    },
    {
      "id": 99735,
      "createdAt": "2023-11-06T17:15:36.984Z",
      "updatedAt": "2023-11-06T17:15:36.984Z",
      "body": "sdasdas",
      "author": {
        "username": "ABC_123_ABC",
        "bio": "abcdefghijklmnollko",
        "image": "https://api.realworld.io/images/smiley-cyrus.jpeg",
        "following": false
      }
    },
    {
      "id": 99737,
      "createdAt": "2023-11-06T17:18:38.372Z",
      "updatedAt": "2023-11-06T17:18:38.372Z",
      "body": "asdasd",
      "author": {
        "username": "ABC_123_ABC",
        "bio": "abcdefghijklmnollko",
        "image": "https://api.realworld.io/images/smiley-cyrus.jpeg",
        "following": false
      }
    }
  ]
}
