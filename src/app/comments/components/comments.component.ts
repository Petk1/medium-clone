import { ChangeDetectionStrategy, Component, Input, inject, OnInit, computed, InjectionToken } from "@angular/core";
import { DatePipe, NgFor, NgIf } from "@angular/common";
import { Store } from "@ngrx/store";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { selectCommentsData, selectError, selectIsLoading } from "../store/reducers";
import { selectCurrentUser } from "src/app/auth/store/reducers";
import { commentsActions } from "../store/actions";
import { IInitConfirmDialogData } from "src/app/shared/types/initConfirmDialogData.interface";
import { LoadingComponent } from "src/app/shared/components/loading/loading.component";
import { ConfirmDialogComponent } from "src/app/shared/components/confirm-dialog/confirm-dialog.component";
import { PaginationComponent } from "src/app/shared/components/pagination/pagination.component";
import { ActivatedRoute, NavigationEnd, ParamMap, Router } from "@angular/router";
import { filter, map, pairwise, tap } from "rxjs/operators";
import { environment } from 'src/environments/environment';
import { getPageFromParamsMap } from 'src/app/shared/utils/get-page-from-params-map';
import { getUrlItem } from "src/app/shared/utils/get-url-item";

export const LIMIT_COMMENTS = new InjectionToken(
  'limit comments on one page',
  {
    providedIn: 'root',
    factory: () => environment.commentsLimit
  }
)


@Component({
  selector: 'mc-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgIf,
    NgFor,
    LoadingComponent,
    DatePipe,
    PaginationComponent,
  ],
})
export class CommentsComponent implements OnInit {
  @Input({ required: true }) slug!: string;

  private readonly store = inject(Store);
  private readonly matDialog = inject(MatDialog);
  private readonly router = inject(Router);
  private readonly activatedRoute = inject(ActivatedRoute);

  readonly isLoading = this.store.selectSignal(selectIsLoading);
  readonly error = this.store.selectSignal(selectError);
  readonly allComments = this.store.selectSignal(selectCommentsData);
  readonly currentUser = this.store.selectSignal(selectCurrentUser);

  readonly pageLimit = inject(LIMIT_COMMENTS);

  readonly currentPageSig = toSignal(
    this.activatedRoute.queryParamMap
      .pipe(map(params => getPageFromParamsMap(params, 1)))
  );

  readonly commentsDisplayed = computed(() => {
    const allComments = this.allComments();
    const pageFromUrl = this.currentPageSig();

    if(!allComments || !allComments.length) return [];

    const currentPage = pageFromUrl || 1;
    const end = (currentPage * this.pageLimit) || this.pageLimit;
    return allComments.slice(end - this.pageLimit, end);
  });

  readonly baseUrlSig = toSignal(
    this.router.events
      .pipe(
        filter((routerEvent): routerEvent is NavigationEnd => routerEvent instanceof NavigationEnd),
        map(() => getUrlItem(this.router.url, 0) || ''),
      ), {
        initialValue: getUrlItem(this.router.url, 0) || ''
      }
  )

  ngOnInit(): void {
    this.store.dispatch(commentsActions.getComments({ slug: this.slug }));
  }

  onDelete(commentId: number): void {
    const dialogConfig = new MatDialogConfig<IInitConfirmDialogData>();
    dialogConfig.width = '440px';
    dialogConfig.height = '275px';
    dialogConfig.closeOnNavigation = true;
    dialogConfig.position = {
      top: '125px',
    }
    dialogConfig.data = {
      title: 'Confirm deletion',
      content: 'Are you sure you want to delete this comment?'
    }

    this.matDialog.open(ConfirmDialogComponent, dialogConfig)
      .afterClosed()
      .subscribe((decision: boolean) => {
        if(decision) {
          this.store.dispatch(commentsActions.deleteComment({ slug: this.slug, commentId }));
        }
      })
  }
}
