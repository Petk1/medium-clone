import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { IComment } from 'src/app/shared/types/comment.interface';
import { IGetCommentsResponse } from '../types/getCommentsResponse.interface';

@Injectable({
  providedIn: 'root',
})
export class CommentsService {
  private http = inject(HttpClient);

  getComments(slug: string): Observable<IComment[]> {
    const url = `articles/${slug}/comments`;
    return this.http.get<IGetCommentsResponse>(url)
    .pipe(map(({ comments }) => comments?.reverse()));
  }

  deleteComments(slug: string, commentId: number): Observable<{}> {
    const url = `articles/${slug}/comments/${commentId}`;
    return this.http.delete<{}>(url);
  }
}
