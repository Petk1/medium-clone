import { IComment } from "src/app/shared/types/comment.interface";

export interface IGetCommentsResponse {
  comments: IComment[];
}
