import { IComment } from "src/app/shared/types/comment.interface";

export interface ICommentsState {
  isLoading: boolean,
  error: string | null,
  commentsData: IComment[] | null | undefined,
}
