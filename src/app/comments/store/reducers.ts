import { createFeature, createReducer, on } from '@ngrx/store';
import { routerNavigatedAction } from '@ngrx/router-store';
import { ICommentsState } from '../types/commentsState.interface';
import { commentsActions } from './actions';

const initialState: ICommentsState = {
  isLoading: false,
  error: null,
  commentsData: undefined,
}

const commentsFeature = createFeature({
  name: 'comments',
  reducer: createReducer(
    initialState,
    on(commentsActions.getComments, state => ({ ...state, isLoading: true })),
    on(commentsActions.getCommentsSuccess, (state, { comments: commentsData }) => ({
      ...state,
      isLoading: false,
      commentsData,
    })),
    on(commentsActions.getCommentsFailure, state => ({
      ...state,
      isLoading: false,
    })),
    on(commentsActions.addComment, (state, { comment }) => ({
      ...state,
      commentsData: state.commentsData ? [comment, ...state.commentsData] : [comment],
    })),
    on(commentsActions.deleteCommentSuccess, (state, { commentId }) => ({
      ...state,
      commentsData: state.commentsData?.filter(({ id }) => id !== commentId),
    })),
    on(routerNavigatedAction, (state, { payload }) => {
      if(payload.event.url.includes('/article/')) {
        return state;
      }

      return initialState;
    })
  ),
})

export const {
  name: commentsFeatureKey,
  reducer: commentsReducer,
  selectIsLoading,
  selectError,
  selectCommentsData,
} = commentsFeature;
