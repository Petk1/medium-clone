import { createActionGroup, emptyProps, props } from "@ngrx/store";
import { IComment } from "src/app/shared/types/comment.interface";

export const commentsActions = createActionGroup({
  source: 'comments',
  events: {
    'Get comments': props<{ slug: string }>(),
    'Get comments success': props<{ comments: IComment[] }>(),
    'Get comments failure': props<{ error: any }>(),

    'Add comment': props<{ comment: IComment }>(),

    'Delete comment': props<{ slug: string, commentId: number }>(),
    'Delete comment success': props<{ commentId: number }>(),
    'Delete comment failure': props<{ error: any }>(),
  }
})
