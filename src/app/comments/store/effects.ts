import { inject } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { commentsActions } from './actions';
import { CommentsService } from '../services/comments.service';
import { catchError, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

export const getCommentsEffect = createEffect((
  actions$ = inject(Actions),
  commentsSrv = inject(CommentsService),
) => (
  actions$.pipe(
    ofType(commentsActions.getComments),
    switchMap(({ slug }) => (
      commentsSrv.getComments(slug)
      .pipe(
        map(comments => commentsActions.getCommentsSuccess({ comments })),
        catchError(error => of(commentsActions.getCommentsFailure(error)))
      )
    ))
  )
), {
  functional: true
})

export const getCommentsFailureEffect = createEffect((
  actions$ = inject(Actions),
) => (
  actions$.pipe(
    ofType(commentsActions.getCommentsFailure),
    map(({ error }) => {
      throw error;
    })
  )
), {
  functional: true,
  dispatch: false,
})

export const deleteCommentEffect = createEffect((
  actions$ = inject(Actions),
  commentsSrv = inject(CommentsService),
) => (
  actions$.pipe(
    ofType(commentsActions.deleteComment),
    switchMap(({ slug, commentId }) => (
      commentsSrv.deleteComments(slug, commentId)
      .pipe(
        map(() => commentsActions.deleteCommentSuccess({ commentId })),
        catchError(error => of(commentsActions.deleteCommentFailure(error)))
      )
    ))
  )
), {
  functional: true
})

export const deleteCommentFailureEffect = createEffect((
  actions$ = inject(Actions),
) => (
  actions$.pipe(
    ofType(commentsActions.deleteCommentFailure),
    map(({ error }) => {
      throw error;
    })
  )
), {
  functional: true,
  dispatch: false,
})
