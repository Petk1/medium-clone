import { Injectable, inject } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { IRegisterRequest } from "../types/registerRequest.interface";
import { ICurrentUser } from "src/app/shared/types/currentUser.interface";
import { IAuthResponse } from "../types/authResponse.interface";
import { ILoginRequest } from "../types/loginRequest.interface";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private http = inject(HttpClient);

  register(registerData: IRegisterRequest): Observable<ICurrentUser> {
    const url = `users`;
    return this.http.post<IAuthResponse>(url, registerData)
    .pipe(map(this.getUser));
  }

  login(loginData: ILoginRequest): Observable<ICurrentUser> {
    const url = `users/login`;
    return this.http.post<IAuthResponse>(url, loginData)
    .pipe(map(this.getUser));
  }

  getCurrentUser(): Observable<ICurrentUser> {
    const url = `/user`;
    return this.http.get<IAuthResponse>(url)
    .pipe(map(this.getUser));
  }

  private getUser = ({ user }: IAuthResponse) => user;
}
