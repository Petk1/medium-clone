import { Route } from "@angular/router";
import { RegistrationComponent } from "./components/registration/registration.component";
import { LoginComponent } from "./components/login/login.component";

export const registrationRoutes: Route[] = [
  {
    path: '',
    component: RegistrationComponent,
  },
]

export const loginRoutes: Route[] = [
  {
    path: '',
    component: LoginComponent,
  },
]
