import { ChangeDetectionStrategy, Component, inject } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormBuilder, Validators } from "@angular/forms";
import { ReactiveFormsModule } from '@angular/forms';
import { RouterLink } from "@angular/router";
import { Store } from "@ngrx/store";
import { authActions } from "../../store/actions";
import { emailValidator } from "src/app/shared/form-validators/email.validator";
import { ILoginRequest } from "../../types/loginRequest.interface";
import { selectIsSubmitting } from "../../store/reducers";

@Component({
  selector: 'mc-login',
  templateUrl: './login.component.html',
  styleUrls: ['./../../styles/default-page.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterLink,
  ]
})
export class LoginComponent {
  private readonly store = inject(Store);
  readonly isSubmittingSig = this.store.selectSignal(selectIsSubmitting);

  form = inject(FormBuilder).nonNullable.group({
    email: ['', [Validators.required, emailValidator()]],
    password: ['', [Validators.required, Validators.minLength(8)]],
  });

  onSubmit(): void {
    const request: ILoginRequest = {
      user: this.form.getRawValue(),
    }

    this.store.dispatch(authActions.login({ request }));
  }

}
