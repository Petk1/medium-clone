import { TestBed } from "@angular/core/testing"
import { RouterTestingModule } from "@angular/router/testing"
import { MockStore, provideMockStore } from "@ngrx/store/testing"
import { authActions } from "../../store/actions"
import { LoginComponent } from "./login.component"
import { ILoginRequest } from "../../types/loginRequest.interface"

describe('LoginComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  it('should dispatch the login action', () => {
    const { componentInstance, store } = setup();
    const mockStoreDispatchSpy = spyOn(store, 'dispatch').and.callThrough();
    componentInstance.form.setValue({
      email: 'mockEmail@gmail.com',
      password: 'MockPassword123',
    })
    const request: ILoginRequest = {
      user: componentInstance.form.getRawValue(),
    }

    componentInstance.onSubmit();

    expect(mockStoreDispatchSpy).toHaveBeenCalledOnceWith(authActions.login({ request }));
  })

  it('should set a form to invalid if incorrect data is entered', () => {
    const { componentInstance } = setup();

    componentInstance.form.setValue({
      email: '',
      password: 'MockPassword123',
    });
    expect(componentInstance.form.invalid).toBe(true);

    componentInstance.form.setValue({
      email: 'mockEmailgmail.com',
      password: '',
    });
    expect(componentInstance.form.invalid).toBe(true);

    componentInstance.form.setValue({
      email: '',
      password: '',
    });
    expect(componentInstance.form.invalid).toBe(true);

    componentInstance.form.setValue({
      email: 'mockEmailgmail.com',
      password: 'MockPassword123',
    });
    expect(componentInstance.form.invalid).toBe(true);

    componentInstance.form.setValue({
      email: 'mockEmail@gmail.com',
      password: 'Mock1',
    });
    expect(componentInstance.form.invalid).toBe(true);

    componentInstance.form.setValue({
      email: 'mockEmail@gmail.com',
      password: 'MockPassword113',
    });
    expect(componentInstance.form.invalid).toBe(false);
  })
})

function setup() {
  TestBed.configureTestingModule({
    imports: [RouterTestingModule],
    providers: [provideMockStore()]
  })

  const fixture = TestBed.createComponent(LoginComponent);
  const componentInstance = fixture.componentInstance;
  const store = TestBed.inject(MockStore);

  fixture.detectChanges();

  return {
    componentInstance,
    store,
  }
}
