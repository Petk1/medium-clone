import { createFeature, createReducer, on } from "@ngrx/store";
import { IAuthState } from "../types/authState.interface";
import { authActions } from "./actions";
import { routerNavigatedAction } from "@ngrx/router-store";

const initialState: IAuthState = {
  isSubmitting: false,
  isLoading: false,
  currentUser: undefined,
  validationErrors: null,
}

const authFeature = createFeature({
  name: 'auth',
  reducer: createReducer(
    initialState,
    on(authActions.register, state => ({
      ...state,
      isSubmitting: true,
      validationErrors: null
    })),
    on(authActions.registerSuccess, (state, { currentUser }) => ({
      ...state,
      isSubmitting: false,
      currentUser,
    })),
    on(authActions.registerFailure, (state, { errors: validationErrors }) => ({
      ...state,
      isSubmitting: false,
      validationErrors,
    })),
    on(authActions.login, state => ({
      ...state,
      isSubmitting: true,
      validationErrors: null
    })),
    on(authActions.loginSuccess, (state, { currentUser }) => ({
      ...state,
      isSubmitting: false,
      currentUser,
    })),
    on(authActions.loginFailure, (state, { errors: validationErrors }) => ({
      ...state,
      isSubmitting: false,
      validationErrors,
    })),
    on(authActions.getCurrentUserSuccess, (state, { currentUser }) => ({
      ...state,
      isLoading: false,
      currentUser,
    })),
    on(authActions.getCurrentUserFailure, state => ({
      ...state,
      isLoading: false,
      currentUser: null,
    })),
    on(authActions.logout, state => ({
      ...state,
      isLoading: false,
      currentUser: null,
    })),
    on(routerNavigatedAction, state => ({ ...state, validationErrors: null })),
  )
})

export const {
  name: authFeatureKey,
  reducer: authReducer,
  selectIsSubmitting,
  selectIsLoading,
  selectCurrentUser,
  selectValidationErrors,
} = authFeature;
