import { inject } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, map, switchMap, tap, of, throwError } from "rxjs";
import { authActions } from "./actions";
import { AuthService } from "../services/auth.service";
import { UserTokenService } from "src/app/shared/services/user-token.service";
import { Store } from "@ngrx/store";
import { Router } from "@angular/router";

export const registerEffect = createEffect((
  actions$ = inject(Actions),
  store = inject(Store),
  authSrv = inject(AuthService),
  tokenSrv = inject(UserTokenService),
) => (
  actions$.pipe(
    ofType(authActions.register),
    switchMap(({ request }) => authSrv.register(request).pipe(
      tap(({ token }) => tokenSrv.set('accessToken', token)),
      map(currentUser => authActions.registerSuccess({ currentUser })),
      catchError(({ error: { errors } }) => {
        store.dispatch(authActions.registerFailure({ errors }));
        return throwError(() => errors);
      })
    ))
  )
), { functional: true })

export const registerSuccessEffect = createEffect((
  actions$ = inject(Actions),
  router = inject(Router),
) => (
  actions$.pipe(
    ofType(authActions.registerSuccess),
    map(() => router.navigateByUrl('/'))
  )
), {
  functional: true,
  dispatch: false
})

export const loginEffect = createEffect((
  actions$ = inject(Actions),
  store = inject(Store),
  authSrv = inject(AuthService),
  tokenSrv = inject(UserTokenService),
) => (
  actions$.pipe(
    ofType(authActions.login),
    switchMap(({ request }) => authSrv.login(request).pipe(
      tap(({ token }) => tokenSrv.set('accessToken', token)),
      map(currentUser => authActions.loginSuccess({ currentUser })),
      catchError(({ error: { errors } }) => {
        store.dispatch(authActions.loginFailure({ errors }));
        return throwError(() => errors);
      })
    ))
  )
), { functional: true })

export const loginSuccessEffect = createEffect((
  actions$ = inject(Actions),
  router = inject(Router),
) => (
  actions$.pipe(
    ofType(authActions.loginSuccess),
    map(() => router.navigateByUrl('/'))
  )
), {
  functional: true,
  dispatch: false
})

export const getCurrentUserEffect = createEffect((
  actions$ = inject(Actions),
  authSrv = inject(AuthService),
  token = inject(UserTokenService).get('accessToken'),
) => (
  actions$.pipe(
    ofType(authActions.getCurrentUser),
    switchMap(() => {
      if(!token) {
        return of(authActions.getCurrentUserFailure());
      }

      return authSrv.getCurrentUser().pipe(
        map(currentUser => authActions.getCurrentUserSuccess({ currentUser })),
        catchError(() => of(authActions.getCurrentUserFailure()))
      )
    })
  )
), { functional: true })

export const logoutEffect = createEffect((
  actions$ = inject(Actions),
  tokenSrv = inject(UserTokenService),
  router = inject(Router),
) => (
  actions$.pipe(
    ofType(authActions.logout),
    tap(() => tokenSrv.remove('accessToken')),
    tap(() => router.navigateByUrl('/login'))
  )
), {
  functional: true,
  dispatch: false
})
