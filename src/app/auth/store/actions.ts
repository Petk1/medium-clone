import { createActionGroup, emptyProps, props } from "@ngrx/store";
import { IRegisterRequest } from "../types/registerRequest.interface";
import { ICurrentUser } from "src/app/shared/types/currentUser.interface";
import { IBackendErrors } from "src/app/shared/types/backendErrors.interface";
import { ILoginRequest } from "../types/loginRequest.interface";

export const authActions = createActionGroup({
  source: 'auth',
  events: {
    'Register': props<{ request: IRegisterRequest }>(),
    'Register success': props<{ currentUser: ICurrentUser }>(),
    'Register failure': props<{ errors: IBackendErrors }>(),

    'Login': props<{ request: ILoginRequest }>(),
    'Login success': props<{ currentUser: ICurrentUser }>(),
    'Login failure': props<{ errors: IBackendErrors }>(),

    'Get current user': emptyProps(),
    'Get current user success': props<{ currentUser: ICurrentUser }>(),
    'Get current user failure': emptyProps(),

    'Logout': emptyProps(),
  }
})
