import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: 'mc-page-not-found',
  template: `
    <p>404</p>
    <p>Page not found</p>
  `,
  styleUrls: ['./page-not-found.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PageNotFoundComponent {}
