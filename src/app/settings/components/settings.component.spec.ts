import { TestBed } from "@angular/core/testing";
import { MockStore, provideMockStore } from "@ngrx/store/testing";
import { selectCurrentUser } from "src/app/auth/store/reducers";
import { SettingsComponent } from "./settings.component";
import { ICurrentUser } from "src/app/shared/types/currentUser.interface";
import { updateUserActions } from "../store/actions";
import { IUpdateUserRequest } from "../types/updateUserRequest.interface";
import { Router } from "@angular/router";

describe('SettingsComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  it('should set the stored data to the form', () => {
    const { componentInstance } = setup();
    const { image, username, bio, email } = componentInstance.form.value;
    expect(image).toContain(MOCK_USER_DATA.image);
    expect(username).toContain(MOCK_USER_DATA.username);
    expect(bio).toContain(MOCK_USER_DATA.bio);
    expect(email).toContain(MOCK_USER_DATA.email);
  })

  describe('Submit', () => {
    it('should dispatch an updateUser action and navigate to the profile view', () => {
      const { componentInstance, store, mockRouter } = setup();
      const mockStoreDispatchSpy = spyOn(store, 'dispatch').and.callThrough();
      componentInstance.onSubmit();
      const updateUserRequestData: IUpdateUserRequest = {
        user: {
          ...componentInstance.form.getRawValue(),
          token: componentInstance.currentUser()!.token
        },
      }

      expect(mockStoreDispatchSpy).toHaveBeenCalledOnceWith(updateUserActions.updateUser({ updateUserRequestData }));
      expect(mockRouter.navigateByUrl).toHaveBeenCalledOnceWith(`/profile/${updateUserRequestData.user.username}/my-posts`);
    })

    it('should prevent update the user data if no token and navigate to the login view', () => {
      const { componentInstance, store, mockRouter } = setup({
        ...MOCK_USER_DATA,
        token: '',
      });
      const mockStoreDispatchSpy = spyOn(store, 'dispatch').and.callThrough();
      componentInstance.onSubmit();
      const updateUserRequestData: IUpdateUserRequest = {
        user: {
          ...componentInstance.form.getRawValue(),
          token: componentInstance.currentUser()!.token
        },
      }

      expect(mockStoreDispatchSpy).not.toHaveBeenCalled();
      expect(mockRouter.navigateByUrl).toHaveBeenCalledOnceWith('/login');
    })
  })

  it('should return an expected value from a canDeactivate method', () => {
    const { componentInstance } = setup();

    expect(componentInstance.canDeactivate()).toBe(true);

    componentInstance['isFormSubmitted'] = true;
    expect(componentInstance.canDeactivate()).toBe(true);

    componentInstance['isFormSubmitted'] = false;
    expect(componentInstance.canDeactivate()).toBe(true);

    componentInstance['isFormSubmitted'] = false;
    componentInstance.form.markAsDirty();
    expect(componentInstance.canDeactivate()).toBe(false);
  })
})

function setup(currentUser: ICurrentUser = MOCK_USER_DATA) {
  const mockRouter = {
    navigateByUrl: jasmine.createSpy('navigateByUrl'),
  };

  TestBed.configureTestingModule({
    providers: [
      {
        provide: Router,
        useValue: mockRouter
      },
      provideMockStore({
        selectors: [
          { selector: selectCurrentUser, value: currentUser }
        ]
      })
    ]
  })

  const fixture = TestBed.createComponent(SettingsComponent);
  const componentInstance = fixture.componentInstance;
  const store = TestBed.inject(MockStore);

  fixture.detectChanges();

  return {
    fixture,
    componentInstance,
    store,
    mockRouter,
  }
}

const MOCK_USER_DATA: ICurrentUser = {
  email: 'testemail@gmail.com',
  token: 'eyMockToken123',
  username: 'Test username',
  bio: 'Short test bio',
  image: 'mock-image-link'
}
