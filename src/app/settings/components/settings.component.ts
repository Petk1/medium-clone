import { ChangeDetectionStrategy, Component, effect, inject } from "@angular/core";
import { NgClass } from "@angular/common";
import { Router } from "@angular/router";
import { FormBuilder, ReactiveFormsModule, Validators } from "@angular/forms";
import { Store } from "@ngrx/store";
import { selectCurrentUser } from "src/app/auth/store/reducers";
import { updateUserActions } from "../store/actions";
import { emailValidator } from "src/app/shared/form-validators/email.validator";
import { ICanComponentDeactivate } from "src/app/core/guards/types/canComponentDeactivate.interface";
import { IUpdateUserRequest } from "../types/updateUserRequest.interface";

@Component({
  selector: 'mc-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['../../../assets/scss/bases/base-form.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgClass,
    ReactiveFormsModule,
  ]
})
export class SettingsComponent implements ICanComponentDeactivate {
  form = inject(FormBuilder).nonNullable.group({
    image: ['', Validators.required],
    username: ['', [Validators.required]],
    bio: [''],
    email: ['', [Validators.required, emailValidator()]],
    password: [''],
  });

  private readonly store = inject(Store);
  private router = inject(Router);
  readonly currentUser = this.store.selectSignal(selectCurrentUser);
  private isFormSubmitted = false;

  constructor() {
    effect(() => {
      const currentUser = this.currentUser();
      if(currentUser) {
        const { image, username, bio, email } = currentUser;

        this.form.setValue({
          image: image || '',
          username,
          bio: bio || '',
          email,
          password: ''
        });

        // after updating reset these properites
        if(this.isFormSubmitted){
          this.isFormSubmitted = false;
          this.form.markAsPristine();
        }
      }
    });
  }

  onSubmit(): void {
    const token = this.currentUser()?.token;
    if(!token) {
      this.router.navigateByUrl('/login');
      return;
    }

    const updateUserRequestData: IUpdateUserRequest = {
      user: {
        ...this.form.getRawValue(),
        token,
      },
    }

    this.isFormSubmitted = true;
    this.store.dispatch(updateUserActions.updateUser({ updateUserRequestData }));
    this.router.navigateByUrl(`/profile/${updateUserRequestData.user.username}/my-posts`);
  }

  canDeactivate(): boolean {
    if(this.isFormSubmitted) return true;

    return !this.form.dirty;
  };

}
