import { Route } from "@angular/router";
import { provideEffects } from "@ngrx/effects";
import { SettingsComponent } from "./components/settings.component";
import { UpdateUserService } from "./services/update-user.service";
import * as updateUserEffect from './store/effects';
import { unsavedChangesGuard } from "../core/guards/unsaved-changes.guard";

export const routes: Route[] = [
  {
    path: '',
    component: SettingsComponent,
    providers: [
      provideEffects(updateUserEffect),
      UpdateUserService,
    ],
    canDeactivate: [unsavedChangesGuard],
  },
]
