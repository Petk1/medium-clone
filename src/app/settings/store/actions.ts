import { createActionGroup, emptyProps, props } from "@ngrx/store";
import { IUpdateUserRequest } from "../types/updateUserRequest.interface";
import { ICurrentUser } from "src/app/shared/types/currentUser.interface";

export const updateUserActions = createActionGroup({
  source: 'Update User',
  events: {
    'Update user': props<{ updateUserRequestData: IUpdateUserRequest }>(),
    'Update user success': props<{ user: ICurrentUser }>(),
    'Update user failure': emptyProps(),
  }
})
