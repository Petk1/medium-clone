import { inject } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { UpdateUserService } from "../services/update-user.service";
import { updateUserActions } from "./actions";
import { catchError, map, switchMap, tap } from "rxjs/operators";
import { of } from "rxjs";
import { authActions } from "src/app/auth/store/actions";


export const updateUserEffect = createEffect((
  actions$ = inject(Actions),
  updateUserSrv = inject(UpdateUserService),
) => (
  actions$.pipe(
    ofType(updateUserActions.updateUser),
    switchMap(({ updateUserRequestData }) => (
      updateUserSrv.updateUser(updateUserRequestData)
      .pipe(
        map(user => updateUserActions.updateUserSuccess({ user })),
        catchError(() => of(updateUserActions.updateUserFailure()))
      )
    ))
  )
), {
  functional: true,
})

export const updateUserSuccessEffect = createEffect((
  actions$ = inject(Actions),
) => (
  actions$.pipe(
    ofType(updateUserActions.updateUserSuccess),
    map(({ user }) => authActions.getCurrentUserSuccess({ currentUser: user })),
  )
), {
  functional: true,
})
