import { ICurrentUser } from "src/app/shared/types/currentUser.interface";

export interface IUpdateUserResponse {
  user: ICurrentUser;
}
