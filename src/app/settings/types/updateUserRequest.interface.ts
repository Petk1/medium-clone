import { ICurrentUser } from "src/app/shared/types/currentUser.interface";

export interface IUpdateUserRequest {
  user: ICurrentUser & {
    password: string;
  }
}
