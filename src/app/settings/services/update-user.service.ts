import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { IUpdateUserRequest } from '../types/updateUserRequest.interface';
import { IUpdateUserResponse } from '../types/updateUserResponse.interface';
import { ICurrentUser } from 'src/app/shared/types/currentUser.interface';

@Injectable()
export class UpdateUserService {
  private http = inject(HttpClient);

  updateUser(updateUserRequestData: IUpdateUserRequest): Observable<ICurrentUser> {
    const url = 'user';
    return this.http.put<IUpdateUserResponse>(url, updateUserRequestData)
    .pipe((map(({ user }) => user)));
  }
}
