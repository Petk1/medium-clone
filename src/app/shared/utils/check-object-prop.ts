export const checkObjectProp = (data: any, propName: string, typeName: string) => {
  if(!data || typeof data !== 'object') return false;

  return propName in data && typeof data[propName] === typeName;
}
