export const getUrlItem = (url: string, itemNumber: number) => {
  return url.split('?')[itemNumber] || undefined;
}
