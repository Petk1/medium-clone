import { checkObjectProp } from "./check-object-prop";

describe('checkObjectProp Function', () => {
  it('should return true if the property exists', () => {
    expect(checkObjectProp({
      testProperty: 'test property'
    }, 'testProperty', 'string')).toBe(true);
  })

  it('should return false if the property does not exist', () => {
    expect(checkObjectProp({
      testProperty: 'test property'
    }, 'anotherProperty', 'string')).toBe(false);
  })

  it('should return false if the property has a different type', () => {
    expect(checkObjectProp({
      testProperty: 'test property'
    }, 'testProperty', 'boolean')).toBe(false);
  })

  it('should return false if the passed propName and typeName is incorrect', () => {
    expect(checkObjectProp({
      testProperty: 'test property'
    }, 'anotherProperty', 'number')).toBe(false);
  })
})
