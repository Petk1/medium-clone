import { SimpleChange } from "@angular/core";
import { isSimpleChangeChanged } from "./is-simple-change-changed";

describe('isSimpleChangeChanged Function', () => {
  describe('The first change', () => {
    it('should return false when it is the first change', () => {
      const { simpleChange } = setup({
        firstChange: true,
        previousValue: undefined,
        currentValue: 10,
      });

      expect(isSimpleChangeChanged(simpleChange)).toBe(false);
    })
  })

  describe('Not the first change', () => {
    it('should return true when currentValue is different form previousValue', () => {
      const { simpleChange } = setup({
        firstChange: false,
        previousValue: 5,
        currentValue: 10,
      });

      expect(isSimpleChangeChanged(simpleChange)).toBe(true);

      simpleChange.previousValue = 'test1';
      simpleChange.currentValue = 'test2';

      expect(isSimpleChangeChanged(simpleChange)).toBe(true);
    })

    it('should return false when currentValue is the same as previousValue', () => {
      const { simpleChange } = setup({
        firstChange: false,
        previousValue: 5,
        currentValue: 5,
      });

      expect(isSimpleChangeChanged(simpleChange)).toBe(false);

      simpleChange.previousValue = 'test1';
      simpleChange.currentValue = 'test1';

      expect(isSimpleChangeChanged(simpleChange)).toBe(false);
    })
  })
})

function setup(simpleChange: Omit<SimpleChange, 'isFirstChange'>) {
  return {
    simpleChange: { ...simpleChange } as SimpleChange
  }
}
