import { getUrlItem } from "./get-url-item";

describe('getUrlItem Function', () => {
  it('should return the item from the URL when item exists', ( ) => {
    const url = 'https://example.com/page?param1=value1';
    expect(getUrlItem(url, 1)).toEqual('param1=value1');
  })

  it('should return undefined when the item does not exist in the URL', ( ) => {
    const url = 'https://example.com/page';
    expect(getUrlItem(url, 1)).toBeUndefined();
  })

  it('should return undefined when the URL is empty', () => {
    const url = '';
    expect(getUrlItem(url, 1)).toBeUndefined();
  })
})
