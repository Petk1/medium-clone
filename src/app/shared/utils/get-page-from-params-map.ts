import { ParamMap } from "@angular/router";

export const getPageFromParamsMap = (params: ParamMap, defaultValue: number | undefined = undefined) => {
  return Number(params?.get('page')) || defaultValue;
}
