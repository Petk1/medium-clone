import { Params, convertToParamMap } from "@angular/router"
import { getPageFromParamsMap } from "./get-page-from-params-map"

describe('getPageFromParams Function', () => {
  it('should return page number from params when present', () => {
    const { paramMap } = setup({ page: '5' });
    expect(getPageFromParamsMap(paramMap)).toEqual(5);
  })

  it('should return default value when page param is not present', () => {
    const { paramMap } = setup({});
    expect(getPageFromParamsMap(paramMap, 1)).toEqual(1);
  })

  it('should return default value when page number is 0', () => {
    const { paramMap } = setup({ page: '0' });
    expect(getPageFromParamsMap(paramMap, 1)).toEqual(1);
  })

  it('should return undefined when page param is not present and defaultValue is not passed', () => {
    const { paramMap } = setup({});
    expect(getPageFromParamsMap(paramMap)).toBeUndefined();
  })

  it('should return undefined when page number is 0 and defaultValue is not passed', () => {
    const { paramMap } = setup({ page: '0' });
    expect(getPageFromParamsMap(paramMap)).toBeUndefined();
  })
})

function setup(params: Params) {
  return {
    paramMap: convertToParamMap(params)
  }
}
