import { SimpleChange } from "@angular/core";

export const isSimpleChangeChanged = (simpleChange: SimpleChange): boolean => {
  return !simpleChange?.firstChange && simpleChange?.currentValue !== simpleChange?.previousValue;
}
