import { FormControl } from "@angular/forms"
import { emailValidator } from "./email.validator";

describe('Email Validator', () => {
  it('should return null if a control value is empty', () => {
    const control = getFormControl('');
    expect(emailValidator()(control)).toBeNull();
  })

  it('should return null if an email is valid', () => {
    const control = getFormControl('testemail12@gmail.com');
    expect(emailValidator()(control)).toBeNull();
  })

  it('should return the error object if an email is invalid', () => {
    let control = getFormControl('testemail12@com');
    expect(emailValidator()(control)).toEqual({ emailRegex: true });

    control = getFormControl('testemail12@gmail.com');
    expect(emailValidator()(control)).toBeNull();

    control = getFormControl('testemail12.com');
    expect(emailValidator()(control)).toEqual({ emailRegex: true });

    control = getFormControl('testemail12@.com');
    expect(emailValidator()(control)).toEqual({ emailRegex: true });

    control = getFormControl('@gmail.com');
    expect(emailValidator()(control)).toEqual({ emailRegex: true });
  })
})

const getFormControl = (controlValue: string) => new FormControl(controlValue);
