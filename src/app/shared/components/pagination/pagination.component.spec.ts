import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { By } from "@angular/platform-browser";
import { TestBed } from "@angular/core/testing";
import { PaginationComponent } from "./pagination.component";

describe('PaginationComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  it('should set an appropriate number of pages', () => {
    const { componentInstance, paginationDebugEl } = setup(20, 5);

    expect(componentInstance.pagesCount).toBe(4);

    const optionsDebugEl = paginationDebugEl.query(By.css('[data-testingId="pagination"]'));
    const liEls = optionsDebugEl.queryAll(By.css('li'));

    expect(liEls.length).toBe(4);
  })

  it('should call onChangePage when a page is changed', () => {
    const { componentInstance, paginationDebugEl } = setup();
    spyOn(componentInstance, 'onChangePage');
    const optionsDebugEl = paginationDebugEl.query(By.css('[data-testingId="pagination"]'));
    const liEls = optionsDebugEl.queryAll(By.css('li'));

    liEls[2].query(By.css('a')).triggerEventHandler('click');
    expect(componentInstance.onChangePage).toHaveBeenCalledWith(3);

    liEls[1].query(By.css('a')).triggerEventHandler('click');
    expect(componentInstance.onChangePage).toHaveBeenCalledWith(2);
  })

  it('should navigate to the correct page when onChangePage is called', () => {
    const { paginationDebugEl, mockRouter } = setup(15, 3, 1, 'test/url');
    const optionsDebugEl = paginationDebugEl.query(By.css('[data-testingId="pagination"]'));
    const liEls = optionsDebugEl.queryAll(By.css('li'));

    liEls[2].query(By.css('a')).triggerEventHandler('click');
    expect(mockRouter.navigate).toHaveBeenCalledWith(['test/url'], { queryParams: { page: 3 } });

    liEls[1].query(By.css('a')).triggerEventHandler('click');
    expect(mockRouter.navigate).toHaveBeenCalledWith(['test/url'], { queryParams: { page: 2 } });
  })
})

function setup(numberOfComments = 10, pageLimit = 2, currentPage = 1, baseUrl = '/base/url') {
  @Component({
    standalone: true,
    template: `
      <mc-pagination
        [total]="numberOfComments"
        [limit]="pageLimit"
        [url]="baseUrl"
        [currentPage]="currentPage"
      />
    `,
    imports: [
      PaginationComponent
    ]
  })
  class PaginationTestHost {
    numberOfComments = numberOfComments;
    pageLimit = pageLimit;
    baseUrl = baseUrl;
    currentPage = currentPage;
  }

  const mockRouter = {
    navigate: jasmine.createSpy('navigate'),
  };

  TestBed.configureTestingModule({
    providers: [
      {
        provide: Router,
        useValue: mockRouter
      },
    ]
  })

  const fixture = TestBed.createComponent(PaginationTestHost);
  const hostComponentInstance = fixture.componentInstance;
  const paginationDebugEl = fixture.debugElement.query(By.directive(PaginationComponent));
  const componentInstance = paginationDebugEl.componentInstance as PaginationComponent;

  fixture.detectChanges();

  return {
    fixture,
    hostComponentInstance,
    paginationDebugEl,
    componentInstance,
    mockRouter,
  }
}
