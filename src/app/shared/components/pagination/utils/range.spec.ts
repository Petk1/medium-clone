import { range } from "./range"

describe("Range Function", () => {
  it('should get an array of appropriate length', () => {
    expect(range(0, 10).length).toBe(10);
  })
})
