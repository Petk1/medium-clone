import { Component, ChangeDetectionStrategy, Input, OnChanges, OnInit, SimpleChanges, inject } from "@angular/core";
import { NgClass, NgFor, NgIf } from "@angular/common";
import { Router } from "@angular/router";
import { range } from "./utils/range";
import { isSimpleChangeChanged } from "../../utils/is-simple-change-changed";

@Component({
  selector: 'mc-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgIf,
    NgFor,
    NgClass,
  ]
})
export class PaginationComponent implements OnInit, OnChanges {
  @Input() total: number = 0;
  @Input() limit: number = 20;
  @Input() currentPage: number = 1;
  @Input() url: string = '';

  private readonly router = inject(Router);

  pagesCount: number = 1;
  pages: number[] = [];

  ngOnInit(): void {
    this.setPages();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const currentPage = changes['currentPage'];
    const total = changes['total'];

    const isCurrentPageChanged = isSimpleChangeChanged(currentPage);
    const isTotalChanged = isSimpleChangeChanged(total);

    if(isCurrentPageChanged || isTotalChanged) {
      this.setPages();
    }
  }

  private setPages(): void {
    this.pagesCount = Math.ceil(this.total / this.limit);
    this.pages = this.pagesCount > 0 ? range(0, this.pagesCount) : [];

    const numberOfPages = this.pages.length;
    if(numberOfPages < this.currentPage) {
      this.onChangePage(numberOfPages);
    }
  }

  onChangePage(page: number): void {
    if(this.currentPage === page) return;

    this.router.navigate([this.url], { queryParams: { page } });
  }

}
