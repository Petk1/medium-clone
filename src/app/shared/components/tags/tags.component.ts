import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { NgFor } from '@angular/common';
import { TPopularTag } from '../../types/popularTag.type';

@Component({
  selector: 'mc-tags',
  template: `
    @for(tag of tags; track tag) {
      <div class="tag" data-testingId="tag">
        {{ tag }}
      </div>
    }
  `,
  styles: [`
    @use 'partials/colors';

    :host {
      display: flex;
      flex-wrap: wrap;
      justify-content: flex-end;
    }

    .tag {
      display: block;
      margin: 3px 5px;
      padding: 2px 5px;
      color: colors.$second-complementary-color;
      border: 1px solid colors.$primary-color;
      border-radius: 5%;
      letter-spacing: 2px;
    }`
  ],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgFor,
  ]
})
export class TagsComponent {
  @Input({ required: true }) tags!: TPopularTag[];
}
