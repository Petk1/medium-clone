import { Component } from "@angular/core"
import { By } from "@angular/platform-browser";
import { TestBed } from "@angular/core/testing";
import { TagsComponent } from "./tags.component"

describe('TagsComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  it('should display all passed tags', () => {
    const { tagsDebugEl } = setup();
    const tagsDebugEls = tagsDebugEl.queryAll(By.css('[data-testingId="tag"]'));
    expect(tagsDebugEls.length).toBe(3);
  })
})

function setup() {
  @Component({
    standalone: true,
    template: `
      <mc-tags [tags]="tagList"/>
    `,
    imports: [
      TagsComponent
    ]
  })
  class TagsTestHost {
    tagList = ['TestTag1', 'TestTag2', 'TestTag3'];
  }

  const fixture = TestBed.createComponent(TagsTestHost);
  const tagsDebugEl = fixture.debugElement;
  const componentInstance = tagsDebugEl.componentInstance as TagsComponent;

  fixture.detectChanges();

  return {
    tagsDebugEl,
    componentInstance
  }
}
