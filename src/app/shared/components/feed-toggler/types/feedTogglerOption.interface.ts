export interface IFeedTogglerOption {
  name: string;
  apiUrl: string | undefined;
  callback?: Function; // function called when an option is selected
}
