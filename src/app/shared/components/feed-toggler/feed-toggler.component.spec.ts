import { Component } from "@angular/core"
import { By } from "@angular/platform-browser"
import { ActivatedRoute, NavigationEnd, NavigationStart, Params, Router, convertToParamMap } from "@angular/router"
import { TestBed } from "@angular/core/testing"
import { of } from "rxjs"
import { provideMockStore } from "@ngrx/store/testing"
import { FeedTogglerComponent } from "./feed-toggler.component"
import { LIMIT_ARTICLES } from "../articles/articles.component"
import { IFeedTogglerOption } from "./types/feedTogglerOption.interface"
import { IArticlesState } from "../articles/types/articlesState.interface"
import { IGetArticlesResponse } from "../articles/types/getArticlesResponse.interface"
import { selectArticlesData, selectError, selectIsLoading } from "../articles/store/reducers"

describe('FeedTogglerComponent', () => {
  it('should create', () => {
    expect(setup().componentInstance).toBeTruthy();
  })

  it('should initialize correctly with a passed data', () => {
    const { hostComponentInstance, componentInstance } = setup();
    const togglerOptions = hostComponentInstance.togglerOptions;
    expect(componentInstance._togglerOptions).toEqual(togglerOptions);
    expect(componentInstance.selectedApiUrlSig()).toBe(togglerOptions[0]?.apiUrl);
  })

  it('should toggle correctly', () => {
    const { fixture, feedTogglerOptionDebugEl, componentInstance, hostComponentInstance } = setup();

    const buttonEls = feedTogglerOptionDebugEl.queryAll(By.css('[data-testingId="toggler-option"]'));
    expect(buttonEls.length).toBe(2);

    const togglerOptions = hostComponentInstance.togglerOptions

    buttonEls[1].triggerEventHandler('click');
    fixture.detectChanges();
    expect(componentInstance.selectedApiUrlSig()).toBe(togglerOptions[1]?.apiUrl)
    expect(togglerOptions[1].callback).toHaveBeenCalled();

    buttonEls[0].triggerEventHandler('click');
    fixture.detectChanges();
    expect(componentInstance.selectedApiUrlSig()).toBe(togglerOptions[0]?.apiUrl)
    expect(togglerOptions[0].callback).toHaveBeenCalled();
  })

  it('should correctly handle case when the initSelectedOption is passed', () => {
    const { hostComponentInstance, componentInstance } = setup(1, 1);
    const togglerOptions = hostComponentInstance.togglerOptions
    expect(componentInstance.selectedApiUrlSig()).toBe(togglerOptions[1]?.apiUrl)
    expect(togglerOptions[1].callback).toHaveBeenCalled();
  })

  it('should correctly handle case when the initSelectedOption is the first option', () => {
    const { hostComponentInstance, componentInstance } = setup(1, 0);
    const togglerOptions = hostComponentInstance.togglerOptions
    expect(componentInstance.selectedApiUrlSig()).toBe(togglerOptions[0]?.apiUrl)
    expect(togglerOptions[0].callback).not.toHaveBeenCalled();
  })
})

function setup(
  limit: number | undefined = 1,
  numberInitSelectedOption: number | undefined = undefined
) {
  @Component({
    standalone: true,
    template: `
      <mc-feed-toggler
        [togglerOptions]="togglerOptions"
        [initSelectedOption]="initSelectedOption"
      />
    `,
    imports: [
      FeedTogglerComponent
    ],
  })
  class FeedTogglerTestHost {
    togglerOptions: IFeedTogglerOption[] = [
      {
        apiUrl: 'first/api/url',
        name: 'First Option',
        callback: jasmine.createSpy('callback1')
      },
      {
        apiUrl: 'second/api/url',
        name: 'Second Option',
        callback: jasmine.createSpy('callback2')
      }
    ]
    initSelectedOption: IFeedTogglerOption | undefined =
      typeof numberInitSelectedOption === 'number'
        ? this.togglerOptions[numberInitSelectedOption]
        : undefined
  }

  const mockRouter = {
    url: '',
    events: of(new NavigationEnd(1, 'url', 'urlAfterRedirects')),
    navigate: jasmine.createSpy('navigate'),
    createUrlTree: () => {},
    serializeUrl: () => {},
  };

  const params: Params = {
    page: 1
  }

  const mockActivatedRoute = {
    queryParamMap: of(convertToParamMap(params)),
    snapshot: {
      queryParamMap: {
        get: jasmine.createSpy('get').and.returnValue('1')
      },
      url: [{ path: 'global-feed' }]
    },
    events: of(new NavigationStart(1, 'url'))
  };

  const mockArticles: IGetArticlesResponse = {
    articles: INITIAL_STATE.articlesData?.articles.slice(0, limit) || [],
    articlesCount: INITIAL_STATE.articlesData?.articlesCount || 0,
  }

  TestBed.configureTestingModule({
    providers: [
      {
        provide: ActivatedRoute,
        useValue: mockActivatedRoute
      },
      {
        provide: Router,
        useValue: mockRouter
      },
      {
        provide: LIMIT_ARTICLES,
        useValue: limit
      },
      provideMockStore({
        initialState: INITIAL_STATE,
        selectors: [
          { selector: selectIsLoading, value: INITIAL_STATE.isLoading },
          { selector: selectError, value: INITIAL_STATE.error },
          { selector: selectArticlesData, value: mockArticles },
        ]
      })
    ]
  })

  const fixture = TestBed.createComponent(FeedTogglerTestHost);
  const hostComponentInstance = fixture.componentInstance;
  const feedTogglerOptionDebugEl = fixture.debugElement.query(By.directive(FeedTogglerComponent));
  const componentInstance = feedTogglerOptionDebugEl.componentInstance as FeedTogglerComponent;

  fixture.detectChanges();

  return {
    fixture,
    hostComponentInstance,
    feedTogglerOptionDebugEl,
    componentInstance,
  }
}

const INITIAL_STATE: IArticlesState = {
  isLoading: false,
  error: null,
  articlesData: {
    articles: [
      {
        slug: "asdas-240342",
        title: "asdas",
        description: "dasd",
        body: "asdasd",
        tagList: [
          "asdasd"
        ],
        createdAt: "2023-11-05T16:47:51.553Z",
        updatedAt: "2023-11-05T16:47:51.553Z",
        favorited: false,
        favoritesCount: 0,
        author: {
          username: "ABC_123_ABC",
          bio: "abcdefghijklmnollko",
          image: "https://api.realworld.io/images/smiley-cyrus.jpeg",
          following: false
        }
      },
      {
        slug: "sadasd-240342",
        title: "sadasd",
        description: "asdas",
        body: "dasda",
        tagList: [
          "sdasd",
          'ABC',
          'test tag'
        ],
        createdAt: "2023-11-05T16:14:21.006Z",
        updatedAt: "2023-11-05T16:14:21.006Z",
        favorited: false,
        favoritesCount: 0,
        author: {
          username: "ABC_123_ABC",
          bio: "abcdefghijklmnollko",
          image: "https://api.realworld.io/images/smiley-cyrus.jpeg",
          following: false
        }
      }
    ],
    articlesCount: 2
  }
}
