import { ChangeDetectionStrategy, Component, Input, signal } from "@angular/core";
import { NgClass, NgFor, NgIf } from "@angular/common";
import { ArticlesComponent } from "../articles/articles.component";
import { IFeedTogglerOption } from "./types/feedTogglerOption.interface";

@Component({
  selector: 'mc-feed-toggler',
  templateUrl: './feed-toggler.component.html',
  styleUrls: ['./feed-toggler.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgFor,
    NgIf,
    NgClass,
    ArticlesComponent,
  ]
})
export class FeedTogglerComponent {
  @Input({ required: true })
  set togglerOptions(options: IFeedTogglerOption[]){
    this._togglerOptions = options;
    this.selectedApiUrlSig.set(options?.[0].apiUrl);
  }

  @Input() set initSelectedOption(option: IFeedTogglerOption) {
    if(!option) return;
    this.onToggle(option);
  }

  _togglerOptions: IFeedTogglerOption[] = [];
  selectedApiUrlSig = signal<string | undefined>(undefined);

  onToggle({ apiUrl, callback }: IFeedTogglerOption): void {
    if(apiUrl === this.selectedApiUrlSig()) {
      return;
    }

    if(apiUrl) {
      this.selectedApiUrlSig.set(apiUrl);
    }

    callback?.();
  }
}
