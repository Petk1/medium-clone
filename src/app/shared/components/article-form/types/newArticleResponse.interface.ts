import { IArticle } from "src/app/shared/types/article.interface";

export interface INewArticleResponse {
  article: IArticle;
}
