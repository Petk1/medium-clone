import { IArticle } from "src/app/shared/types/article.interface";

export interface INewArticleRequest {
  article: Pick<IArticle,
    'title' |
    'description' |
    'body' |
    'tagList'
  >
}
