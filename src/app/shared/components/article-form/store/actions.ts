import { createActionGroup, emptyProps, props } from "@ngrx/store";
import { INewArticleRequest } from "../types/newArticleRequest.interface";
import { IArticle } from "src/app/shared/types/article.interface";

export const articleFormActions = createActionGroup({
  source: 'Article form',
  events: {
    'Create new article': props<{ newArticle: INewArticleRequest }>(),
    'Create new article success': props<{ article: IArticle }>(),
    'Create new article failure': emptyProps(),
  }
})
