import { inject } from '@angular/core';
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { switchMap, map, catchError, of, tap } from 'rxjs';
import { ArticleService } from 'src/app/shared/services/article.service';
import { articleFormActions } from './actions';
import { Router } from '@angular/router';

export const createNewArticle = createEffect((
  actions$ = inject(Actions),
  articleSrv = inject(ArticleService),
) => (
  actions$.pipe(
    ofType(articleFormActions.createNewArticle),
    switchMap(({ newArticle }) => (
      articleSrv.newArticle(newArticle).pipe(
        map(article => articleFormActions.createNewArticleSuccess({ article })),
        catchError(() => of(articleFormActions.createNewArticleFailure()))
      )
    ))
  )
), {
  functional: true
})

export const newArticleSuccessEffect = createEffect((
  actions$ = inject(Actions),
  router = inject(Router),
) => (
  actions$.pipe(
    ofType(articleFormActions.createNewArticleSuccess),
    tap(({ article: { slug } }) => router.navigateByUrl(`/article/${slug}`)),
  )
), {
  functional: true,
  dispatch: false,
})
