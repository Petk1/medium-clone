import { ChangeDetectionStrategy, Component, Input, OnInit, effect, inject } from "@angular/core";
import { NgClass } from "@angular/common";
import { FormBuilder, ReactiveFormsModule, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { selectArticleData } from "src/app/article/store/reducers";
import { IEditArticleRequest } from "src/app/article/types/editArticleRequest.interface";
import { articleActions } from "src/app/article/store/actions";
import { IArticle } from "../../types/article.interface";
import { articleFormActions } from "./store/actions";
import { INewArticleRequest } from "./types/newArticleRequest.interface";
import { ICanComponentDeactivate } from "src/app/core/guards/types/canComponentDeactivate.interface";

@Component({
  selector: 'mc-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['../../../../assets/scss/bases/base-form.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgClass,
    ReactiveFormsModule,
  ]
})
export class ArticleFormComponent implements OnInit, ICanComponentDeactivate {
  private readonly slug = inject(ActivatedRoute).snapshot.paramMap.get('slug') ?? '';
  private readonly router = inject(Router);
  private readonly store = inject(Store);
  private readonly editMode = this.router.url.includes('edit');
  article = this.store.selectSignal(selectArticleData);
  private isFormSubmitted = false;

  form = inject(FormBuilder).nonNullable.group({
    title: ['', Validators.required],
    description: ['', [Validators.required]],
    body: ['', [Validators.required]],
    tagList: [''],
  });

  constructor() {
    effect(() => {
      if(this.editMode) {
        const currentArticleData = this.article();

        if(currentArticleData) {
          const { title, description, body, tagList } = currentArticleData;

          this.form.setValue({
            title,
            description,
            body,
            tagList: tagList.join(', ')
          });
        }
      }
    })
  }

  ngOnInit(): void {
    if(this.editMode) {
      const currentArticleData = this.article();

      // If we got edit mode and into the store the article is not available
      // we have to fetch that one. It will trigger the code snippet (article() signal)
      // from the effect function and then set the form value.
      // This case is possible when the user navigated to edit and used refresh,
      // so the store is empty.
      if(!currentArticleData) {
        this.store.dispatch(articleActions.getArticle({ slug: this.slug }));
      }
    }
  }

  onSubmit(): void {
    if(this.editMode) {
      this.handleEdit();
      return;
    }

    this.handleNewArticle();
  }

  handleEdit(): void {
    const articleRequestData: IEditArticleRequest = {
      article: {
        ...this.article() as IArticle,
        title: this.getFormControlValue('title'),
        description: this.getFormControlValue('description'),
        body: this.getFormControlValue('body'),
        tagList: this.getTagList(),
        updatedAt: new Date().toISOString(),
      }
    };

    this.store.dispatch(articleActions.editArticle({
      slug: this.slug,
      articleRequestData
    }));
  }

  handleNewArticle(): void {
    const articleRequestData: INewArticleRequest = {
      article: {
        title: this.getFormControlValue('title'),
        description: this.getFormControlValue('description'),
        body: this.getFormControlValue('body'),
        tagList: this.getTagList(),
      }
    }

    this.isFormSubmitted = true;

    this.store.dispatch(articleFormActions.createNewArticle({
      newArticle: articleRequestData
    }));
  }

  private getTagList(): string[] {
    return this.getFormControlValue('tagList')
      .split(',')
      .map(tag => tag.trim())
      .filter(tag => !!tag); // to avoid adding an empty tag ''
  }

  private getFormControlValue(name: string): string {
    return this.form.get(name)!.value as string;
  }

  canDeactivate(): boolean {
    if(this.isFormSubmitted) return true;

    return !this.form.dirty;
  };
}
