import { FormBuilder } from "@angular/forms"
import { ActivatedRoute, Router } from "@angular/router"
import { TestBed } from "@angular/core/testing"
import { Store } from "@ngrx/store"
import { articleFormActions } from "./store/actions"
import { articleActions } from "src/app/article/store/actions"
import { ArticleFormComponent } from "./article-form.component"
import { IArticle } from "../../types/article.interface"
import { INewArticleRequest } from "./types/newArticleRequest.interface"
import { IEditArticleRequest } from "src/app/article/types/editArticleRequest.interface"

const ARTICLE_MOCK_DATA = {
  body: 'Sample Body',
  createdAt: '2023-11-05T16:14:21.006Z',
  description: 'Sample Description',
  favorited: true,
  favoritesCount: 5,
  slug: 'Sample Slug',
  tagList: ['tag1', 'tag2'],
  title: 'Sample Title',
  updatedAt: '2023-11-06T11:12:25.003Z',
  author: {
    username: 'Sample Username',
    bio: null,
    image: '',
    following: true
  }
}

describe('ArticleFormComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  it('should set the form values in edit mode', () => {
    const { componentInstance } = setup('edit', ARTICLE_MOCK_DATA);
    const { title, description, body, tagList } = componentInstance.form.value;

    expect(title).toEqual(ARTICLE_MOCK_DATA.title);
    expect(description).toEqual(ARTICLE_MOCK_DATA.description);
    expect(body).toEqual(ARTICLE_MOCK_DATA.body);
    expect(tagList).toEqual(ARTICLE_MOCK_DATA.tagList.join(', '));
  })

  describe('onSubmit', () => {
    it('should call a dispatch method with the properly data', () => {
      const { fixture, componentInstance, mockStore } = setup();
      const { title, description, body, tagList } = ARTICLE_MOCK_DATA;

      componentInstance.form.setValue({
        title, description, body, tagList: tagList.join(', ')
      });

      fixture.detectChanges();

      const articleRequestData: INewArticleRequest = {
        article: { title, description, body, tagList }
      }

      componentInstance.onSubmit();

      expect(mockStore.dispatch).toHaveBeenCalledWith(articleFormActions.createNewArticle({
        newArticle: articleRequestData
      }))
    })

    it('should call a dispatch method with the properly data (edit mode)', () => {
      const { componentInstance, mockStore, mockActivatedRoute } = setup('edit', ARTICLE_MOCK_DATA);

      const articleRequestData: IEditArticleRequest = {
        article: {
          ...ARTICLE_MOCK_DATA,
          updatedAt: new Date().toISOString(),
        }
      };

      componentInstance.onSubmit();

      expect(mockStore.dispatch).toHaveBeenCalledWith(articleActions.editArticle({
        slug: mockActivatedRoute.snapshot.paramMap.get(),
        articleRequestData
      }))
    })
  })

  describe('canDeactivate method', () => {
    it('should return true if the form is submitted', () => {
      const { fixture, componentInstance } = setup();
      const { title, description, body, tagList } = ARTICLE_MOCK_DATA;

      componentInstance.form.setValue({
        title, description, body, tagList: tagList.join(', ')
      });

      fixture.detectChanges();

      const articleRequestData: INewArticleRequest = {
        article: { title, description, body, tagList }
      }

      componentInstance.onSubmit();

      expect(componentInstance['isFormSubmitted']).toBe(true);
      expect(componentInstance.canDeactivate()).toBe(true);
    })

    it('should return false if the form is dirty', () => {
      const dirtyStateMock = {
        dirty: true
      }
      const { fixture, componentInstance } = setup();
      componentInstance.form = dirtyStateMock as any;
      fixture.detectChanges();

      expect(componentInstance.canDeactivate()).toBe(false);
    })

    it('should return true if neither the form is submitted nor is dirty', () => {
      // Default state isFormSubmitted is false and form.dirty is false
      const { componentInstance } = setup();
      expect(componentInstance.canDeactivate()).toBe(true);
    })
  })

  afterEach(() => {
    jasmine.clock().uninstall();
  })
})

function setup(url: string = '', storeData: IArticle | null = null) {
  jasmine.clock().install().mockDate(new Date())

  const mockActivatedRoute = {
    snapshot: {
      paramMap: {
        get: jasmine.createSpy('get').and.returnValue('some-test-slug')
      }
    }
  }

  const mockRouter = { url };

  const mockStore = {
    selectSignal: jasmine.createSpy('selectSignal').and.returnValue(() => storeData),
    dispatch: jasmine.createSpy('dispatch')
  }

  TestBed.configureTestingModule({
    providers: [
      FormBuilder,
      {
        provide: ActivatedRoute,
        useValue: mockActivatedRoute
      },
      {
        provide: Router,
        useValue: mockRouter
      },
      {
        provide: Store,
        useValue: mockStore
      },
    ]
  })

  const fixture = TestBed.createComponent(ArticleFormComponent);
  const componentInstance = fixture.componentInstance;

  fixture.detectChanges();

  return {
    fixture,
    componentInstance,
    mockStore,
    mockActivatedRoute,
  }
}
