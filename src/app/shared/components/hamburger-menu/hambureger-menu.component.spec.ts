import { Component, DebugElement, ElementRef, Renderer2 } from "@angular/core"
import { By } from "@angular/platform-browser";
import { TestBed } from "@angular/core/testing";
import { NavigationService } from "../../services/navigation.service";
import { ROUTER_CONFIG } from "src/app/core/router-config/router-config.token";
import { HamburgerMenuComponent } from "./hamburger-menu.component"
import { ICurrentUser } from "../../types/currentUser.interface";
import { IRouterConfig } from "src/app/core/router-config/types/routerConfig.interface";

describe('HamburgerMenuComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  it('should add and remove the hamburger-menu-open class when toggling', () => {
    const { fixture, componentInstance } = setup();

    expect(componentInstance.isMenuOpen).toBe(false)

    componentInstance.toggleMenu();
    let doc = (fixture.nativeElement as HTMLElement).ownerDocument;
    expect(componentInstance.isMenuOpen).toBe(true);
    expect(doc.querySelector('.hamburger-menu-open')).toBeTruthy();

    componentInstance.toggleMenu();
    doc = (fixture.nativeElement as HTMLElement).ownerDocument;
    expect(componentInstance.isMenuOpen).toBe(false);
    expect(doc.querySelector('.hamburger-menu-open')).toBeNull();
  });


  describe('User is NOT logged in', () => {
    let routerConfig: IRouterConfig;
    let _mockNavigationService: any;
    let liEls: NodeListOf<HTMLLIElement>;
    let liDebugEls: DebugElement[];

    beforeEach(() => {
      const { hamburgerMenuDebugEl, mockNavigationService } = setup();
      routerConfig = TestBed.inject<IRouterConfig>(ROUTER_CONFIG);
      _mockNavigationService = mockNavigationService;
      const optionEls = hamburgerMenuDebugEl.query(By.css('[data-testingId="mobile-menu-options"]'));
      liEls = (optionEls.nativeElement as HTMLElement).querySelectorAll('li');
      liDebugEls = optionEls.queryAll(By.css('li'));
    })

    it('should initialize with all injected menu options (user is NOT logged in)', () => {
      expect(liEls.length).toBe(routerConfig.userNotLoggedIn.length);
    })

    it('should navigate on a menu item click (user is NOT logged in)', () => {
      liDebugEls.forEach((liDebugEl, index) => {
        liDebugEl.triggerEventHandler('click');
        expect(_mockNavigationService.onNavigation).toHaveBeenCalledWith(routerConfig.userNotLoggedIn[index].link);
      })
    })
  })

  describe('User is logged in', () => {
    let routerConfig: IRouterConfig;
    let _mockNavigationService: any;
    let liEls: NodeListOf<HTMLLIElement>;
    let liDebugEls: DebugElement[];

    beforeEach(() => {
      const { hamburgerMenuDebugEl, mockNavigationService } = setup({} as ICurrentUser);
      routerConfig = TestBed.inject<IRouterConfig>(ROUTER_CONFIG);
      _mockNavigationService = mockNavigationService;
      const optionEls = hamburgerMenuDebugEl.query(By.css('[data-testingId="mobile-menu-options"]'));
      liEls = (optionEls.nativeElement as HTMLElement).querySelectorAll('li');
      liDebugEls = optionEls.queryAll(By.css('li'));
    })

    it('should initialize with all injected menu options (user is logged in)', () => {
      expect(liEls.length).toBe(routerConfig.userLoggedIn.length);
    })

    it('should navigate on a menu item click (user is logged in)', () => {
      liDebugEls.forEach((liDebugEl, index) => {
        liDebugEl.triggerEventHandler('click');
        expect(_mockNavigationService.onNavigation).toHaveBeenCalledWith(routerConfig.userLoggedIn[index].link);
      })
    })
  })
})

function setup(currentUser: ICurrentUser | null | undefined = undefined) {
  @Component({
    standalone: true,
    template: `
      <mc-hamburger-menu [currentUser]="currentUser"/>
    `,
    imports: [
      HamburgerMenuComponent
    ],
  })
  class HamburgerMenuTestHost {
    currentUser = currentUser;
  }

  const mockElementRef = jasmine.createSpyObj('ElementRef', ['nativeElement']);
  const mockNavigationService = jasmine.createSpyObj('NavigationService', ['onNavigation']);

  TestBed.configureTestingModule({
    providers: [
      Renderer2,
      { provide: ElementRef, useValue: mockElementRef },
      { provide: NavigationService, useValue: mockNavigationService },
      {
        provide: ROUTER_CONFIG,
        useValue: {
          userLoggedIn: [
            {
              name: 'Home',
              link: '/'
            },
            {
              name: 'New article',
              link: '/new-article'
            }
          ],
          userNotLoggedIn: [
            {
              name: 'Home',
              link: '/'
            },
            {
              name: 'Sign In',
              link: '/login'
            },
            {
              name: 'Sign Up',
              link: '/registration'
            }
          ]
        } as IRouterConfig
      }
    ]
  })

  const fixture = TestBed.createComponent(HamburgerMenuTestHost);
  const hamburgerMenuDebugEl = fixture.debugElement.query(By.directive(HamburgerMenuComponent));
  const componentInstance = hamburgerMenuDebugEl.componentInstance as HamburgerMenuComponent;

  fixture.detectChanges();

  return {
    fixture,
    hamburgerMenuDebugEl,
    componentInstance,
    mockNavigationService,
  }
}
