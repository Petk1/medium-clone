import { Component, inject, ElementRef, Renderer2, Input, ChangeDetectionStrategy } from "@angular/core";
import { NgClass, NgFor } from "@angular/common";
import { ROUTER_CONFIG } from "src/app/core/router-config/router-config.token";
import { NavigationService } from "../../services/navigation.service";
import { ICurrentUser } from "../../types/currentUser.interface";

@Component({
  selector: 'mc-hamburger-menu',
  template: `
    <div class="hamburger-menu" (click)="toggleMenu()" data-testingId="hamburger-menu">
      <span class="line" [class.line-open]="isMenuOpen"></span>
      <span class="line" [ngClass]="{'line-open': isMenuOpen, 'line-close': !isMenuOpen && !firstToggleMenu }"></span>
      <span class="line" [class.line-open]="isMenuOpen"></span>
    </div>
    <ul class="mobile-menu" [ngClass]="isMenuOpen ? 'mobile-menu-open' : 'mobile-menu-close'" data-testingId="mobile-menu-options">
      @for(link of currentUser ? routerConfig.userLoggedIn : routerConfig.userNotLoggedIn; track link) {
        <li (click)="onNavigation(link.link)">
          <span>{{ link.name }}</span>
        </li>
      }
    </ul>
  `,
  styleUrls: ['./hamburger-menu.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgClass,
    NgFor,
  ],
})
export class HamburgerMenuComponent {
  @Input({ required: true }) currentUser!: ICurrentUser | null | undefined;

  readonly routerConfig = inject(ROUTER_CONFIG);
  private readonly renderer = inject(Renderer2);
  private readonly el = inject(ElementRef);
  private readonly navigationSrv = inject(NavigationService);
  isMenuOpen = false;
  firstToggleMenu = true;

  toggleMenu(): void {
    this.isMenuOpen = !this.isMenuOpen;
    this.firstToggleMenu = false;

    const className = 'hamburger-menu-open';
    if(this.isMenuOpen) { // hidden/show scroll
      this.renderer.addClass(this.el.nativeElement.ownerDocument.body, className);
    } else {
      this.renderer.removeClass(this.el.nativeElement.ownerDocument.body, className);
    }
  }

  onNavigation(link: string): void {
    this.toggleMenu();
    this.navigationSrv.onNavigation(link);
  }

}
