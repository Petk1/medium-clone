import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: 'mc-loading',
  template: `<p>Loading...</p>`,
  styles: [`
    :host {
      margin: 20px;
      font-size: 1.5rem;
    }
  `],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadingComponent {}