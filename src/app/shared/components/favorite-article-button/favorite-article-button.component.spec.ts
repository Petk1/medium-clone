import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { By } from "@angular/platform-browser";
import { TestBed } from "@angular/core/testing";
import { MockStore, provideMockStore } from "@ngrx/store/testing";
import { likeActions } from "./store/actions";
import { selectLikedArticle } from "./store/reducers";
import { selectCurrentUser } from "src/app/auth/store/reducers";
import { FavoriteArticleButtonComponent } from "./favorite-article-button.component";

describe('FavoriteArticleButtonComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  it('should initialize with a correct passed data', () => {
    const { favoriteArticleButtonDebugEl, componentInstance } = setup(
      true,
      'testslug',
      1000
    );

    const buttonEl = favoriteArticleButtonDebugEl.query(By.css('button'))?.nativeElement as HTMLElement;

    expect(componentInstance['isFavorited']).toBe(true);
    expect(componentInstance['favoritesCount']).toBe(1001);
    expect(componentInstance['articleSlug']).toBe('testslug');
    expect(buttonEl.textContent).toContain('Unfavorite Article (1000)')
  })

  it('should dispatch unlike action when an article is liked', () => {
    const { componentInstance, store } = setup(
      true,
      'testslug',
      1000
    );

    const mockStoreDispatchSpy = spyOn(store, 'dispatch').and.callThrough();
    componentInstance.handleLike();

    expect(mockStoreDispatchSpy).toHaveBeenCalledWith(likeActions.unlike({ slug: 'testslug' }))
  })

  it('should dispatch like action when an article is not liked', () => {
    const { componentInstance, store } = setup(
      false,
      'testslug',
      1000
    );

    const mockStoreDispatchSpy = spyOn(store, 'dispatch').and.callThrough();
    componentInstance.handleLike();

    expect(mockStoreDispatchSpy).toHaveBeenCalledWith(likeActions.like({ slug: 'testslug' }))
  })

  it('should navigate to login when the user is not logged in', () => {
    const { componentInstance, mockRouter, store } = setup(
      false,
      'testslug',
      1000,
      false
    );

    const mockStoreDispatchSpy = spyOn(store, 'dispatch').and.callThrough();

    componentInstance.handleLike();

    expect(mockRouter.navigate).toHaveBeenCalledWith(['/login']);
    expect(mockStoreDispatchSpy).not.toHaveBeenCalled();
  })
})

function setup(
  favorited: boolean = false,
  articleSlug: string = 'testSlug',
  favoritesCount: number = 100,
  isUserLoggedIn: boolean = true
) {
  @Component({
    standalone: true,
    template: `
      <mc-favorite-article-button
        [isFavorited]="favorited"
        [articleSlug]="articleSlug"
        [favoritesCount]="favoritesCount"
      />
    `,
    imports: [
      FavoriteArticleButtonComponent
    ]
  })
  class FavoriteArticleButtonTestHost {
    favorited = favorited;
    articleSlug = articleSlug;
    favoritesCount = favoritesCount;
  }

  const mockRouter = {
    navigate: jasmine.createSpy('navigate')
  }

  TestBed.configureTestingModule({
    providers: [
      {
        provide: Router,
        useValue: mockRouter
      },
      provideMockStore({
        initialState: INITIAL_STATE,
        selectors: [
          {
            selector: selectCurrentUser,
            value: isUserLoggedIn ? INITIAL_STATE.currentUser : undefined
          },
          {
            selector: selectLikedArticle,
            value: favorited ? INITIAL_STATE.likedArticle : null
          },
        ]
      })
    ]
  })

  const fixture = TestBed.createComponent(FavoriteArticleButtonTestHost);
  const favoriteArticleButtonDebugEl = fixture.debugElement.query(By.directive(FavoriteArticleButtonComponent));
  const componentInstance = favoriteArticleButtonDebugEl.componentInstance as FavoriteArticleButtonComponent;
  const store = TestBed.inject(MockStore);

  fixture.detectChanges();

  return {
    favoriteArticleButtonDebugEl,
    componentInstance,
    store,
    mockRouter,
  }
}

const INITIAL_STATE = {
  currentUser: {},
  likedArticle: {}
}
