import { inject } from "@angular/core";
import { createEffect, Actions, ofType } from "@ngrx/effects";
import { switchMap, map, catchError, of, tap } from "rxjs";
import { LikeService } from "../services/like.service";
import { likeActions } from "./actions";

export const likeEffect = createEffect((
  actions$ = inject(Actions),
  likeSrv = inject(LikeService)
) => (
  actions$.pipe(
    ofType(likeActions.like),
    switchMap(({ slug }) => (
      likeSrv.like(slug)
      .pipe(
        map(article => likeActions.likeSuccess({ article })),
        catchError(() => of(likeActions.likeFailure()))
      )
    ))
  )
), {
  functional: true
})

export const unlikeEffect = createEffect((
  actions$ = inject(Actions),
  likeSrv = inject(LikeService)
) => (
  actions$.pipe(
    ofType(likeActions.unlike),
    switchMap(({ slug }) => (
      likeSrv.unlike(slug)
      .pipe(
        map(() => likeActions.unlikeSuccess()),
        catchError(() => of(likeActions.unlikeFailure()))
      )
    ))
  )
), {
  functional: true
})
