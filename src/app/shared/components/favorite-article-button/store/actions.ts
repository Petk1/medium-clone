import { createActionGroup, emptyProps, props } from "@ngrx/store";
import { IArticle } from "src/app/shared/types/article.interface";

export const likeActions = createActionGroup({
  source: 'Like',
  events: {
    'Like': props<{ slug: string }>(),
    'Like success': props<{ article: IArticle }>(),
    'Like failure': emptyProps(),

    'Unlike': props<{ slug: string }>(),
    'Unlike success': emptyProps(),
    'Unlike failure': emptyProps(),
  }
})
