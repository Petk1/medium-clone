import { createFeature, createReducer, on } from "@ngrx/store";
import { ILikeState } from "../types/likeState.interface";
import { likeActions } from "./actions";
import { routerNavigatedAction } from "@ngrx/router-store";

const initialState: ILikeState = {
  likedArticle: undefined,
}

const likeFeature = createFeature({
  name: 'like',
  reducer: createReducer(
    initialState,
    on(likeActions.likeSuccess, (state, { article: likedArticle }) => ({
      ...state,
      likedArticle,
    })),
    on(likeActions.unlikeSuccess, state => ({
      ...state,
      likedArticle: null,
    })),
    on(routerNavigatedAction, () => initialState)
  )
})

export const {
  name: likeFeatureKey,
  reducer: likeReducer,
  selectLikedArticle,
} = likeFeature;
