import { ChangeDetectionStrategy, Component, Input, Signal, computed, effect, inject } from "@angular/core";
import { NgClass } from "@angular/common";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { likeActions } from "./store/actions";
import { selectLikedArticle } from "./store/reducers";
import { selectCurrentUser } from "src/app/auth/store/reducers";

@Component({
  selector: 'mc-favorite-article-button',
  template: `
    <button class="primary-btn primary-btn--active" (click)="handleLike()"
      [ngClass]="{'btn--liked': likeSignal()}"
    >
      {{ btnTxt() }} Article ({{ favoritesCount }})
    </button>
  `,
  styles: [`
    @use 'partials/colors';

    button {
      height: 30px;
    }

    .btn--liked {
      background-color: colors.$green-color;
      color: colors.$white-color;
    }
  `],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgClass,
  ],
})
export class FavoriteArticleButtonComponent {
  @Input({ required: true }) isFavorited = false;
  @Input({ required: true }) favoritesCount = 0;
  @Input({ required: true }) articleSlug = '';

  private store = inject(Store);
  private currentUser = this.store.selectSignal(selectCurrentUser);
  private router = inject(Router);

  likedArticle = this.store.selectSignal(selectLikedArticle);

  likeSignal: Signal<boolean> = computed(() => (
    !!this.likedArticle() || this.isFavorited
  ));

  btnTxt: Signal<string> = computed(() => (
    !!this.likedArticle() || this.isFavorited ? 'Unfavorite' : 'Favorite'
  ))

  constructor() {
    effect(() => this.likedArticle() ? this.favoritesCount++ : this.favoritesCount--);
  }

  handleLike(): void {
    if(!this.currentUser()) {
      this.router.navigate(['/login']);
      return;
    }

    if(this.likeSignal()) {
      this.store.dispatch(likeActions.unlike({ slug: this.articleSlug }));
      this.isFavorited = false;
    } else {
      this.store.dispatch(likeActions.like({ slug: this.articleSlug }));
      this.isFavorited = true;
    }
  }
}
