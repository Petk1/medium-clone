import { IArticle } from "src/app/shared/types/article.interface";

export interface ILikeState {
  likedArticle: IArticle | null | undefined;
}
