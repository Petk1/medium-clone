import { Observable, map } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { IArticle } from 'src/app/shared/types/article.interface';
import { IArticleResponse } from 'src/app/shared/types/articleResponse.interface';

@Injectable()
export class LikeService {
  private http = inject(HttpClient);

  like(slug: string): Observable<IArticle> {
    const url = `articles/${slug}/favorite`;
    return this.http.post<IArticleResponse>(url, {})
    .pipe(map(this.getArticle));
  }

  unlike(slug: string): Observable<IArticle> {
    const url = `articles/${slug}/favorite`;
    return this.http.delete<IArticleResponse>(url, {})
    .pipe(map(this.getArticle));
  }

  private getArticle({ article }: IArticleResponse ): IArticle {
    return article;
  }
}
