import { Component, ChangeDetectionStrategy } from "@angular/core";

@Component({
  selector: 'mc-footer',
  template: `&#64;2023 Personal Side Project`,
  styleUrls: ['./footer.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent {}
