import { Injectable, inject } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { LoaderComponent } from '../loader.component';
import { take } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoaderService {
  private readonly matDialog = inject(MatDialog);
  private matDialogRef: MatDialogRef<LoaderComponent> | undefined = undefined;
  private loaderCounter = 0;

  openLoader() {
    this.loaderCounter++;

    if(this.matDialogRef) return;

    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '300px';
    dialogConfig.height = '225px';
    dialogConfig.closeOnNavigation = true;
    dialogConfig.disableClose = true;
    dialogConfig.position = {
      top: '125px',
    }

    this.matDialogRef = this.matDialog.open(LoaderComponent, dialogConfig);
    this.matDialogRef
    .afterClosed()
    .pipe(take(1))
    .subscribe(() => this.matDialogRef = undefined);

    return this.matDialogRef;
  }

  closeLoader(): void {
    if(!this.matDialogRef) {
      return;
    }

    this.loaderCounter--;

    if(!this.loaderCounter){
      this.matDialogRef.close();
    }
  }
}
