import { TestBed } from "@angular/core/testing";
import { MatDialog } from "@angular/material/dialog";
import { Subject } from "rxjs";
import { LoaderService } from "./loader.service"

describe('LoaderService', () => {
  it('should open the loader dialog', () => {
    const { loaderSrv, mockMatDialog } = setup();
    loaderSrv.openLoader();
    expect(mockMatDialog.open).toHaveBeenCalled();
  })

  it('should close the loader dialog', () => {
    const { loaderSrv, mockMatDialogRef } = setup();
    loaderSrv.openLoader();
    loaderSrv.closeLoader();
    expect(mockMatDialogRef.close).toHaveBeenCalled();
  })

  it('should not open more than one dialog if one is already open', () => {
    const { loaderSrv, mockMatDialog } = setup();
    loaderSrv.openLoader();
    loaderSrv.openLoader();
    loaderSrv.openLoader();
    expect(mockMatDialog.open).toHaveBeenCalledTimes(1);
  })
})

function setup() {
  let loaderSrv: LoaderService;
  const afterClosed$ = new Subject<void>()

  const mockMatDialogRef = {
    afterClosed: () => afterClosed$,
    close: jasmine.createSpy('close').and.callFake(() => {
      afterClosed$.next();
    })
  }
  const mockMatDialog = {
    open: jasmine.createSpy('open').and.returnValue(mockMatDialogRef)
  }

  TestBed.configureTestingModule({
    providers: [
      {
        provide: MatDialog,
        useValue: mockMatDialog
      }
    ]
  })

  loaderSrv = TestBed.inject(LoaderService);

  return {
    loaderSrv,
    mockMatDialog,
    mockMatDialogRef,
  }
}
