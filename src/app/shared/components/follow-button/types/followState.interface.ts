import { IProfile } from "src/app/shared/types/profile.interface";

export interface IFollowState {
  followedProfile: IProfile | null | undefined;
}
