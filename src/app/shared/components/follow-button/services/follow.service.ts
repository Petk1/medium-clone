import { Observable, map } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { IProfile } from 'src/app/shared/types/profile.interface';
import { IFollowResponse } from 'src/app/shared/types/followResponse.interface';

@Injectable()
export class FollowService {
  private http = inject(HttpClient);

  follow(username: string): Observable<IProfile> {
    const url = `profiles/${username}/follow`;
    return this.http.post<IFollowResponse>(url, {})
    .pipe(map(this.getProfile));
  }

  unfollow(username: string): Observable<IProfile> {
    const url = `profiles/${username}/follow`;
    return this.http.delete<IFollowResponse>(url, {})
    .pipe(map(this.getProfile));
  }

  private getProfile({ profile }: IFollowResponse ): IProfile {
    return profile;
  }
}
