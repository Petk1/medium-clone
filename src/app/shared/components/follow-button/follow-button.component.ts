import { Component, ChangeDetectionStrategy, Input, Signal, computed, inject, signal } from "@angular/core";
import { NgClass } from "@angular/common";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { selectCurrentUser } from "src/app/auth/store/reducers";
import { followActions } from "./store/actions";
import { selectFollowedProfile } from "./store/reducers";

@Component({
  selector: 'mc-follow-button',
  template: `
    <button class="primary-btn primary-btn--active" (click)="handleFollow()"
      [ngClass]="{'btn--followed': followingSignal()}"
    >
      {{ btnTxt() }} {{ username }}
    </button>
  `,
  styles: [`
    @use 'partials/colors';

    button {
      height: 30px;
    }

    .btn--followed {
      background-color: colors.$green-color;
      color: colors.$white-color;
    }
  `],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgClass,
  ],
})
export class FollowButtonComponent {
  @Input({ required: true}) username = '';
  @Input({ required: true}) following = false;

  private store = inject(Store);
  private currentUser = this.store.selectSignal(selectCurrentUser);
  private router = inject(Router);

  followedProfile = this.store.selectSignal(selectFollowedProfile);

  followingSignal: Signal<boolean> = computed(() => (
    !!this.followedProfile() || this.following
  ));

  btnTxt: Signal<string> = computed(() => (
    !!this.followedProfile() || this.following ? 'Unfollow' : 'Follow'
  ))

  handleFollow(): void {
    if(!this.currentUser()) {
      this.router.navigate(['/login']);
      return;
    }

    if(this.followingSignal()) {
      this.store.dispatch(followActions.unfollow({ username: this.username }));
      this.following = false;
    } else {
      this.store.dispatch(followActions.follow({ username: this.username }));
      this.following = true;
    }
  }
}
