import { Component } from "@angular/core"
import { By } from "@angular/platform-browser"
import { Router } from "@angular/router"
import { TestBed } from "@angular/core/testing"
import { MockStore, provideMockStore } from "@ngrx/store/testing"
import { followActions } from "./store/actions"
import { selectFollowedProfile } from "./store/reducers"
import { selectCurrentUser } from "src/app/auth/store/reducers"
import { FollowButtonComponent } from "./follow-button.component"

describe('FollowButtonComponent', () => {
  it('should create', () => {
    expect(setup().componentInstance).toBeTruthy();
  })

  it('should initialize with correct username and following status', () => {
    const { followButtonDebugEl, componentInstance, hostComponentInstance } = setup(true, true);
    const buttonEl = followButtonDebugEl.query(By.css('button')).nativeElement as HTMLElement;
    expect(componentInstance.username).toBe(hostComponentInstance.username);
    expect(componentInstance.following).toBe(hostComponentInstance.following);
    expect(buttonEl.textContent).toContain(hostComponentInstance.username);
  })

  describe('Follow/Unfollow', () => {
    it('should handle follow correctly', () => {
      const { followButtonDebugEl, store, componentInstance } = setup();
      const mockStoreDispatchSpy = spyOn(store, 'dispatch').and.callThrough();
      const buttonDebugEl = followButtonDebugEl.query(By.css('button'))
      buttonDebugEl.triggerEventHandler('click');
      expect(mockStoreDispatchSpy).toHaveBeenCalledWith(followActions.follow({ username: componentInstance.username }));
    })

    it('should handle unfollow correctly', () => {
      const { followButtonDebugEl, store, componentInstance } = setup(true, true);
      const mockStoreDispatchSpy = spyOn(store, 'dispatch').and.callThrough();
      const buttonDebugEl = followButtonDebugEl.query(By.css('button'))
      buttonDebugEl.triggerEventHandler('click');
      expect(mockStoreDispatchSpy).toHaveBeenCalledWith(followActions.unfollow({ username: componentInstance.username }));
    })

    it('should redirect to login if no current user', () => {
      const { followButtonDebugEl, mockRouter } = setup(false, false, false);
      const buttonDebugEl = followButtonDebugEl.query(By.css('button'))
      buttonDebugEl.triggerEventHandler('click');
      expect(mockRouter.navigate).toHaveBeenCalledWith(['/login']);
    })
  })
})

function setup(following = false, isFollowedProfile = false, isCurrentUser = true) {
  @Component({
    standalone: true,
    template: `
      <mc-follow-button
        [username]="username"
        [following]="following"
      />
    `,
    imports: [
      FollowButtonComponent
    ]
  })
  class FollowButtonTestHost {
    username = 'Test Username'
    following = following;
  }

  const mockRouter = {
    navigate: jasmine.createSpy('navigate'),
  };

  TestBed.configureTestingModule({
    providers: [
      {
        provide: Router,
        useValue: mockRouter
      },
      provideMockStore({
        initialState: INITIAL_STATE,
        selectors: [
          {
            selector: selectCurrentUser,
            value: isCurrentUser
              ? INITIAL_STATE.currentUser
              : undefined
          },
          {
            selector: selectFollowedProfile,
            value: isFollowedProfile
              ? INITIAL_STATE.followedProfile
              : undefined
          },
        ]
      })
    ]
  })

  const fixture = TestBed.createComponent(FollowButtonTestHost);
  const hostComponentInstance = fixture.componentInstance;
  const followButtonDebugEl = fixture.debugElement.query(By.directive(FollowButtonComponent));
  const componentInstance = followButtonDebugEl.componentInstance as FollowButtonComponent;
  const store = TestBed.inject(MockStore);

  fixture.detectChanges();

  return {
    fixture,
    hostComponentInstance,
    followButtonDebugEl,
    componentInstance,
    store,
    mockRouter,
  }
}

const INITIAL_STATE = {
  currentUser: {},
  followedProfile: {}
}
