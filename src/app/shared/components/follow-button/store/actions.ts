import { createActionGroup, emptyProps, props } from "@ngrx/store";
import { IProfile } from "src/app/shared/types/profile.interface";

export const followActions = createActionGroup({
  source: 'Follow',
  events: {
    'Follow': props<{ username: string }>(),
    'Follow success': props<{ profile: IProfile }>(),
    'Follow failure': emptyProps(),

    'Unfollow': props<{ username: string }>(),
    'Unfollow success': emptyProps(),
    'Unfollow failure': emptyProps(),
  }
})
