import { createFeature, createReducer, on } from "@ngrx/store";
import { IFollowState } from "../types/followState.interface";
import { followActions } from "./actions";
import { routerNavigatedAction } from "@ngrx/router-store";

const initialState: IFollowState = {
  followedProfile: undefined,
}

const followFeature = createFeature({
  name: 'follow',
  reducer: createReducer(
    initialState,
    on(followActions.followSuccess, (state, { profile: followedProfile }) => ({
      ...state,
      followedProfile,
    })),
    on(followActions.unfollowSuccess, state => ({
      ...state,
      followedProfile: null,
    })),
    on(routerNavigatedAction, () => initialState)
  )
})

export const {
  name: followFeatureKey,
  reducer: followReducer,
  selectFollowedProfile,
} = followFeature;
