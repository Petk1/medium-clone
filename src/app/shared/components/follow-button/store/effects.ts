import { switchMap, map, catchError, of } from 'rxjs';
import { inject } from '@angular/core';
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { FollowService } from '../services/follow.service';
import { followActions } from './actions';

export const followEffect = createEffect((
  actions$ = inject(Actions),
  followSrv = inject(FollowService)
) => (
  actions$.pipe(
    ofType(followActions.follow),
    switchMap(({ username }) => (
      followSrv.follow(username)
      .pipe(
        map(profile => followActions.followSuccess({ profile })),
        catchError(() => of(followActions.followFailure()))
      )
    ))
  )
), { functional: true })

export const unfollowEffect = createEffect((
  actions$ = inject(Actions),
  followSrv = inject(FollowService)
) => (
  actions$.pipe(
    ofType(followActions.unfollow),
    switchMap(({ username }) => (
      followSrv.unfollow(username)
      .pipe(
        map(() => followActions.unfollowSuccess()),
        catchError(() => of(followActions.unfollowFailure()))
      )
    ))
  )
), { functional: true })
