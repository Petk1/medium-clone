import { Component, ChangeDetectionStrategy, OnInit, inject, signal } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { IInitConfirmDialogData } from '../../types/initConfirmDialogData.interface';
import { checkObjectProp } from '../../utils/check-object-prop';

@Component({
  selector: 'mc-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    MatDialogModule,
  ]
})
export class ConfirmDialogComponent implements OnInit {
  private readonly initData = inject<IInitConfirmDialogData>(MAT_DIALOG_DATA);
  private readonly dialogRef = inject<MatDialogRef<ConfirmDialogComponent>>(MatDialogRef);
  title = signal('Confirm');
  content = signal('Are you sure?');

  ngOnInit(): void {
    this.setInitData();
  }

  private setInitData(): void {
    if(!this.initData) return;

    Object.keys(this.initData).forEach(prop => {
      if(checkObjectProp(this.initData, prop, 'string')){
        const propName = prop as keyof IInitConfirmDialogData;
        this[propName].set(this.initData[propName] || '');
      }
    })
  }

  onClose(decision: boolean): void {
    this.dialogRef.close(decision);
  }

}
