import { TestBed } from "@angular/core/testing";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { ConfirmDialogComponent } from "./confirm-dialog.component";
import { IInitConfirmDialogData } from "../../types/initConfirmDialogData.interface";

describe('ConfirmDialogComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  it('should initialize with default values', () => {
    const { componentInstance } = setup();
    expect(componentInstance.title()).toBe('Confirm');
    expect(componentInstance.content()).toBe('Are you sure?');
  })

  it('should set initData values', () => {
    const mockData = {
      title: 'Test Title',
      content: 'Test Content'
    };
    const { componentInstance } = setup(mockData);
    expect(componentInstance.title()).toBe(mockData.title);
    expect(componentInstance.content()).toBe(mockData.content);
  })

  it('should close dialog with true when onClose(true) is called', () => {
    const { componentInstance, mockDialogRef } = setup();
    componentInstance.onClose(true);
    expect(mockDialogRef.close).toHaveBeenCalledOnceWith(true);
  })

  it('should close dialog with false when onClose(false) is called', () => {
    const { componentInstance, mockDialogRef } = setup();
    componentInstance.onClose(false);
    expect(mockDialogRef.close).toHaveBeenCalledOnceWith(false);
  })
})

function setup(initData: IInitConfirmDialogData | undefined = undefined) {
  const mockDialogRef = jasmine.createSpyObj('MatDialogRef', ['close']);

  TestBed.configureTestingModule({
    providers: [
      {
        provide: MAT_DIALOG_DATA,
        useValue: initData
      },
      {
        provide: MatDialogRef,
        useValue: mockDialogRef
      }
    ]
  })

  const fixture = TestBed.createComponent(ConfirmDialogComponent);
  const componentInstance = fixture.componentInstance;
  fixture.detectChanges();

  return {
    componentInstance,
    mockDialogRef,
  }
}
