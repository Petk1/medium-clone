import { ChangeDetectionStrategy, Component, Input } from "@angular/core";
import { MatDividerModule } from '@angular/material/divider';
import { TagsComponent } from "../tags/tags.component";
import { IArticle } from "../../types/article.interface";
import { RouterLink } from "@angular/router";
import { ArticleAuthorComponent } from "../article-author/article-author.component";

@Component({
  selector: 'mc-single-article',
  templateUrl: './single-article.component.html',
  styleUrls: ['./single-article.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    MatDividerModule,
    TagsComponent,
    RouterLink,
    ArticleAuthorComponent,
  ],
})
export class SingleArticleComponent {
  @Input({ required: true }) article!: IArticle
}
