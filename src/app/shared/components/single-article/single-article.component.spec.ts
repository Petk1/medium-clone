import { Component, DebugElement } from "@angular/core"
import { DatePipe } from "@angular/common"
import { By } from "@angular/platform-browser"
import { ActivatedRoute } from "@angular/router"
import { TestBed } from "@angular/core/testing"
import { RouterTestingModule } from "@angular/router/testing"
import { TagsComponent } from "../tags/tags.component"
import { SingleArticleComponent } from "./single-article.component"
import { IArticle } from "../../types/article.interface"

describe('SingleArticleComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  it('should set a routerLink correctly', () => {
    const { componentInstance, singleArticleDebugEl } = setup();
    const routerLinkEl = singleArticleDebugEl.query(By.css('[data-testingId="article-content"]')).nativeElement;
    expect(routerLinkEl.getAttribute('ng-reflect-router-link')).toBe(`/article,${componentInstance.article.slug}`);
  });

  describe('Render details', () => {
    let _componentInstance: SingleArticleComponent;
    let _singleArticleDebugEl: DebugElement;
    let _datePipe: DatePipe;
    let passedArticle: IArticle;

    beforeEach(() => {
      const { componentInstance, singleArticleDebugEl, datePipe } = setup();
      _componentInstance = componentInstance;
      _singleArticleDebugEl = singleArticleDebugEl;
      _datePipe = datePipe;
      passedArticle = componentInstance.article;
    })

    it('should render the article details correctly', () => {
      const titleEl = _singleArticleDebugEl.query(By.css('[data-testingId="article-title"]'))?.nativeElement as HTMLElement;
      const descriptionEl = _singleArticleDebugEl.query(By.css('[data-testingId="article-description"]'))?.nativeElement as HTMLElement;
      expect(titleEl.textContent).toContain(passedArticle.title);
      expect(descriptionEl.textContent).toContain(passedArticle.description);
    })

    it('should render an autor data correctly', () => {
      const authorInfoDebugEl = _singleArticleDebugEl.query(By.css('[data-testingId="author-info"]'));
      const authorInfoEl = authorInfoDebugEl.query(By.css('a')).nativeElement as HTMLElement;
      const dateEl = authorInfoDebugEl.query(By.css('[data-testingId="date"]')).nativeElement as HTMLElement;
      expect(authorInfoEl.textContent).toContain(passedArticle.author.username);
      expect(dateEl.textContent).toContain(_datePipe.transform(passedArticle.createdAt, 'fullDate'));
    })

    it('should render the tags correctly', () => {
      const tagsComponent = _singleArticleDebugEl.query(By.directive(TagsComponent)).componentInstance as TagsComponent;
      const tagsDebugEls = _singleArticleDebugEl.queryAll(By.css('[data-testingId="tag"]'));
      const tagList = _componentInstance.article.tagList;
      expect(tagsComponent.tags).toEqual(tagList);
      expect(tagsDebugEls.length).toBe(tagList.length);
    })
  })
})

function setup() {
  @Component({
    standalone: true,
    template: `
      <mc-single-article [article]="article"/>
    `,
    imports: [
      SingleArticleComponent
    ]
  })
  class SingleArticleTestHost {
    article: IArticle = {
      slug: 'testSlug',
      title: "asdas",
      description: "dasd",
      body: "asdasd",
      tagList: [
        "asdasd"
      ],
      createdAt: "2023-11-05T16:47:51.553Z",
      updatedAt: "2023-11-05T16:47:51.553Z",
      favorited: false,
      favoritesCount: 0,
      author: {
        username: "ABC_123_ABC",
        bio: "abcdefghijklmnollko",
        image: "https://api.realworld.io/images/smiley-cyrus.jpeg",
        following: false
      }
    }
  }

  const mockActivatedRoute = {
    snapshot: {
      paramMap: {
        get: () => '',
      },
    },
  }

  TestBed.configureTestingModule({
    imports: [RouterTestingModule],
    providers: [
      {
        provide: ActivatedRoute,
        useValue: mockActivatedRoute,
      },
    ],
  })

  const fixture = TestBed.createComponent(SingleArticleTestHost);
  const singleArticleDebugEl = fixture.debugElement.query(By.directive(SingleArticleComponent));
  const componentInstance = singleArticleDebugEl.componentInstance as SingleArticleComponent;
  const datePipe = new DatePipe('en');

  fixture.detectChanges();

  return {
    fixture,
    singleArticleDebugEl,
    componentInstance,
    datePipe,
  }
}
