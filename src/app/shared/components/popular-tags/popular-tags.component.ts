import { ChangeDetectionStrategy, Component, OnInit, inject } from "@angular/core";
import { NgIf } from "@angular/common";
import { Store } from "@ngrx/store";
import { popularTagsActions } from "./store/actions";
import { selectError, selectIsLoading, selectPopularTagsData } from "./store/reducers";
import { TagsComponent } from "../tags/tags.component";

@Component({
  selector: 'mc-popular-tags',
  template: `
    <h1>Popular Tags</h1>
    @if(popularTags(); as tags) {
      <mc-tags [tags]="tags"/>
    }
  `,
  styles: [`
    @use 'partials/colors';
    @use 'mixins/media-screen';

    :host {
      margin: 0 25px 25px;
    }

    h1 {
      display: flex;
      justify-content: center;
      margin-bottom: 20px;
      font-size: 1.4rem;
      color: colors.$primary-color;
    }

    @include media-screen.small-screens {
      :host {
        max-width: 250px;
      }

      h1 {
        justify-content: flex-end;
        margin-right: 5px;
        min-width: 170px;
      }
    }`
  ],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgIf,
    TagsComponent,
  ]
})
export class PopularTagsComponent implements OnInit {
  private readonly store = inject(Store);

  isLoading = this.store.selectSignal(selectIsLoading);
  error = this.store.selectSignal(selectError);
  popularTags = this.store.selectSignal(selectPopularTagsData);

  ngOnInit(): void {
    this.store.dispatch(popularTagsActions.getPopularTags());
  }
}
