import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { TPopularTag } from 'src/app/shared/types/popularTag.type';
import { IGetPopularTagsResponse } from '../types/getPopularTagsResponse.interface';

@Injectable({
  providedIn: 'root',
})
export class PopularTagsService {
  private http = inject(HttpClient);

  getPopularTags(): Observable<TPopularTag[]> {
    const url = '/tags';
    return this.http.get<IGetPopularTagsResponse>(url)
    .pipe(map(({ tags }) => tags));
  }
}
