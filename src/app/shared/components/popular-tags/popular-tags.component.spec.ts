import { By } from "@angular/platform-browser";
import { TestBed } from "@angular/core/testing";
import { MockStore, provideMockStore } from "@ngrx/store/testing";
import { selectError, selectIsLoading, selectPopularTagsData } from "./store/reducers";
import { PopularTagsComponent } from "./popular-tags.component";
import { IPopularTagsState } from "./types/popularTagsState.interface";

describe('PopularTagsComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  it('should dispatch the getPopularTags action', () => {
    const { store, componentInstance } = setup();
    const mockStoreDispatchSpy = spyOn(store, 'dispatch').and.callThrough();
    componentInstance.ngOnInit()
    expect(mockStoreDispatchSpy).toHaveBeenCalled();
  })

  it('should display all tags from the store', () => {
    const { debugEl } = setup();
    const tagsDebugEls = debugEl.queryAll(By.css('[data-testingId="tag"]'));
    const numberOfTags = INITIAL_STATE.data?.length || 0
    expect(tagsDebugEls.length).toBe(numberOfTags);
  })
})

function setup() {
  TestBed.configureTestingModule({
    providers: [
      provideMockStore({
        initialState: INITIAL_STATE,
        selectors: [
          { selector: selectIsLoading, value: INITIAL_STATE.isLoading },
          { selector: selectError, value: INITIAL_STATE.error },
          { selector: selectPopularTagsData, value: INITIAL_STATE.data },
        ]
      })
    ]
  })

  const fixture = TestBed.createComponent(PopularTagsComponent);
  const debugEl = fixture.debugElement;
  const componentInstance = fixture.componentInstance;
  const store = TestBed.inject(MockStore);

  fixture.detectChanges();

  return {
    debugEl,
    componentInstance,
    store,
  }
}

const INITIAL_STATE: IPopularTagsState = {
  isLoading: false,
  error: null,
  data: ['Tag1', 'Tag2', 'Tag3']
}
