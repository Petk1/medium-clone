import { inject } from '@angular/core';
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { switchMap, map, catchError, of } from 'rxjs';
import { popularTagsActions } from './actions';
import { PopularTagsService } from '../services/popular-tags.service';

export const getPopularTagsEffect = createEffect((
  actions$ = inject(Actions),
  popularTagsSrv = inject(PopularTagsService),
) => (
  actions$.pipe(
    ofType(popularTagsActions.getPopularTags),
    switchMap(() => (
      popularTagsSrv.getPopularTags()
      .pipe(
        map(popularTags => popularTagsActions.getPopularTagsSuccess({ popularTags })),
        catchError(() => of(popularTagsActions.getPopularTagsFailure()))
      )
    ))
  )
), { functional: true })
