import {
  Component,
  ChangeDetectionStrategy,
  Input,
  OnInit,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  inject,
  InjectionToken,
} from "@angular/core";
import { toSignal } from '@angular/core/rxjs-interop';
import { NgFor, NgIf } from "@angular/common";
import { ActivatedRoute, NavigationEnd, NavigationStart, ParamMap, Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { Subject } from "rxjs";
import { filter, map, takeUntil, tap } from "rxjs/operators";
import queryString from "query-string";
import { environment } from "src/environments/environment";
import { articlesActions } from "./store/actions";
import { selectArticlesData, selectError, selectIsLoading } from "./store/reducers";
import { SingleArticleComponent } from "../single-article/single-article.component";
import { PaginationComponent } from "../pagination/pagination.component";
import { LoadingComponent } from "../loading/loading.component";
import { getPageFromParamsMap } from "../../utils/get-page-from-params-map";
import { getUrlItem } from "../../utils/get-url-item";


export const LIMIT_ARTICLES = new InjectionToken(
  'limit articles on one page',
  {
    providedIn: 'root',
    factory: () => environment.limit
  }
)

@Component({
  selector: 'mc-articles',
  template: `
  @if(isLoading()) {
    <mc-loading/>
  }
  @if(!isLoading() && articlesData()?.articles; as articles) {
    @for(article of articles; track article) {
      <mc-single-article [article]="article"/>
    }
    @if(articles?.length && articlesData(); as articlesData) {
      <mc-pagination
        [total]="articlesData.articlesCount"
        [limit]="limit"
        [url]="baseUrlSig()"
        [currentPage]="currentPageNumber"
      />
    }
    @if(!articles?.length) {
      <span class="no-articles">
        There are no articles here...
      </span>
    }
  }
  `,
  styles: [`
    .no-articles {
      font-size: 1.5rem;
      letter-spacing: 2px;
    }
  `],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgIf,
    NgFor,
    SingleArticleComponent,
    PaginationComponent,
    LoadingComponent,
  ]
})
export class ArticlesComponent implements OnInit, OnChanges, OnDestroy {
  @Input({ required: true }) apiUrl!: string;

  private readonly store = inject(Store);
  private readonly router = inject(Router);
  private readonly activatedRoute = inject(ActivatedRoute);
  private readonly destroy$ = new Subject<void>();

  isLoading = this.store.selectSignal(selectIsLoading);
  error = this.store.selectSignal(selectError);
  articlesData = this.store.selectSignal(selectArticlesData);

  readonly limit = inject(LIMIT_ARTICLES);

  readonly baseUrlSig = toSignal(this.router.events
    .pipe(
      filter((routerEvent): routerEvent is NavigationEnd => routerEvent instanceof NavigationEnd),
      map(() => getUrlItem(this.router.url, 0) || '')
    ), {
      initialValue: getUrlItem(this.router.url, 0) || ''
    }
  )

  currentPageNumber = 1;
  private readonly currentPage$ = this.activatedRoute.queryParamMap
    .pipe(
      map(params => getPageFromParamsMap(params)),
      tap(currentPage => currentPage && this.getArticles(currentPage || 1)),
      tap(currentPage => this.currentPageNumber = currentPage || 1),
      takeUntil(this.destroy$)
    );

  ngOnInit(): void {
    this.currentPage$.subscribe();

    const page = this.getPageFromSnapshot();
    if(!page) {
      this.getArticles(this.currentPageNumber);
    }

    // To handle the case when the user uses the menu to navigate to the 'Home' page being on the 'Home" page
    // and has a page number selected. In this case after selecting the 'Home' option from the menu
    // let's call the getArticles method with page number 1 as an argument.
    // It calls the getArticles method only when urlPath before navigation was 'global-feed'
    // because for the 'your-feed' it is triggered in the ngOnChanges hook (the apiUrl is changed
    // in this case because when the user selected before navigation
    // the 'your-feed' option, during navigation to 'global-feed' it changes)
    this.router.events
    .pipe(
      tap(routerEvent => {
        if(routerEvent instanceof NavigationStart && routerEvent.url === '/') {
          const urlPath = this.activatedRoute.snapshot.url[0].path;
          const pageNumber = this.getPageFromSnapshot();

          if(urlPath === 'global-feed' && pageNumber){
            this.getArticles(1);
          }
        }
      }),
      takeUntil(this.destroy$)
    )
    .subscribe()
  }

  ngOnChanges(changes: SimpleChanges): void {
    const apiUrl = changes['apiUrl'];
    const isApiUrlChanged = !apiUrl.firstChange && apiUrl.currentValue !== apiUrl.previousValue;

    if(isApiUrlChanged) {
      this.getArticles(this.currentPageNumber);
      return;
    }
  }

  private getArticles(currentPage: number): void {
    const offset = currentPage * this.limit - this.limit;
    const parsedUrl = queryString.parseUrl(this.apiUrl);

    const stringifiedParams = queryString.stringify({
      limit: this.limit,
      offset,
      ...parsedUrl.query
    });

    const url = `${parsedUrl.url}?${stringifiedParams}`;
    this.store.dispatch(articlesActions.getArticles({ url }));
  }

  private getPageFromSnapshot() {
    return this.activatedRoute.snapshot.queryParamMap.get('page');
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
