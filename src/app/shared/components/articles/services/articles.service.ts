import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IGetArticlesResponse } from '../types/getArticlesResponse.interface';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {
  private http = inject(HttpClient);

  getArticles(url: string): Observable<IGetArticlesResponse> {
    return this.http.get<IGetArticlesResponse>(url);
  }
}
