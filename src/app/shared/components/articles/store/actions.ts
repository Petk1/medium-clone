import { createActionGroup, emptyProps, props } from "@ngrx/store";
import { IGetArticlesResponse } from "../types/getArticlesResponse.interface";

export const articlesActions = createActionGroup({
  source: 'articles',
  events: {
    'Get articles': props<{ url: string }>(),
    'Get articles success': props<{ articlesData: IGetArticlesResponse }>(),
    'Get articles failure': emptyProps(),
  }
})
