import { inject } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { switchMap, map, catchError, of, tap } from 'rxjs';
import { articlesActions } from "./actions";
import { ArticlesService } from "../services/articles.service";

export const getArticlesEffect = createEffect((
  actions$ = inject(Actions),
  articlesSrv = inject(ArticlesService),
) => (
  actions$.pipe(
    ofType(articlesActions.getArticles),
    switchMap(({ url }) => (
      articlesSrv.getArticles(url)
      .pipe(
        map(articlesData => articlesActions.getArticlesSuccess({ articlesData })),
        catchError(() => of(articlesActions.getArticlesFailure()))
      )
    ))
  )
), { functional: true })
