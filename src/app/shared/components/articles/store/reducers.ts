import { createFeature, createReducer, on } from "@ngrx/store";
import { IArticlesState } from "../types/articlesState.interface";
import { articlesActions } from "./actions";
import { routerNavigatedAction } from "@ngrx/router-store";

const initialState: IArticlesState = {
  isLoading: false,
  error: null,
  articlesData: null,
}

const articlesFeature = createFeature({
  name: 'articles',
  reducer: createReducer(
    initialState,
    on(articlesActions.getArticles, state => ({
      ...state,
      isLoading: true,
    })),
    on(articlesActions.getArticlesSuccess, (state, { articlesData }) => ({
      ...state,
      isLoading: false,
      articlesData,
    })),
    on(articlesActions.getArticlesFailure, state => ({
      ...state,
      isLoading: false,
    })),
    on(routerNavigatedAction, (state, { payload }) => { // when jumping to another page it resets the whole state
      // To prevent the state reset when the user being on 'Home' page uses an menu option
      // to navigate to the 'Home' page/
      // The same case happens on the profile page.
      const url = payload.event.url;
      if(url === '/' || url.includes('/profile/')) {
        return state;
      }

      return initialState;
    })
  )
})

export const {
  name: articlesFeatureKey,
  reducer: articlesReducer,
  selectIsLoading,
  selectError,
  selectArticlesData,
} = articlesFeature;
