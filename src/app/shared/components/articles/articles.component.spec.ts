import { Component } from "@angular/core"
import { DatePipe } from "@angular/common";
import { ActivatedRoute, NavigationEnd, NavigationStart, Params, Router, convertToParamMap } from "@angular/router";
import { TestBed } from "@angular/core/testing";
import { By } from "@angular/platform-browser";
import { of } from "rxjs";
import { MockState, MockStore, provideMockStore } from '@ngrx/store/testing';
import { articlesActions } from "./store/actions";
import { selectArticlesData, selectError, selectIsLoading } from "./store/reducers";
import { ArticlesComponent, LIMIT_ARTICLES } from "./articles.component"
import { SingleArticleComponent } from "../single-article/single-article.component";
import { IArticlesState } from "./types/articlesState.interface";
import { IGetArticlesResponse } from "./types/getArticlesResponse.interface";

describe('ArticlesComponent', () => {
  it('should create', () => {
    const { fixture, articlesDebugEl, } = setup();
    expect(fixture).toBeTruthy();
    expect(articlesDebugEl).toBeTruthy();
  })

  it('should display properly each article', () => {
    const { articlesDebugEl, datePipe } = setup();
    const arrayOfSingleArticlesDebugEls = articlesDebugEl.queryAll(By.directive(SingleArticleComponent));
    const storedArticles = INITIAL_STATE.articlesData?.articles;

    arrayOfSingleArticlesDebugEls.forEach((articleDebugEl, index) => {
      const article = storedArticles?.[index];
      expect(article).toBeTruthy();
      if(!article) {
        return;
      }

      const authorInfoDebugEl = articleDebugEl.query(By.css('[data-testingId="author-info"]'));
      const authorInfoAEl = authorInfoDebugEl.query(By.css('a')).nativeElement as HTMLElement;
      const dateEl = articleDebugEl.query(By.css('[data-testingId="date"]')).nativeElement as HTMLElement;
      const titleEl = articleDebugEl.query(By.css('[data-testingId="article-title"]'))?.nativeElement as HTMLElement;
      const descriptionEl = articleDebugEl.query(By.css('[data-testingId="article-description"]'))?.nativeElement as HTMLElement;
      const tagsDebugEls = articleDebugEl.queryAll(By.css('[data-testingId="tag"]'));

      expect(authorInfoAEl.textContent).toContain(article.author.username);
      expect(dateEl.textContent).toContain(datePipe.transform(article.createdAt, 'fullDate'));
      expect(titleEl.textContent).toContain(article.title);
      expect(descriptionEl.textContent).toContain(article.description);

      tagsDebugEls.forEach((tagDebugEl, index) => {
        const storedTag = article.tagList[index];
        expect(storedTag).toBeTruthy();
        expect((<HTMLElement>tagDebugEl?.nativeElement as HTMLElement).textContent).toContain(storedTag);
      })
    })
  })

  describe('Pagination', () => {
    it('should display a limited number of articles on one page', () => {
      const { articlesDebugEl } = setup('', 1);
      expect(articlesDebugEl.queryAll(By.directive(SingleArticleComponent)).length).toBe(1);
    })

    it('should paginate articles', () => {
      const { articlesDebugEl } = setup('', 1);
      const pageLinksDebugEls = articlesDebugEl.queryAll(By.css('[data-testingId="page-link"]'));
      expect(pageLinksDebugEls.length).toBe(2);
    })

    it('should fetch the articles for the current page number', () => {
      const { fixture, articlesDebugEl, store } = setup('', 1);
      const mockStoreDispatchSpy = spyOn(store, 'dispatch').and.callThrough();

      (articlesDebugEl.componentInstance as ArticlesComponent)['getArticles'](2);
      expect(mockStoreDispatchSpy).toHaveBeenCalledWith(articlesActions.getArticles({
        url: 'test/api/url?limit=1&offset=1'
      }));
    })
  })
})

function setup(url: string = '', limit: number | undefined = 10) {
  @Component({
    standalone: true,
    imports: [
      ArticlesComponent
    ],
    template: `<mc-articles [apiUrl]="apiUrl"/>`
  })
  class ArticlesComponentTestHost {
    apiUrl = 'test/api/url';
  }

  const mockRouter = {
    url,
    events: of(new NavigationEnd(1, 'url', 'urlAfterRedirects')),
    navigate: jasmine.createSpy('navigate'),
    createUrlTree: () => {},
    serializeUrl: () => {},
  };

  const params: Params = {
    page: 1
  }

  const mockActivatedRoute = {
    queryParamMap: of(convertToParamMap(params)),
    snapshot: {
      queryParamMap: {
        get: jasmine.createSpy('get').and.returnValue('1')
      },
      url: [{ path: 'global-feed' }]
    },
    events: of(new NavigationStart(1, 'url'))
  };

  const mockArticles: IGetArticlesResponse = {
    articles: INITIAL_STATE.articlesData?.articles.slice(0, limit) || [],
    articlesCount: INITIAL_STATE.articlesData?.articlesCount || 0,
  }

  TestBed.configureTestingModule({
    providers: [
      {
        provide: ActivatedRoute,
        useValue: mockActivatedRoute
      },
      {
        provide: Router,
        useValue: mockRouter
      },
      {
        provide: LIMIT_ARTICLES,
        useValue: limit
      },
      provideMockStore({
        initialState: INITIAL_STATE,
        selectors: [
          { selector: selectIsLoading, value: INITIAL_STATE.isLoading },
          { selector: selectError, value: INITIAL_STATE.error },
          { selector: selectArticlesData, value: mockArticles },
        ]
      })
    ]
  })

  const fixture = TestBed.createComponent(ArticlesComponentTestHost);
  const articlesDebugEl = fixture.debugElement.query(By.directive(ArticlesComponent));
  const hostComponent = fixture.componentInstance;
  const store = TestBed.inject(MockStore);
  const datePipe = new DatePipe('en');

  fixture.detectChanges();

  return {
    fixture,
    articlesDebugEl,
    hostComponent,
    datePipe,
    store,
  }
}

const INITIAL_STATE: IArticlesState = {
  isLoading: false,
  error: null,
  articlesData: {
    articles: [
      {
        slug: "asdas-240342",
        title: "asdas",
        description: "dasd",
        body: "asdasd",
        tagList: [
          "asdasd"
        ],
        createdAt: "2023-11-05T16:47:51.553Z",
        updatedAt: "2023-11-05T16:47:51.553Z",
        favorited: false,
        favoritesCount: 0,
        author: {
          username: "ABC_123_ABC",
          bio: "abcdefghijklmnollko",
          image: "https://api.realworld.io/images/smiley-cyrus.jpeg",
          following: false
        }
      },
      {
        slug: "sadasd-240342",
        title: "sadasd",
        description: "asdas",
        body: "dasda",
        tagList: [
          "sdasd",
          'ABC',
          'test tag'
        ],
        createdAt: "2023-11-05T16:14:21.006Z",
        updatedAt: "2023-11-05T16:14:21.006Z",
        favorited: false,
        favoritesCount: 0,
        author: {
          username: "ABC_123_ABC",
          bio: "abcdefghijklmnollko",
          image: "https://api.realworld.io/images/smiley-cyrus.jpeg",
          following: false
        }
      }
    ],
    articlesCount: 2
  }
}
