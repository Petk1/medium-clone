import { IGetArticlesResponse } from "./getArticlesResponse.interface";

export interface IArticlesState {
  isLoading: boolean;
  error: string | null;
  articlesData: IGetArticlesResponse | null;
}
