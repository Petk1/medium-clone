import { Component } from "@angular/core";
import { TestBed } from "@angular/core/testing";
import { RouterTestingModule } from '@angular/router/testing';
import { By } from "@angular/platform-browser";
import { ArticleAuthorComponent } from "./article-author.component";

describe('ArticleAuthorComponent', () => {
  it('should create', () => {
    const { fixture, articleAuthorDebugEl } = setup();
    expect(fixture).toBeTruthy();
    expect(articleAuthorDebugEl).toBeTruthy();
  })

  it('should display an author username with correct color and link', () => {
    const { articleAuthorDebugEl, hostComponent } = setup();
    const { authorUsername, authorUsernameColor } = hostComponent;
    const authorInfoDebugEl = articleAuthorDebugEl.query(By.css('[data-testingId="author-info"]'));
    const aEl = authorInfoDebugEl.query(By.css('a')).nativeElement as HTMLElement;

    expect(aEl.textContent).toContain(authorUsername);
    expect(aEl.style.color).toBe(authorUsernameColor);
    expect(aEl.getAttribute('ng-reflect-router-link')).toBe(`/profile,${authorUsername}`)
  })

  it('should display an article creation date with correct color', () => {
    const { fixture, articleAuthorDebugEl, hostComponent } = setup();
    hostComponent.articleCreatedAt = '2023-11-15T16:14:21.006Z';
    fixture.detectChanges();

    const dateEl = articleAuthorDebugEl.query(By.css('[data-testingId="date"]')).nativeElement as HTMLElement;

    expect(dateEl.textContent).toContain('Wednesday, November 15, 2023');
    expect(dateEl.style.color).toBe('green');

    hostComponent.articleCreatedAt = '2023-11-17T16:14:21.006Z';
    hostComponent.articleCreatedAtColor = 'yellow';
    fixture.detectChanges();

    expect(dateEl.textContent).toContain('Friday, November 17, 2023');
    expect(dateEl.style.color).toBe('yellow');
  })
})

function setup() {
  @Component({
    standalone: true,
    imports: [
      ArticleAuthorComponent,
      RouterTestingModule,
    ],
    template: `
      <mc-article-author
        [authorAvatarImage]="authorAvatarImage"
        [authorUsername]="authorUsername"
        [articleCreatedAt]="articleCreatedAt"
        [authorUsernameColor]="authorUsernameColor"
        [articleCreatedAtColor]="articleCreatedAtColor"
      />
    `
  })
  class ArticleAuthorTestHost {
    authorAvatarImage = '';
    authorUsername = 'Test Username';
    articleCreatedAt =  '2023-11-05T16:14:21.006Z';
    authorUsernameColor = 'blue';
    articleCreatedAtColor = 'green';
  }

  const fixture = TestBed.createComponent(ArticleAuthorTestHost);
  const articleAuthorDebugEl = fixture.debugElement.query(By.directive(ArticleAuthorComponent));
  const hostComponent = fixture.componentInstance;

  fixture.detectChanges();

  return {
    fixture,
    articleAuthorDebugEl,
    hostComponent,
  }
}
