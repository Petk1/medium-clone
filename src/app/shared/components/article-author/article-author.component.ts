import { DatePipe } from "@angular/common";
import { ChangeDetectionStrategy, Component, Input } from "@angular/core";
import { RouterLink } from "@angular/router";

@Component({
  selector: 'mc-article-author',
  template: `
    <div class="avatar">
      <img [src]="authorAvatarImage" alt="avatar">
    </div>
    <div class="author-info" data-testingId="author-info">
      <a class="name" [style.color]="authorUsernameColor"
        [routerLink]="['/profile', authorUsername]"
      >
        {{ authorUsername }}
      </a>
      <span class="date" [style.color]="articleCreatedAtColor" data-testingId="date">
        {{ articleCreatedAt | date:"fullDate" }}
      </span>
    </div>
  `,
  styles: [`
    @use 'partials/colors';

    :host {
      display: flex;
      flex-direction: row;

      .avatar {
        margin-right: 10px;

        >img {
          width: 40px;
          height: 40px;
          border-radius: 50%;
        }
      }

      .author-info {
        display: flex;
        flex-direction: column;

        .name {
          font-size: 1.5rem;
          color: colors.$primary-color;
          text-decoration: none;
        }

        .date {
          color: colors.$complementary-color;
        }
      }
    }
  `],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    DatePipe,
    RouterLink,
  ]
})
export class ArticleAuthorComponent {
  @Input({ required: true }) authorAvatarImage = '';
  @Input({ required: true }) authorUsername = '';
  @Input({ required: true }) articleCreatedAt = '';
  @Input() authorUsernameColor = '';
  @Input() articleCreatedAtColor = '';
}
