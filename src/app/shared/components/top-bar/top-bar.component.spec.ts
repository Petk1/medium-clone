import { DebugElement } from "@angular/core";
import { By } from "@angular/platform-browser";
import { TestBed } from "@angular/core/testing";
import { provideMockStore } from "@ngrx/store/testing";
import { selectCurrentUser } from "src/app/auth/store/reducers";
import { TopBarComponent } from "./top-bar.component";
import { StandardMenuComponent } from "../standard-menu/standard-menu.component";
import { HamburgerMenuComponent } from "../hamburger-menu/hamburger-menu.component";

describe('TopBarComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  it('should display the appropriate menu type depending on a screen width', () => {
    const { fixture, getHamburgerMenu, getStandardMenu } = setup();
    const spyInnerWidth = spyOnProperty(window, 'innerWidth')

    const check = (width: number, expectedArgument: () => DebugElement) => {
      spyInnerWidth.and.returnValue(width);
      window.dispatchEvent(new Event('resize'));
      fixture.detectChanges();
      expect(expectedArgument()).toBeTruthy();
    }

    check(500, getHamburgerMenu);
    check(768, getStandardMenu);
    check(100, getHamburgerMenu);
    check(1920, getStandardMenu);
    check(769, getStandardMenu);
    check(767, getHamburgerMenu);
    check(344, getHamburgerMenu);
  })
})

function setup() {
  TestBed.configureTestingModule({
    providers: [
      provideMockStore({
        initialState: {},
        selectors: [
          { selector: selectCurrentUser, value: {} },
        ]
      })
    ]
  })

  const fixture = TestBed.createComponent(TopBarComponent);
  const componentInstance = fixture.componentInstance;

  fixture.detectChanges();

  const getHamburgerMenu = () => fixture.debugElement.query(By.directive(HamburgerMenuComponent));
  const getStandardMenu = () => fixture.debugElement.query(By.directive(StandardMenuComponent));

  return {
    fixture,
    componentInstance,
    getHamburgerMenu,
    getStandardMenu,
  }
}
