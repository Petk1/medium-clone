import { ChangeDetectionStrategy, Component, HostListener, inject } from "@angular/core";
import { NgIf } from "@angular/common";
import { Store } from "@ngrx/store";
import { HamburgerMenuComponent } from "../hamburger-menu/hamburger-menu.component";
import { StandardMenuComponent } from "../standard-menu/standard-menu.component";
import { selectCurrentUser } from "src/app/auth/store/reducers";

@Component({
  selector: 'mc-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgIf,
    HamburgerMenuComponent,
    StandardMenuComponent,
  ],
})
export class TopBarComponent {
  private store = inject(Store);
  windowWidth = window.innerWidth;
  currentUser = this.store.selectSignal(selectCurrentUser);

  @HostListener('window:resize')
  onResize = () => this.windowWidth = window.innerWidth;

}
