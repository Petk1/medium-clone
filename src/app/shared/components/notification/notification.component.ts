import { Component, ChangeDetectionStrategy, OnInit, inject, signal } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { IInitNotificationData } from '../../types/initNotificationData';
import { checkObjectProp } from '../../utils/check-object-prop';

@Component({
  selector: 'mc-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    MatDialogModule,
  ]
})
export class NotificationComponent implements OnInit {
  private readonly initData = inject<IInitNotificationData>(MAT_DIALOG_DATA);
  title = signal('Information');
  content = signal('');

  ngOnInit(): void {
    this.setInitData();
  }

  private setInitData() {
    if(!this.initData) return;

    Object.keys(this.initData).forEach(prop => {
      if(checkObjectProp(this.initData, prop, 'string')){
        const propName = prop as keyof IInitNotificationData;
        this[propName].set(this.initData[propName] || '');
      }
    })
  }

}
