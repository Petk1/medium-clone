import { TestBed } from "@angular/core/testing";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { NOTIFICATION_DIALOG_CONFIG, NotificationService } from "./notification.service"
import { NotificationComponent } from "../notification.component";
import { IInitNotificationData } from "src/app/shared/types/initNotificationData";

describe('NotificationService', () => {
  it('should open a notification', () => {
    const { notificationSrv, mockMatDialog } = setup();
    notificationSrv.newNotification();
    expect(mockMatDialog.open).toHaveBeenCalled();
  })

  it('should use a default config', () => {
    const { notificationSrv, mockMatDialog, notificationDialogConfig } = setup();
    notificationSrv.newNotification();
    expect(mockMatDialog.open).toHaveBeenCalledWith(NotificationComponent, notificationDialogConfig);
  })

  it('should use a passed config', () => {
    const { notificationSrv, mockMatDialog, notificationDialogConfig } = setup();

    const passedConfig: MatDialogConfig<IInitNotificationData> = {
      data: {
        title: 'Error occured',
        content: 'Something went wrong'
      },
      width: '500px',
      height: '300px'
    };

    notificationSrv.newNotification(passedConfig);
    const config = { ...notificationDialogConfig, ...passedConfig };

    expect(mockMatDialog.open).toHaveBeenCalledWith(NotificationComponent, config);
  })
})

function setup() {
  const mockMatDialog = {
    open: jasmine.createSpy('open').and.callThrough()
  }

  TestBed.configureTestingModule({
    providers: [
      {
        provide: MatDialog,
        useValue: mockMatDialog
      }
    ]
  })

  const notificationSrv = TestBed.inject(NotificationService);
  const notificationDialogConfig = TestBed.inject(NOTIFICATION_DIALOG_CONFIG);

  return {
    notificationSrv,
    mockMatDialog,
    notificationDialogConfig,
  }
}
