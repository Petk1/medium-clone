import { Injectable, InjectionToken, inject } from "@angular/core";
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { NotificationComponent } from "../notification.component";
import { IInitNotificationData } from "../../../types/initNotificationData";

export const NOTIFICATION_DIALOG_CONFIG = new InjectionToken<MatDialogConfig<IInitNotificationData>>(
  'notification config',
  {
    providedIn: 'root',
    factory: () => {
      const dialogConfig = new MatDialogConfig<IInitNotificationData>();
      dialogConfig.width = '440px';
      dialogConfig.height = '275px';
      dialogConfig.closeOnNavigation = true;
      dialogConfig.position = {
        top: '125px',
      }

      return dialogConfig;
    }
  }
)

@Injectable({ providedIn: 'root' })
export class NotificationService {
  private readonly matDialog = inject(MatDialog);
  private readonly dialogConfig = inject(NOTIFICATION_DIALOG_CONFIG);

  newNotification(config?: MatDialogConfig<IInitNotificationData>): MatDialogRef<NotificationComponent> {
    let dialogConfig = this.dialogConfig;

    if(config) {
      dialogConfig = { ...dialogConfig, ...config };
    }

    return this.matDialog.open(NotificationComponent, dialogConfig);
  }

}
