import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { TestBed } from "@angular/core/testing";
import { NotificationComponent } from "./notification.component";
import { IInitNotificationData } from "../../types/initNotificationData";

describe('NotificationComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  it('should initialize with default values', () => {
    const { componentInstance } = setup();
    expect(componentInstance.title()).toBe('Information');
    expect(componentInstance.content()).toBe('');
  })

  it('should set initData values', () => {
    const mockData = {
      title: 'Test Title',
      content: 'Test Content'
    };
    const { componentInstance } = setup(mockData);
    expect(componentInstance.title()).toBe(mockData.title);
    expect(componentInstance.content()).toBe(mockData.content);
  })
})

function setup(initData: IInitNotificationData | undefined = undefined) {
  TestBed.configureTestingModule({
    providers: [
      {
        provide: MAT_DIALOG_DATA,
        useValue: initData
      }
    ]
  })

  const fixtrue = TestBed.createComponent(NotificationComponent);
  const componentInstance = fixtrue.componentInstance;

  fixtrue.detectChanges();

  return {
    componentInstance,
  }
}
