import { ChangeDetectionStrategy, Component, Input, inject } from '@angular/core';
import { NgFor } from '@angular/common';
import { RouterLink } from '@angular/router';
import { ICurrentUser } from '../../types/currentUser.interface';
import { ROUTER_CONFIG } from 'src/app/core/router-config/router-config.token';
import { NavigationService } from '../../services/navigation.service';

@Component({
  selector: 'mc-standard-menu',
  template: `
    <ul class="options">
      @for(link of currentUser ? routerConfig.userLoggedIn : routerConfig.userNotLoggedIn; track link) {
        <li (click)="onNavigation(link.link)">
          <span>{{ link.name }}</span>
        </li>
      }
    </ul>
  `,
  styleUrls: ['./standard-menu.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgFor,
    RouterLink,
  ],
})
export class StandardMenuComponent {
  @Input({ required: true }) currentUser!: ICurrentUser | null | undefined;

  readonly routerConfig = inject(ROUTER_CONFIG);
  private readonly navigationSrv = inject(NavigationService);

  onNavigation(link: string): void {
    this.navigationSrv.onNavigation(link);
  }
}
