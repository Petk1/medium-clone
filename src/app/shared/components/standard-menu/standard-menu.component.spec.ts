import { Component, DebugElement } from "@angular/core";
import { ICurrentUser } from "../../types/currentUser.interface";
import { StandardMenuComponent } from "./standard-menu.component";
import { TestBed } from "@angular/core/testing";
import { NavigationService } from "../../services/navigation.service";
import { ROUTER_CONFIG } from "src/app/core/router-config/router-config.token";
import { IRouterConfig } from "src/app/core/router-config/types/routerConfig.interface";
import { By } from "@angular/platform-browser";

describe('StandardMenuComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  describe('User is NOT logged in', () => {
    let _mockNavigationService: any;
    let routerConfig: IRouterConfig;
    let liDebugEls: DebugElement[];

    beforeEach(() => {
      const { standardMenuDebugEl, mockNavigationService } = setup();
      _mockNavigationService = mockNavigationService;
      routerConfig = TestBed.inject<IRouterConfig>(ROUTER_CONFIG);
      liDebugEls = liDebugEls = standardMenuDebugEl.queryAll(By.css('li'));
    })

    it('should initialize with all injected menu options', () => {
      expect(liDebugEls.length).toBe(routerConfig.userNotLoggedIn.length);
    })

    it('should navigate on a menu item click', () => {
      liDebugEls.forEach((liDebugEl, index) => {
        liDebugEl.triggerEventHandler('click');
        expect(_mockNavigationService.onNavigation).toHaveBeenCalledWith(routerConfig.userNotLoggedIn[index].link);
      })
    })
  })

  describe('User is logged in', () => {
    let _mockNavigationService: any;
    let routerConfig: IRouterConfig;
    let liDebugEls: DebugElement[];

    beforeEach(() => {
      const { standardMenuDebugEl, mockNavigationService } = setup({} as ICurrentUser);
      _mockNavigationService = mockNavigationService;
      routerConfig = TestBed.inject<IRouterConfig>(ROUTER_CONFIG);
      liDebugEls = liDebugEls = standardMenuDebugEl.queryAll(By.css('li'));
    })

    it('should initialize with all injected menu options (user is logged in)', () => {
      expect(liDebugEls.length).toBe(routerConfig.userLoggedIn.length);
    })

    it('should navigate on a menu item click (user is logged in)', () => {
      liDebugEls.forEach((liDebugEl, index) => {
        liDebugEl.triggerEventHandler('click');
        expect(_mockNavigationService.onNavigation).toHaveBeenCalledWith(routerConfig.userLoggedIn[index].link);
      })
    })
  })
})

function setup(currentUser: ICurrentUser | null | undefined = undefined) {
  @Component({
    standalone: true,
    template: `
      <mc-standard-menu [currentUser]="currentUser"/>
    `,
    imports: [
      StandardMenuComponent
    ],
  })
  class StandardMenuTestHost {
    currentUser = currentUser;
  }

  const mockNavigationService = jasmine.createSpyObj('NavigationService', ['onNavigation']);

  TestBed.configureTestingModule({
    providers: [
      { provide: NavigationService, useValue: mockNavigationService },
      {
        provide: ROUTER_CONFIG,
        useValue: {
          userLoggedIn: [
            {
              name: 'Home',
              link: '/'
            },
            {
              name: 'New article',
              link: '/new-article'
            }
          ],
          userNotLoggedIn: [
            {
              name: 'Home',
              link: '/'
            },
            {
              name: 'Sign In',
              link: '/login'
            },
            {
              name: 'Sign Up',
              link: '/registration'
            }
          ]
        } as IRouterConfig
      }
    ]
  })

  const fixture = TestBed.createComponent(StandardMenuTestHost);
  const standardMenuDebugEl = fixture.debugElement.query(By.directive(StandardMenuComponent));
  const componentInstance = standardMenuDebugEl.componentInstance;

  fixture.detectChanges();

  return {
    fixture,
    standardMenuDebugEl,
    componentInstance,
    mockNavigationService,
  }
}
