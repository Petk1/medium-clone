import { IProfile } from "src/app/shared/types/profile.interface";

export interface IComment {
  id: number;
  createdAt: string,
  updatedAt: string,
  body: string,
  author: IProfile,
}
