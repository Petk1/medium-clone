export interface IInitNotificationData {
  title?: string;
  content?: string;
}
