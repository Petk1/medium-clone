import { IProfile } from "./profile.interface";

export interface IFollowResponse {
  profile: IProfile;
}
