export interface IInitConfirmDialogData {
  title: string;
  content: string;
}
