import { Injectable, inject } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { selectCurrentUser } from 'src/app/auth/store/reducers';

@Injectable({ providedIn: 'root' })
export class NavigationService {
  private readonly router = inject(Router);
  private readonly currentUser = inject(Store).selectSignal(selectCurrentUser);

  onNavigation(link: string): void {
    if(link.includes(':username')) {
      const username = this.currentUser()?.username;
      if(username) {
        link = link.replace(':username', username);
      } else {
        console.warn('Username is unavailable.')
      }
    }

    this.router.navigateByUrl(link);
  }

}
