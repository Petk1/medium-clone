import { Injectable } from "@angular/core";

@Injectable({ providedIn: 'root' })
export class UserTokenService {
  set(key: string, data: unknown): void {
    try {
      localStorage.setItem(key, JSON.stringify(data));
    } catch(error) {
      const errorMsg = 'Failure to saving token to local storage';
      console.error(errorMsg, error)
      throw new Error(errorMsg)
    }
  }

  get(key: string): unknown {
    try {
      const localStorageItem = localStorage.getItem(key);
      return localStorageItem ? JSON.parse(localStorageItem) : null;
    } catch(error) {
      console.error('Failure to getting token from local storage', error);
      return null;
    }
  }

  remove(key: string) {
    try {
      localStorage.removeItem(key);
    } catch(error) {
      const errorMsg = 'Failure to removing token from local storage';
      console.error(errorMsg, error)
      throw new Error(errorMsg)
    }
  }
}
