import { TestBed } from '@angular/core/testing';
import { signal } from "@angular/core";
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { NavigationService } from "./navigation.service"

describe('NavigationService', () => {
  it('should navigate to link with replaced ":username" with current username', () => {
    const { navigationSrv, mockRouter } = setup();
    navigationSrv.onNavigation('/user/:username');
    expect(mockRouter.navigateByUrl).toHaveBeenCalledWith('/user/TestUser');
  })

  it('should navigate to the passed link if link does not contain ":username"', () => {
    const { navigationSrv, mockRouter } = setup();
    navigationSrv.onNavigation('/page');
    expect(mockRouter.navigateByUrl).toHaveBeenCalledWith('/page');
  })

  it('should not replace ":username" if a current username is not available', () => {
    const { navigationSrv, mockRouter } = setup(null);
    navigationSrv.onNavigation('/user/:username');
    expect(mockRouter.navigateByUrl).toHaveBeenCalledWith('/user/:username');
  })
})

function setup(username: string | null = 'TestUser') {
  let navigationSrv: NavigationService;
  const mockRouter = {
    navigateByUrl: jasmine.createSpy('navigateByUrl')
  }
  const mockStore = {
    selectSignal: jasmine.createSpy('selectSignal').and.returnValue(
      signal({ username })
    )
  }

  TestBed.configureTestingModule({
    providers: [
      {
        provide: Store,
        useValue: mockStore
      },
      {
        provide: Router,
        useValue: mockRouter
      }
    ]
  })

  navigationSrv = TestBed.inject(NavigationService);

  return {
    navigationSrv,
    mockRouter,
  }
}
