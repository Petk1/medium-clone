import { TestBed } from "@angular/core/testing"
import { UserTokenService } from "./user-token.service"

describe('UserTokenService', () => {
  let userTokenSrv: UserTokenService;

  beforeEach(() => {
    userTokenSrv = TestBed.inject(UserTokenService);
  })

  afterEach(() => {
    localStorage.clear();
  })

  it('should set data to local storage', () => {
    const key = 'testKey';
    const data = { token: 'testToken' };
    userTokenSrv.set(key, data);
    const storedData = JSON.parse(localStorage.getItem(key) as string);
    expect(storedData).toEqual(data);
  })

  it('should get data from local storage', () => {
    const key = 'testKey';
    const data = { token: 'testToken' };
    localStorage.setItem(key, JSON.stringify(data));
    expect(userTokenSrv.get(key)).toEqual(data);
  })

  it('should set an get data from local storage', () => {
    const key = 'testKey';
    const data = { token: 'testToken' };
    userTokenSrv.set(key, data);
    expect(userTokenSrv.get(key)).toEqual(data);
  })

  it('should return null if data does not exist in local storage', () => {
    const key = 'testNonExistentKey';
    expect(userTokenSrv.get(key)).toBeNull();
  })

  it('should remove data from local storage', () => {
    const key = 'testKey';
    const data = { token: 'testToken' };
    localStorage.setItem(key, JSON.stringify(data));
    userTokenSrv.remove(key);
    expect(localStorage.getItem(key)).toBeNull();
  });

  it('should handle errors when setting data to local storage', () => {
    spyOn(localStorage, 'setItem').and.throwError('Error setting item');
    expect(() => userTokenSrv.set('testKey', 'testData')).toThrowError(/Failure/);
  });

  it('should return null when an error occurred while getting data from local storage', () => {
    spyOn(localStorage, 'getItem').and.throwError('Error getting item');
    const key = 'testKey';
    const data = { token: 'testToken' };
    localStorage.setItem(key, JSON.stringify(data));
    expect(userTokenSrv.get('testKey')).toBeNull();
  });

  it('should handle errors when removing data from local storage', () => {
    spyOn(localStorage, 'removeItem').and.throwError('Error remmoving item');
    expect(() => userTokenSrv.remove('testKey')).toThrowError(/Failure/);
  });
})
