import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { IArticle } from '../types/article.interface';
import { IArticleResponse } from '../types/articleResponse.interface';
import { INewArticleResponse } from '../components/article-form/types/newArticleResponse.interface';
import { INewArticleRequest } from '../components/article-form/types/newArticleRequest.interface';

@Injectable({ providedIn: 'root' })
export class ArticleService {
  private http = inject(HttpClient);

  getArticle(slug: string): Observable<IArticle> {
    const url = `/articles/${slug}`;
    return this.http.get<IArticleResponse>(url)
    .pipe((map(({ article }) => article)));
  }

  newArticle(newArticle: INewArticleRequest): Observable<IArticle> {
    const url = `/articles`;
    return this.http.post<INewArticleResponse>(url, newArticle)
    .pipe((map(({ article }) => article)));
  }
}
