import { Route } from "@angular/router";
import { unsavedChangesGuard } from "./core/guards/unsaved-changes.guard";
import { authGuard } from "./core/guards/auth.guard";
import { loggedInUserGuard } from "./core/guards/logged-in-user.guard";

export const appRoutes: Route[] = [
  {
    path: 'registration',
    loadChildren: () =>
      import('src/app/auth/auth.routes')
        .then(({ registrationRoutes }) => registrationRoutes),
    canActivate: [loggedInUserGuard],
  },
  {
    path: 'login',
    loadChildren: () =>
      import('src/app/auth/auth.routes')
        .then(({ loginRoutes }) => loginRoutes),
    canActivate: [loggedInUserGuard],
  },
  {
    path: 'article/:slug',
    loadChildren: () =>
      import('src/app/article/article.routes')
        .then(({ routes }) => routes),
  },
  {
    path: 'article/:slug/edit',
    loadComponent: () =>
      import('src/app/shared/components/article-form/article-form.component')
        .then(({ ArticleFormComponent }) => ArticleFormComponent),
  },
  {
    path: 'new-article',
    loadComponent: () =>
      import('src/app/shared/components/article-form/article-form.component')
        .then(({ ArticleFormComponent }) => ArticleFormComponent),
    canDeactivate: [unsavedChangesGuard],
  },
  {
    path: 'profile',
    loadChildren: () =>
      import('src/app/profile/profile.routes')
        .then(({ routes }) => routes),
    canActivate: [authGuard],
  },
  {
    path: 'settings',
    loadChildren: () =>
      import('src/app/settings/settings.routes')
        .then(({ routes }) => routes),
    canActivate: [authGuard],
  },
  {
    path: '',
    loadChildren: () =>
      import('src/app/home/home.routes')
        .then(({ routes }) => routes),
  },
  {
    path: '**',
    loadComponent: () =>
      import('src/app/page-not-found/page-not-found.component')
        .then(({ PageNotFoundComponent }) => PageNotFoundComponent),
  }
]
