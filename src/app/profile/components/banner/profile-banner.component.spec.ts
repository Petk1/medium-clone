import { By } from "@angular/platform-browser";
import { Router } from "@angular/router"
import { TestBed } from "@angular/core/testing"
import { provideMockStore } from "@ngrx/store/testing";
import { ProfileBannerComponent } from "./profile-banner.component";
import { LogoutButtonComponent } from "../logout-button/logout-button.component";
import { FollowButtonComponent } from "src/app/shared/components/follow-button/follow-button.component";

describe('ProfileBannerComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  it('should display the profile username and avatar', () => {
    const { componentInstance, debugEl } = setup(false, 'Test username', 'Test username', '');
    const usernameEl = debugEl.query(By.css('[data-testingId="profile-username"]')).nativeElement as HTMLElement;
    const avatarEl = debugEl.query(By.css('[data-testingId="profile-avatar"]')).nativeElement as HTMLElement;
    expect(usernameEl.textContent).toContain(componentInstance.profileData.username);
    expect(avatarEl.getAttribute('src')).toBe('');
  })

  it('should determine if a profile is being following', () => {
    const { componentInstance, componentRef } = setup(true);
    expect(componentInstance.isFollowing()).toBe(true);

    const checkResult = (isFollowing: boolean) => {
      componentRef.setInput('profileData', { username: 'Test username', following: isFollowing });
      expect(componentInstance.isFollowing()).toBe(isFollowing);
    }

    checkResult(false);
    checkResult(true);
    checkResult(false);
    checkResult(true);
  })

  it('should navigate to the settings on edit profile', () => {
    const { componentInstance, mockRouter } = setup();
    componentInstance.onEditProfile();
    expect(mockRouter.navigateByUrl).toHaveBeenCalledOnceWith('/settings');
  })

  describe("Display buttons", () => {
    it('should display an edit button if it is a profile of the currently logged in user', () => {
      const { debugEl, componentRef, fixture } = setup();
      const getEditButtonDebugEl = () => debugEl.query(By.css('[data-testingId="profile-edit-button"]'));

      expect(getEditButtonDebugEl()).toBeTruthy();

      componentRef.setInput('currentUser', { username: 'Another username' });
      fixture.detectChanges();

      expect(getEditButtonDebugEl()).toBeFalsy();
    })

    it('should display a follow button if it is not a profile of the currently logged in user', () => {
      const { debugEl, componentRef, fixture } = setup();
      const getFollowButtonDebugEl = () => debugEl.query(By.directive(FollowButtonComponent));

      expect(getFollowButtonDebugEl()).toBeFalsy();

      componentRef.setInput('currentUser', { username: 'Another username' });
      fixture.detectChanges();

      expect(getFollowButtonDebugEl()).toBeTruthy();
    })

    it('should display a logout button if it is a profile of the currently logged in user', () => {
      const { debugEl, componentRef, fixture } = setup();
      const getLogoutButtonDebugEl = () => debugEl.query(By.directive(LogoutButtonComponent));

      expect(getLogoutButtonDebugEl()).toBeTruthy();

      componentRef.setInput('currentUser', { username: 'Another username' });
      fixture.detectChanges();

      expect(getLogoutButtonDebugEl()).toBeFalsy();
    })
  })
})

function setup(
  isFollowing = false,
  profileUsername = 'Test username',
  currentUserUsername = 'Test username',
  image = ''
) {
  const mockRouter = {
    navigateByUrl: jasmine.createSpy('navigateByUrl'),
  };

  TestBed.configureTestingModule({
    providers: [
      {
        provide: Router,
        useValue: mockRouter
      },
      provideMockStore()
    ]
  })

  const fixture = TestBed.createComponent(ProfileBannerComponent);
  const componentRef = fixture.componentRef;
  const componentInstance = fixture.componentInstance;
  const debugEl = fixture.debugElement;
  componentRef.setInput('profileData', { username: profileUsername, following: isFollowing, image });
  componentRef.setInput('currentUser', { username: currentUserUsername });

  fixture.detectChanges();

  return {
    fixture,
    componentRef,
    componentInstance,
    debugEl,
    mockRouter,
  }
}
