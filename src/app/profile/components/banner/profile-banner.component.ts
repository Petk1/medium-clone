import { ChangeDetectionStrategy, Component, Input, inject } from '@angular/core';
import { NgIf } from '@angular/common';
import { FollowButtonComponent } from 'src/app/shared/components/follow-button/follow-button.component';
import { ICurrentUser } from 'src/app/shared/types/currentUser.interface';
import { IProfile } from 'src/app/shared/types/profile.interface';
import { Router } from '@angular/router';
import { LogoutButtonComponent } from '../logout-button/logout-button.component';

@Component({
  selector: 'mc-profile-banner',
  templateUrl: './profile-banner.component.html',
  styleUrls: ['./profile-banner.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgIf,
    FollowButtonComponent,
    LogoutButtonComponent,
  ]
})
export class ProfileBannerComponent {
  @Input({ required: true }) profileData!: IProfile;
  @Input({ required: true }) currentUser!: ICurrentUser;

  private readonly router = inject(Router);

  isFollowing(): boolean {
    return 'following' in this.profileData && this.profileData.following;
  }

  onEditProfile(): void {
    this.router.navigateByUrl('/settings');
  }
}
