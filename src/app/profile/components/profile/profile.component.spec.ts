import { By } from "@angular/platform-browser";
import { ActivatedRoute, NavigationStart, Params, Router, convertToParamMap } from "@angular/router";
import { TestBed } from "@angular/core/testing";
import { of } from "rxjs";
import { provideMockStore } from "@ngrx/store/testing";
import { selectCurrentUser } from "src/app/auth/store/reducers";
import { selectIsLoading, selectProfileData } from "../../store/reducers";
import { ProfileComponent } from "./profile.component";
import { LoadingComponent } from "src/app/shared/components/loading/loading.component";
import { ICurrentUser } from "src/app/shared/types/currentUser.interface";
import { IProfile } from "src/app/shared/types/profile.interface";

describe('ProfileComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  it('should display a loading component', () => {
    const { debugEl } = setup('my-posts', `/profile${MOCK_PROFILE_DATA.username}/my-posts`, true);
    const loadingDebugEl = debugEl.query(By.directive(LoadingComponent));
    expect(loadingDebugEl).toBeTruthy();
  })

  describe('Setting the initial toggler option', () => {
    it('should toggle to the Favorited Posts option', () => {
      const { componentInstance } = setup('favorited-posts');
      expect(componentInstance.initSelectedOptionSig().apiUrl).toBe(componentInstance.togglerOptionsSig()[1].apiUrl);
    })

    it('should toggle to the My Posts option', () => {
      const { componentInstance } = setup('my-posts');
      expect(componentInstance.initSelectedOptionSig().apiUrl).toBe(componentInstance.togglerOptionsSig()[0].apiUrl);
    })

    it('should toggle to the My Posts option if a feed type is not provided', () => {
      const { componentInstance } = setup('', `/profile${MOCK_PROFILE_DATA.username}`);
      expect(componentInstance.initSelectedOptionSig().apiUrl).toBe(componentInstance.togglerOptionsSig()[0].apiUrl);
    })
  })
})

function setup(
  feedTypeInUrl: 'my-posts' | 'favorited-posts' | '' = 'my-posts',
  url = `/profile${MOCK_PROFILE_DATA.username}/my-posts`,
  isLoading = false
) {
  const mockRouter = {
    url: '/',
    events: of(new NavigationStart(1, url)),
    navigate: jasmine.createSpy('navigate'),
    navigateByUrl: jasmine.createSpy('navigateByUrl'),
    createUrlTree: () => {},
    serializeUrl: () => {},
  };

  const mockActivatedRoute = {
    snapshot: {
      paramMap: {
        get: jasmine.createSpy('feedType').and.returnValue(feedTypeInUrl),
      },
      queryParamMap: {
        get: jasmine.createSpy('get').and.returnValue('1')
      },
    },
    queryParams: {} as Params,
    queryParamMap: of(convertToParamMap({ page: 1 })),
  }

  TestBed.configureTestingModule({
    providers: [
      {
        provide: Router,
        useValue: mockRouter
      },
      {
        provide: ActivatedRoute,
        useValue: mockActivatedRoute
      },
      provideMockStore({
        selectors: [
          { selector: selectCurrentUser, value: MOCK_USER_DATA },
          { selector: selectProfileData, value: MOCK_PROFILE_DATA },
          { selector: selectIsLoading, value: isLoading }
        ]
      })
    ]
  })

  const fixture = TestBed.createComponent(ProfileComponent);
  const componentInstance = fixture.componentInstance;
  const debugEl = fixture.debugElement;

  fixture.detectChanges();

  return {
    fixture,
    componentInstance,
    mockRouter,
    debugEl,
  }
}

const MOCK_USER_DATA: ICurrentUser = {
  email: 'testemail@gmail.com',
  token: 'eyMockToken123',
  username: 'Test username',
  bio: 'Short test bio',
  image: ''
}

const MOCK_PROFILE_DATA: IProfile = {
  username: 'Test username',
  bio: 'Short test bio',
  image: '',
  following: false
}
