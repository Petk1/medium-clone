import {
  ChangeDetectionStrategy,
  Component,
  Input as RouterInput,
  OnChanges,
  OnInit,
  SimpleChanges,
  computed,
  inject,
  signal,
  OnDestroy,
} from "@angular/core";
import { NgIf } from "@angular/common";
import { Store } from "@ngrx/store";
import { selectCurrentUser } from "src/app/auth/store/reducers";
import { selectIsLoading, selectProfileData } from "../../store/reducers";
import { profileActions } from "../../store/actions";
import { ProfileBannerComponent } from "../banner/profile-banner.component";
import { FeedTogglerComponent } from "src/app/shared/components/feed-toggler/feed-toggler.component";
import { IFeedTogglerOption } from "src/app/shared/components/feed-toggler/types/feedTogglerOption.interface";
import { ActivatedRoute, NavigationStart, Router } from "@angular/router";
import { isSimpleChangeChanged } from "src/app/shared/utils/is-simple-change-changed";
import { Subject, takeUntil, tap } from "rxjs";
import { LoadingComponent } from "src/app/shared/components/loading/loading.component";

@Component({
  selector: 'mc-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgIf,
    ProfileBannerComponent,
    FeedTogglerComponent,
    LoadingComponent,
  ]
})
export class ProfileComponent implements OnInit, OnChanges, OnDestroy {
  @RouterInput() feedType!: string;
  @RouterInput() set username(value: string){
    this.usernameSig.set(value);
    this.getProfile(value);
  }
  private readonly usernameSig = signal<string | undefined>(undefined);

  private readonly store = inject(Store);
  private readonly router = inject(Router);
  private readonly activatedRoute = inject(ActivatedRoute);
  private queryParams = this.activatedRoute.snapshot.queryParams;
  private readonly updateViewCounterSig = signal<number>(0);
  private readonly destroy$ = new Subject<void>();
  private feedTypeFromUrl = this.activatedRoute.snapshot.paramMap.get('feedType');
  readonly profileSig = this.store.selectSignal(selectProfileData);
  readonly isProfileLoadingSig = this.store.selectSignal(selectIsLoading);
  readonly currentUserSig = this.store.selectSignal(selectCurrentUser);

  togglerOptionsSig = computed(() => {
    const username = this.usernameSig();
    return [
      {
        name: 'My Posts',
        apiUrl: `/articles?author=${username}`,
        callback: () => this.getOptionNavigateByUrl(`profile/${username}/my-posts`),
      },
      {
        name: 'Favorited Posts',
        apiUrl: `/articles?favorited=${username}`,
        callback: () => this.getOptionNavigateByUrl(`profile/${username}/favorited-posts`),
      }
    ] as IFeedTogglerOption[]
  });

  initSelectedOptionSig = computed<IFeedTogglerOption>(() => {
    this.updateViewCounterSig();
    const togglerOptions = this.togglerOptionsSig();
    return this.getInitSelectedOption(togglerOptions);
  })

  getInitSelectedOption(togglerOptions: IFeedTogglerOption[]): IFeedTogglerOption {
    const username = this.usernameSig();

    if(this.feedTypeFromUrl === 'my-posts'){
      return {
        ...togglerOptions[0],
        callback: () => this.getOptionNavigate(`profile/${username}/my-posts`),
      }
    }
    if(this.feedTypeFromUrl === 'favorited-posts'){
      return {
        ...togglerOptions[1],
        callback: () => this.getOptionNavigate(`profile/${username}/favorited-posts`),
      }
    }

    return {
      ...togglerOptions[0],
      callback: () => this.getOptionNavigate(`profile/${username}/my-posts`),
    }
  }

  ngOnInit(): void {
    this.getProfile();

    // To handle the case when the user uses the menu to navigate to the 'Your profile' page being on the 'Your profile" page.
    // Without this snippet of code the data reload and showing articles were not triggered.
    // The updateViewCounterSig signal triggers a recompute of the initSelectedOptionSig signal and it
    // triggers the onToggle method in the feed-toggle.component.ts.
    // In result it kind of works like the user used the toggler to change the type of articles displayed.
    this.router.events
    .pipe(
      tap(routerEvent => {
        if(routerEvent instanceof NavigationStart){
          const url = routerEvent.url;

          if(url === '/') { // navigation to homepage happens
            return;
          }

          const urlArray = url.split('/');

          // handling the case when a feed type is not provided
          if(this.currentUserSig() && urlArray.length < 4 && urlArray.includes('profile')){
            this.feedTypeFromUrl = 'my-posts';
            this.queryParams = {};
            let updateViewCounterValue = this.updateViewCounterSig();
            this.updateViewCounterSig.set(++updateViewCounterValue);
          }
        }
      }),
      takeUntil(this.destroy$)
    )
    .subscribe()
  }

  ngOnChanges(changes: SimpleChanges): void {
    const username = changes['username'];
    const isUsernameChanged = isSimpleChangeChanged(username);

    if(isUsernameChanged) {
      this.getProfile();
    }
  }

  private getProfile(username: string | undefined = undefined): void {
    const bodyUsername = username || this.username;
    if(bodyUsername){
      this.store.dispatch(profileActions.getProfile({ username: username || this.username }));
    }
  }

  private getOptionNavigate(path: string) {
    return this.router.navigate([path], { queryParams: this.queryParams })
  }

  private getOptionNavigateByUrl(path: string) {
    return this.router.navigateByUrl(path);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
