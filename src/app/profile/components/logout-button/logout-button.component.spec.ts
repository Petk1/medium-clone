import { TestBed } from "@angular/core/testing";
import { MockStore, provideMockStore } from "@ngrx/store/testing";
import { authActions } from "src/app/auth/store/actions";
import { LogoutButtonComponent } from "./logout-button.component";

describe('LogoutButtonComponent', () => {
  it('should create', () => {
    const { componentInstance } = setup();
    expect(componentInstance).toBeTruthy();
  })

  it('should dispatch a logout action', () => {
    const { store, componentInstance } = setup();
    const mockStoreDispatchSpy = spyOn(store, 'dispatch').and.callThrough();
    componentInstance.onLogout();
    expect(mockStoreDispatchSpy).toHaveBeenCalledOnceWith(authActions.logout());
  })
})

function setup() {
  TestBed.configureTestingModule({
    providers: [
      provideMockStore()
    ]
  })

  const fixture = TestBed.createComponent(LogoutButtonComponent);
  const componentInstance = fixture.componentInstance;
  const store = TestBed.inject(MockStore);

  fixture.detectChanges();

  return {
    componentInstance,
    store,
  }
}
