import { Component, ChangeDetectionStrategy, inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { authActions } from 'src/app/auth/store/actions';

@Component({
  selector: 'mc-logout-button',
  template: `
    <button class="logout-btn primary-btn primary-btn--active"
      (click)="onLogout()"
    >
      Logout
    </button>
  `,
  styles: [`
    .logout-btn {
      width: fit-content;
      height: 30px;
      padding: 0 20px;
    }
  `],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LogoutButtonComponent {
  private readonly store = inject(Store);

  onLogout(): void {
    this.store.dispatch(authActions.logout());
  }
}
