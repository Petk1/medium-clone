import { Route } from "@angular/router";
import { provideState } from "@ngrx/store";
import { provideEffects } from "@ngrx/effects";
import { profileFeatureKey, profileReducer } from "./store/reducers";
import * as profileEffects from './store/effects';
import { ProfileService } from "./services/profile.service";
import { ProfileComponent } from "./components/profile/profile.component";

export const routes: Route[] = [
  {
    path: '',
    children: [
      {
        path: ':username',
        redirectTo: ':username/my-posts',
        pathMatch: 'full',
      },
      {
        path: ':username/:feedType',
        component: ProfileComponent,
      }
    ],
    providers: [
      provideState(profileFeatureKey, profileReducer),
      provideEffects(profileEffects),
      ProfileService,
    ]
  }
]
