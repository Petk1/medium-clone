import { IProfile } from "src/app/shared/types/profile.interface";

export interface IGetProfileResponse {
  profile: IProfile;
}
