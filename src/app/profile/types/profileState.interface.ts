import { IProfile } from "src/app/shared/types/profile.interface";

export interface IProfileState {
  isLoading: boolean;
  error: string | null;
  profileData: IProfile | undefined | null;
}
