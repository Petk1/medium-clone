import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable, map } from 'rxjs';
import { IProfile } from 'src/app/shared/types/profile.interface';
import { IGetProfileResponse } from '../types/getProfileResponse.interface';

@Injectable()
export class ProfileService {
  private http = inject(HttpClient);

  getProfile(username: string): Observable<IProfile> {
    const url = `profiles/${username}`;
    return this.http.get<IGetProfileResponse>(url, {})
    .pipe(map(({ profile }) => profile));
  }

}
