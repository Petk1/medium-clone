import { inject } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { switchMap, map, catchError, of } from 'rxjs';
import { profileActions } from "./actions";
import { ProfileService } from "../services/profile.service";

export const getProfileEffect = createEffect((
  actions$ = inject(Actions),
  profileSrv = inject(ProfileService),
) => (
  actions$.pipe(
    ofType(profileActions.getProfile),
    switchMap(({ username }) => (
      profileSrv.getProfile(username)
      .pipe(
        map(profile => profileActions.getProfileSuccess({ profile })),
        catchError(() => of(profileActions.getProfileFailure()))
      )
    ))
  )
), {
  functional: true,
})
