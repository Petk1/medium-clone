import { createActionGroup, emptyProps, props } from "@ngrx/store";
import { IProfile } from "src/app/shared/types/profile.interface";

export const profileActions = createActionGroup({
  source: 'Profile',
  events: {
    'Get profile': props<{ username: string }>(),
    'Get profile success': props<{ profile: IProfile }>(),
    'Get profile failure': emptyProps(),
  }
})
