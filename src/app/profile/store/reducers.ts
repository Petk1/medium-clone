import { createFeature, createReducer, on } from "@ngrx/store";
import { routerNavigatedAction } from "@ngrx/router-store";
import { IProfileState } from "../types/profileState.interface";
import { profileActions } from "./actions";

const initialState: IProfileState = {
  isLoading: false,
  error: null,
  profileData: null,
}

const profileFeature = createFeature({
  name: 'profile',
  reducer: createReducer(
    initialState,
    on(profileActions.getProfile, state => ({
      ...state,
      isLoading: true,
    })),
    on(profileActions.getProfileSuccess, (state, { profile: profileData }) => ({
      ...state,
      isLoading: false,
      profileData,
    })),
    on(profileActions.getProfileFailure, state => ({
      ...state,
      isLoading: false,
    })),
    on(routerNavigatedAction, (state, { payload }) => {
      if(payload.event.url.includes('/profile/')) {
        return state;
      }

      return initialState;
    })
  )
})

export const {
  name: profileFeatureKey,
  reducer: profileReducer,
  selectIsLoading,
  selectError,
  selectProfileData,
} = profileFeature;
