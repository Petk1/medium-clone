import { inject } from '@angular/core';
import { CanDeactivateFn } from '@angular/router';
import { map } from 'rxjs/operators';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { TCanDeactivate } from './types/canDeactivate.type';
import { ICanComponentDeactivate } from './types/canComponentDeactivate.interface';
import { IInitConfirmDialogData } from 'src/app/shared/types/initConfirmDialogData.interface';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';


export type T = ICanComponentDeactivate;

export const unsavedChangesGuard: CanDeactivateFn<T> = (component: T): TCanDeactivate => {
  const result = component.canDeactivate();

  if(!result){
    const dialogConfig = new MatDialogConfig<IInitConfirmDialogData>();
    dialogConfig.width = '440px';
    dialogConfig.height = '275px';
    dialogConfig.closeOnNavigation = true;
    dialogConfig.position = {
      top: '125px',
    }
    dialogConfig.data = {
      title: 'Are you sure?',
      content: 'Some data may be lost. Do you want to leave?'
    }

    return inject(MatDialog)
      .open(ConfirmDialogComponent, dialogConfig)
      .afterClosed()
      .pipe(
        map(decision => decision)
      )
  } else {
    return result;
  }

};
