import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { provideMockStore } from '@ngrx/store/testing';
import { selectCurrentUser } from 'src/app/auth/store/reducers';
import { loggedInUserGuard } from './logged-in-user.guard';
import { ICurrentUser } from 'src/app/shared/types/currentUser.interface';

describe('loggedInUserGuard', () => {
  it('should allow activation if user is NOT logged in', (done) => {
    const { _loggedInUserGuard, mockRouter } = setup(null);

    _loggedInUserGuard.subscribe(canActivate => {
      expect(canActivate).toBe(true);
      expect(mockRouter.createUrlTree).not.toHaveBeenCalled();
      done();
    });
  });

  it('should redirect to "/global-feed" page if user is logged in', (done) => {
    const { _loggedInUserGuard, mockRouter } = setup();

    _loggedInUserGuard.subscribe(() => {
      expect(mockRouter.createUrlTree).toHaveBeenCalledWith(['/global-feed']);
      done();
    });
  });
})

function setup(mockUser: ICurrentUser | null = {} as ICurrentUser) {
  const mockRouter = jasmine.createSpyObj('Router', ['createUrlTree']);

  TestBed.configureTestingModule({
    providers: [
      provideMockStore({
        selectors: [
          { selector: selectCurrentUser, value: mockUser }
        ]
      }),
      { provide: Router, useValue: mockRouter },
    ]
  })

  const route: ActivatedRouteSnapshot = {} as ActivatedRouteSnapshot;
  const state: RouterStateSnapshot = {} as RouterStateSnapshot;

  const _loggedInUserGuard = TestBed.runInInjectionContext(
    (() => loggedInUserGuard(route, state)) as () => Observable<true | UrlTree>);

  return {
    _loggedInUserGuard,
    mockRouter,
  }
}
