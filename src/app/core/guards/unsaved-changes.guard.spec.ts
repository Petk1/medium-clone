import { ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { TestBed } from "@angular/core/testing";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { Subject } from "rxjs";
import { unsavedChangesGuard } from "./unsaved-changes.guard";
import { ICanComponentDeactivate } from "./types/canComponentDeactivate.interface";

describe('unsavedChangeGuard', () => {
  it('should open confirmation dialog if canDeactivate returns false', () => {
    const { mockMatDialog } = setup(false);
    expect(mockMatDialog.open).toHaveBeenCalled();
  })

  it('should allow deactivation if canDeactivate returns true', () => {
    const { _unsavedChangesGuard, mockMatDialog } = setup(true);
    expect(mockMatDialog.open).not.toHaveBeenCalled();
    expect(_unsavedChangesGuard).toBe(true);
  });
})

function setup(canDeactivateResult: boolean) {
  const afterClosed$ = new Subject<void>()

  const mockMatDialogRef = {
    afterClosed: () => afterClosed$,
    close: jasmine.createSpy('close').and.callFake(() => {
      afterClosed$.next();
    })
  }
  const mockMatDialog = {
    open: jasmine.createSpy('open').and.returnValue(mockMatDialogRef)
  }

  TestBed.configureTestingModule({
    providers: [
      {
        provide: MatDialog,
        useValue: mockMatDialog,
      },
    ]
  })

  const component: ICanComponentDeactivate = {
    canDeactivate: () => canDeactivateResult
  }

  const _unsavedChangesGuard = TestBed.runInInjectionContext(
    (() => unsavedChangesGuard(component, {} as ActivatedRouteSnapshot, {} as RouterStateSnapshot, {} as RouterStateSnapshot)));

  return {
    _unsavedChangesGuard,
    mockMatDialog,
  }
}
