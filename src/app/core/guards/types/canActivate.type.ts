import { UrlTree } from "@angular/router";
import { Observable } from "rxjs";

export type TCanActivate =
  | Observable<boolean | UrlTree>
  | Promise<boolean | UrlTree>
  | boolean
  | UrlTree
