import { TCanDeactivate } from "./canDeactivate.type";

export interface ICanComponentDeactivate {
  canDeactivate: () => TCanDeactivate;
}
