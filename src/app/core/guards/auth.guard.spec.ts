import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { provideMockStore } from '@ngrx/store/testing';
import { selectCurrentUser } from 'src/app/auth/store/reducers';
import { authGuard } from './auth.guard';
import { ICurrentUser } from 'src/app/shared/types/currentUser.interface';

describe('authGuard', () => {
  it('should allow activation if user is logged in', (done) => {
    const { _authGuard, mockRouter } = setup();

    _authGuard.subscribe(canActivate => {
      expect(canActivate).toBe(true);
      expect(mockRouter.createUrlTree).not.toHaveBeenCalled();
      done();
    });
  });

  it('should redirect to login page if user is NOT logged in', (done) => {
    const { _authGuard, mockRouter } = setup(null);

    _authGuard.subscribe(() => {
      expect(mockRouter.createUrlTree).toHaveBeenCalledWith(['/login']);
      done();
    });
  });
})

function setup(mockUser: ICurrentUser | null = {} as ICurrentUser) {
  const mockRouter = jasmine.createSpyObj('Router', ['createUrlTree']);

  TestBed.configureTestingModule({
    providers: [
      provideMockStore({
        selectors: [
          { selector: selectCurrentUser, value: mockUser }
        ]
      }),
      { provide: Router, useValue: mockRouter },
    ]
  })

  const route: ActivatedRouteSnapshot = {} as ActivatedRouteSnapshot;
  const state: RouterStateSnapshot = {} as RouterStateSnapshot;

  const _authGuard = TestBed.runInInjectionContext(
    (() => authGuard(route, state)) as () => Observable<true | UrlTree>);

  return {
    _authGuard,
    mockRouter,
  }
}
