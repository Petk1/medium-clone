import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateFn, Router, RouterStateSnapshot } from "@angular/router";
import { Store } from '@ngrx/store';
import { selectCurrentUser } from 'src/app/auth/store/reducers';
import { TCanActivate } from './types/canActivate.type';
import { filter, map } from 'rxjs/operators';
import { ICurrentUser } from 'src/app/shared/types/currentUser.interface';

export const authGuard: CanActivateFn = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot
): TCanActivate => {
  const store = inject(Store);
  const router = inject(Router);

  return store.select(selectCurrentUser)
  .pipe(
    filter((user): user is ICurrentUser | null => user !== undefined),
    map(user => user ? true : router.createUrlTree(['/login']))
  )
}
