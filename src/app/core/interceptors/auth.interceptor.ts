import { inject } from '@angular/core';
import { HttpInterceptorFn } from "@angular/common/http";
import { UserTokenService } from 'src/app/shared/services/user-token.service';

export const authInterceptor: HttpInterceptorFn = (request, next) => {
  const token = inject(UserTokenService).get('accessToken');

  request = request.clone({
    setHeaders: {
      Authorization: token ? `Token ${token}` : '',
    },
  });

  return next(request);
}
