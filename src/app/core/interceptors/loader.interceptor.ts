import { inject } from "@angular/core";
import { HttpInterceptorFn, HttpResponse } from "@angular/common/http";
import { delayWhen, finalize, interval, of } from 'rxjs';
import { LoaderService } from "src/app/shared/components/loader/services/loader.service";

export const loaderInterceptor: HttpInterceptorFn = (request, next) => {
  const loaderSrv = inject(LoaderService);

  loaderSrv.openLoader();

  return next(request)
  .pipe(
    finalize(() => loaderSrv.closeLoader()),
    delayWhen(event => {
      // To fix a style bug that occurred when deleting a comment.
      // When the loader was still open the footer slightly jumps up for a moment
      // and after closing the loader it goes back to the bottom.
      if(event instanceof HttpResponse) {
        return interval(150);
      }

      return of(event)
    })
  )
}
