import { HttpHandlerFn, HttpRequest, HttpResponse } from "@angular/common/http"
import { TestBed } from "@angular/core/testing";
import { of } from "rxjs";
import { authInterceptor } from "./auth.interceptor";
import { UserTokenService } from "src/app/shared/services/user-token.service";

describe('authUrlInterceptor', () => {
  it('should set the appropriate token getting from the UserTokenService', (done) => {
    const testToken = 'test-token-123';
    setup(testToken);
    const mockHttpHandlerFn: HttpHandlerFn = (req: HttpRequest<unknown>) => of(new HttpResponse(req));
    const mockRequest = new HttpRequest('GET', '/test-endpoint');

    TestBed.runInInjectionContext(() => {
      authInterceptor(mockRequest, mockHttpHandlerFn).subscribe(response => {
        expect((response as HttpResponse<unknown>).headers.get('Authorization'))
          .toBe(`Token ${testToken}`);

        done();
      })
    })
  })
})

function setup(token: string) {
  const mockUserTokenSrv = {
    get: jasmine.createSpy('get').and.returnValue(token)
  }

  TestBed.configureTestingModule({
    providers: [
      {
        provide: UserTokenService,
        useValue: mockUserTokenSrv
      },
    ]
  })
}
