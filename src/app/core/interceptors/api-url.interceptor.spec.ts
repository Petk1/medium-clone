import { HttpHandlerFn, HttpRequest, HttpResponse } from "@angular/common/http"
import { TestBed } from "@angular/core/testing";
import { of } from "rxjs";
import { apiUrlInterceptor } from "./api-url.interceptor";
import { environment } from "src/environments/environment";

describe('apiUrlInterceptor', () => {
  it('should set properly a url request containing apiUrl from the environment file', (done) => {
    const _apiUrlInterceptorGuard = TestBed.runInInjectionContext(() => apiUrlInterceptor);
    const mockHttpHandlerFn: HttpHandlerFn = (req: HttpRequest<unknown>) => of(new HttpResponse(req));
    const mockRequest = new HttpRequest('GET', '/test-endpoint');

    _apiUrlInterceptorGuard(mockRequest, mockHttpHandlerFn).subscribe(response => {
      expect((response as HttpResponse<unknown>).url).toBe(`${environment.apiUrl}/${mockRequest.url}`);
      done();
    })
  })
})
