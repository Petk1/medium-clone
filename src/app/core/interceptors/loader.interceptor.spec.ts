import { HttpHandlerFn, HttpRequest, HttpResponse } from "@angular/common/http"
import { TestBed } from "@angular/core/testing"
import { of } from "rxjs"
import { loaderInterceptor } from "./loader.interceptor"
import { LoaderService } from "src/app/shared/components/loader/services/loader.service"

describe('loaderInterceptor', () => {
  it('should open the loader and close it when finalized', (done) => {
    const { mockLoaderSrv } = setup();
    const mockHttpHandlerFn: HttpHandlerFn = (req: HttpRequest<unknown>) => of(new HttpResponse(req));
    const mockRequest = new HttpRequest('GET', '/test-endpoint');

    TestBed.runInInjectionContext(() => {
      const loaderInterceptor$ = loaderInterceptor(mockRequest, mockHttpHandlerFn);
      expect(mockLoaderSrv.openLoader).toHaveBeenCalledTimes(1);

      loaderInterceptor$.subscribe(() => {
        expect(mockLoaderSrv.closeLoader).toHaveBeenCalledTimes(1);
        done();
      })
    })
  })
})

function setup() {
  const mockLoaderSrv = {
    openLoader: jasmine.createSpy('openLoader'),
    closeLoader: jasmine.createSpy('closeLoader')
  }

  TestBed.configureTestingModule({
    providers: [
      {
        provide: LoaderService,
        useValue: mockLoaderSrv
      },
    ]
  })

  return {
    mockLoaderSrv,
  }
}
