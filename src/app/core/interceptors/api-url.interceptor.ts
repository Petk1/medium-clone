import { HttpInterceptorFn } from "@angular/common/http";
import { environment } from "src/environments/environment";

export const apiUrlInterceptor: HttpInterceptorFn = (request, next) => {
  request = request.clone({
    url: `${environment.apiUrl}/${request.url}`
  });

  return next(request);
}
