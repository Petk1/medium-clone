export interface IRouterLink {
  name: string;
  link: string;
}

export interface IRouterConfig {
  userLoggedIn: IRouterLink[];
  userNotLoggedIn: IRouterLink[];
}
