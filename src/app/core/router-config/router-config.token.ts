import { InjectionToken } from "@angular/core";
import { IRouterConfig, IRouterLink } from "./types/routerConfig.interface";

const userLoggedIn: IRouterLink[] = [
  {
    name: 'Home',
    link: '/'
  },
  {
    name: 'New article',
    link: '/new-article'
  },
  {
    name: 'Your profile',
    link: '/profile/:username'
  },
  {
    name: 'Settings',
    link: '/settings'
  },
]

const userNotLoggedIn: IRouterLink[] = [
  {
    name: 'Home',
    link: '/'
  },
  {
    name: 'Sign In',
    link: '/login'
  },
  {
    name: 'Sign Up',
    link: '/registration'
  },
]

export const ROUTER_CONFIG = new InjectionToken<IRouterConfig>('router-config', {
  providedIn: 'root',
  factory: () => ({ userLoggedIn, userNotLoggedIn }),
})
