import { TestBed } from "@angular/core/testing";
import { GlobalErrorHandler } from "./global-error-handler"
import { NotificationService } from "src/app/shared/components/notification/services/notification.service";

describe('GlobalErrorHandler', () => {
  it('should display a notification with an error message', () => {
    const { globalErrorHandler, mockNotificationSrv } = setup();

    expect(globalErrorHandler).toBeTruthy()

    globalErrorHandler.handleError({ email: ['has already been taken'] });
    expect(mockNotificationSrv.newNotification).toHaveBeenCalled();

    globalErrorHandler.handleError(new Error('something went wrong'));
    expect(mockNotificationSrv.newNotification).toHaveBeenCalledWith({
      data: {
        title: 'Error occured',
        content: 'Something went wrong.'
      }
    })

    globalErrorHandler.handleError({ exampleProperty: 'mock error message' });
    expect(mockNotificationSrv.newNotification).toHaveBeenCalledWith({
      data: {
        title: 'Error occured',
        content: 'Something went wrong.'
      }
    })
  })
})

function setup() {
  const mockNotificationSrv = jasmine.createSpyObj('NotificationService', ['newNotification']);

  TestBed.configureTestingModule({
    providers: [
      GlobalErrorHandler,
      { provide: NotificationService, useValue: mockNotificationSrv },
    ],
  });

  const globalErrorHandler = TestBed.inject(GlobalErrorHandler);

  return {
    globalErrorHandler,
    mockNotificationSrv,
  }
}
