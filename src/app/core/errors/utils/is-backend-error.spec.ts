import { isBackendError } from "./is-backend-error"

describe('isBackendError Function', () => {
  it('should return that it is the IBackendErrors', () => {
    expect(isBackendError({
      exampleProperty: ['mock error message']
    })).toBe(true);
  })

  it('should return that it is NOT the IBackendErrors', () => {
    expect(isBackendError({
      exampleProperty: 'mock error message'
    })).toBe(false);
  })
})
