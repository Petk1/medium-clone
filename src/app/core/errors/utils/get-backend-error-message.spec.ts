import { getBackendErrorMessage } from "./get-backend-error-message"

describe('getBackendErrorMessage Function', () => {
  it('should return an appropriate error message', () => {
    expect(getBackendErrorMessage({
      email: ['has already been taken']
    })).toEqual({
      title: 'Wrong data',
      content: 'This email is already taken. Enter another and try again. '
    });

    expect(getBackendErrorMessage({
      email: ['has already been taken'],
      username: ['has already been taken'],
    })).toEqual({
      title: 'Wrong data',
      content: 'This email is already taken. Enter another and try again. This Username is already taken. Enter another and try again. '
    });
  })

  it('should return a default error message if none matches', () => {
    expect(getBackendErrorMessage({
      mockPropertyName: ['mock error message'],
    })).toEqual({
      title: 'Error occured',
      content: 'Something went wrong.'
    });

    expect(getBackendErrorMessage({
      email: ['mock error message'],
    })).toEqual({
      title: 'Wrong data',
      content: 'Something went wrong.'
    });
  })
})
