import { IBackendErrors } from "src/app/shared/types/backendErrors.interface"

export const getBackendErrorMessage = (errors: IBackendErrors) => {
  const errorObj = {
    title: '',
    content: ''
  }

  for(const [errorName, errorContent] of Object.entries(errors)) {
    if(errorName === 'email or password') {
      errorObj.title = 'Wrong data';

      if(errorContent[0] === 'is invalid') {
        errorObj.content += 'Wrong email or password. Correct and try again. ';
      }
    }

    if(errorName === 'email') {
      errorObj.title = 'Wrong data';

      if(errorContent[0] === 'has already been taken'){
        errorObj.content += 'This email is already taken. Enter another and try again. ';
      }
    }

    if(errorName === 'username') {
      errorObj.title = 'Wrong data';

      if(errorContent[0] === 'has already been taken'){
        errorObj.content += 'This Username is already taken. Enter another and try again. ';
      }
    }

    if(errorName === 'article') {
      errorObj.title = 'Not found';

      if(errorContent[0] === 'not found'){
        errorObj.content += 'Article not found. ';
      }
    }
  }

  if(!errorObj.title) errorObj.title = 'Error occured';
  if(!errorObj.content) errorObj.content = 'Something went wrong.';

  return errorObj;
}
