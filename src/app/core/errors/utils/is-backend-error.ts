import { IBackendErrors } from "src/app/shared/types/backendErrors.interface"

export const isBackendError = (error: unknown): error is IBackendErrors => {
  if(!error || typeof error !== 'object') return false;

  for(const value of Object.values(error)) {
    if(!value || !Array.isArray(value)) {
      return false;
    }
  }

  return true;
}
