import { ErrorHandler, Injectable, NgZone, inject } from "@angular/core";
import { NotificationService } from 'src/app/shared/components/notification/services/notification.service';
import { isBackendError } from './utils/is-backend-error';
import { getBackendErrorMessage } from './utils/get-backend-error-message';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  private zone = inject(NgZone);
  private notificationSrv = inject(NotificationService);

  handleError(error: unknown): void {
    console.error('Error occured: ', error);

    this.zone.run(() => {
      let errorObj = {
        title: 'Error occured',
        content: 'Something went wrong.'
      }

      if(isBackendError(error)){
        errorObj = { ...getBackendErrorMessage(error) };
      }

      this.notificationSrv.newNotification({ data: errorObj });
    });
  }
}
