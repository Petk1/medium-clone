import { bootstrapApplication } from '@angular/platform-browser';
import { ErrorHandler, importProvidersFrom, isDevMode } from '@angular/core';
import { provideHttpClient, withInterceptors } from '@angular/common/http';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { MatDialogModule } from '@angular/material/dialog';
import { provideRouterStore, routerReducer } from '@ngrx/router-store';
import { provideState, provideStore } from '@ngrx/store';
import { provideStoreDevtools } from '@ngrx/store-devtools';
import { provideEffects } from '@ngrx/effects';

import { appRoutes } from './app/app.routes';
import { AppComponent } from './app/app.component';

import { authFeatureKey, authReducer } from './app/auth/store/reducers';
import * as authEffects from './app/auth/store/effects';
import * as articlesEffects from './app/shared/components/articles/store/effects';
import * as popularTagsEffects from './app/shared/components/popular-tags/store/effects';
import * as followEffects from './app/shared/components/follow-button/store/effects';
import * as likeEffects from './app/shared/components/favorite-article-button/store/effects';
import * as postCommentEffect from './app/post-comment/store/effects';
import * as commentsEffect from './app/comments/store/effects';
import * as articleFormEffect from './app/shared/components/article-form/store/effects';
import { GlobalErrorHandler } from './app/core/errors/global-error-handler';
import { loaderInterceptor } from './app/core/interceptors/loader.interceptor';
import { articlesFeatureKey, articlesReducer } from './app/shared/components/articles/store/reducers';
import { authInterceptor } from './app/core/interceptors/auth.interceptor';
import { apiUrlInterceptor } from './app/core/interceptors/api-url.interceptor';
import { popularTagsFeatureKey, popularTagsReducer } from './app/shared/components/popular-tags/store/reducers';
import { FollowService } from './app/shared/components/follow-button/services/follow.service';
import { followFeatureKey, followReducer } from './app/shared/components/follow-button/store/reducers';
import { LikeService } from './app/shared/components/favorite-article-button/services/like.service';
import { likeFeatureKey, likeReducer } from './app/shared/components/favorite-article-button/store/reducers';
import { postCommentFeatureKey, postCommentReducer } from './app/post-comment/store/reducers';
import { PostCommentService } from './app/post-comment/services/post-comment.service';
import { CommentsService } from './app/comments/services/comments.service';
import { commentsFeatureKey, commentsReducer } from './app/comments/store/reducers';

bootstrapApplication(AppComponent, {
  providers: [
    provideRouter(appRoutes, withComponentInputBinding()),
    provideHttpClient(
      withInterceptors([
        apiUrlInterceptor,
        authInterceptor,
        loaderInterceptor,
      ])
    ),
    provideRouterStore(),
    provideStore({
      router: routerReducer
    }),
    provideState(authFeatureKey, authReducer),
    provideState(articlesFeatureKey, articlesReducer),
    provideState(popularTagsFeatureKey, popularTagsReducer),
    provideState(followFeatureKey, followReducer),
    provideState(likeFeatureKey, likeReducer),
    provideState(postCommentFeatureKey, postCommentReducer),
    provideState(commentsFeatureKey, commentsReducer),
    provideEffects(
      authEffects,
      articlesEffects,
      popularTagsEffects,
      followEffects,
      likeEffects,
      postCommentEffect,
      commentsEffect,
      articleFormEffect,
    ),
    provideStoreDevtools(
      {
        maxAge: 25,
        logOnly: !isDevMode(),
        autoPause: true,
        trace: false,
        traceLimit: 75,
      }
    ),
    importProvidersFrom(MatDialogModule),
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler,
    },
    FollowService,
    LikeService,
    PostCommentService,
    CommentsService,
  ],
}).catch(err => console.error(err));
